<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Item extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public static function viewItemsToSend($id){

      $items = DB::table('items')
              ->leftJoin('general_serials','general_serials.id','=','items.general_id')
              ->leftJoin('item_statuses', 'item_statuses.id', '=', 'items.item_status_id')
              ->where('general_serials.id', $id)
              ->where('projectid','>=', 0)
              ->select('items.*','item_statuses.item_status_name as item_status')
              ->get();

      return $items;
    }

    public static function returnDetails($id){

      $details = DB::table('items')
              ->leftJoin('general_serials','general_serials.id','=','items.general_id')
              ->leftJoin('donators','general_serials.donorid', '=', 'donators.id')
              ->leftJoin('categories','general_serials.category_id', '=', 'categories.id')
              ->where('general_serials.id', $id)
              ->where('projectid','>=', 0)
              ->select('general_serials.id as gen_id', 'general_serials.generalid as generalID','donators.name as donorname','donators.organization_name as orname','donators.email as donatoremail','general_serials.*',
              'categories.name as catname', 'donators.donatorid as donid')
              ->first();

      return $details;
    }
}
