<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use DB;
use Session;
use Validator;

class Country extends Model
{
    use SoftDeletes;
    protected $table = 'countries';
    protected $dates = ['deleted_at'];

    public static function saveCountry($request){

      $validator = Validator::make($request->all(), [
          'countryName'=>'required',
      ]);

      if ($validator->fails()) {
          Session::flash('error', 'Ooops!!! Fill required fields');
      }else{

        $country = new Country;
        $country->country_name = $request['countryName'];
        $country->save();

        if($country){
            Session::flash('create-country-success', 'Country created Successfully!');
        }else{
            Session::flash('error', 'Ooops!!! Country saving was not Successful');
        }
        return $country;
      }
    }

    public static function getallCountry(){
      $countries = DB::table('countries')
                  ->where('deleted_at','=', null)
                  ->orderBy('created_at', 'desc')
                  ->get();
      return $countries;
    }

    public static function deleteCountry($id){
      $country = DB::table('countries')->where('id',$id);
      $country->delete();
      if($country){
          Session::flash('create-country-success', 'Country deleted Successfully!');
      }else{
          Session::flash('error', 'Ooops!!! Country deleting was not Successful');
      }
      return $country;
    }

    public static function editCountry($id){
        $country = DB::table('countries')->find($id);
        return $country;
    }

    public static function updateCountry($request){
      $validator = Validator::make($request->all(), [
          'countryName'=>'required',
      ]);

      if ($validator->fails()) {
          Session::flash('error', 'Ooops!! Filled cannot be empty');
      }else{

      $country = Country::findorFail($request['country_id']);
      $country->country_name = $request['countryName'];
      $country->save();

      if($country){
          Session::flash('create-country-success', 'Country updated Successfully!');
      }else{
          Session::flash('error', 'Ooops!!! Country updating was not Successful');
      }
      return $country;
     }
    }
}
