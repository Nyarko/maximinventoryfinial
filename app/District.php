<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use DB;
use Session;
use Validator;

class District extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public static function saveDistrict($request){

      $validator = Validator::make($request->all(), [
          'districtName'=>'required',
      ]);

      if ($validator->fails()) {
          Session::flash('error', 'Ooops!!! Fill required fields');
      }else{

        $district = new District;
        $district->district_name = $request['districtName'];
        $district->save();

        if($district){
            Session::flash('create-district-success', 'District created Successfully!');
        }else{
            Session::flash('error', 'Ooops!!! District saving was not Successful');
        }

        return $district;

      }
    }

    public static function getallDistrict(){
      $districts = DB::table('districts')
                 ->where('deleted_at','=', null)
                 ->orderBy('created_at', 'desc')
                 ->get();
      return $districts;
    }

    public static function deleteDistrict($id){
      $district = DB::table('districts')->where('id', $id);
      $district->delete();
      if($district){
          Session::flash('delete', 'District deleted Successfully!');
      }else{
          Session::flash('error', 'Ooops!!! District deleting was not Successful');
      }
      return $district;
    }

    public static function editDistrict($id){
        $district = DB::table('districts')->find($id);
        return $district;
    }

    public static function updateDistrict($request){

      $validator = Validator::make($request->all(), [
          'districtName'=>'required',
      ]);

      if ($validator->fails()) {
          Session::flash('error', 'Ooops!!! Fill required fields');
      }else{

        $district = District::findorFail($request['district_id']);
        $district->district_name = $request['districtName'];
        $district->save();

        if($district){
            Session::flash('create-district-success', 'District updated Successfully!');
        }else{
            Session::flash('error', 'Ooops!!! District updating was not Successful');
        }

        return $district;
      }
    }
}
