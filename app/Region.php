<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use Session;
use Validator;

class Region extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

  // Returning all regions
  public static function getallRegions(){
      $regions = DB::table('regions')
                ->where('deleted_at','=', null)
                ->orderBy('created_at', 'desc')
                ->get();
      return $regions;
    }
    // Saving region
  public static function saveRegion($request){

      $validator = Validator::make($request->all(), [
           'regionName'=>'required',
      ]);

      if ($validator->fails()) {
          Session::flash('error', 'Ooops!!! Fill required fields');
      }else{

        $region = new Region;
        $region->region_name = $request['regionName'];
        $region->save();

        if($region){
           Session::flash('create-region-success', 'Region created Successfully!');
        }else{
           Session::flash('error', 'Ooops!!! Region saving was not Successful');
        }
        return $region;
      }
    }
  //function to delete regions
  public static function deleteRegion($id){
     $region = DB::table('regions')
                  ->where('id',$id);
    $region->delete();
    if($region){
      Session::flash('delete', 'Region deleted Successfully!');
    }else{
      Session::flash('error', 'Ooops!!! Region deleting was not Successful');
    }
     return $region;
  }
  //Editing regions
  public static function editRegion($id){
     $region = DB::table('regions')->find($id);
    return $region;
  }
  // updating region
  public static function updateRegion($request){

    $validator = Validator::make($request->all(), [
         'regionName'=>'required',
    ]);

    if ($validator->fails()) {
        Session::flash('error', 'Ooops!!! Fill required fields');
    }else{

      $region = Region::findorFail($request['region_id']);
      $region->region_name = $request['regionName'];
      $region->save();

      if($region){
         Session::flash('create-region-success', 'Region updated Successfully!');
       }else{
         Session::flash('error', 'Ooops!!! Region updating was not Successful');
       }
      return $region;
    }
  }

}
