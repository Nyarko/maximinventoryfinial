<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;
use App\GeneralSerial;
use App\User;
use App\Item;

Use DB;
use Session;

class SendMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
     public function build(Request $request)
     {
         $id = $request->gen_id;
         $items = Item::viewItemsToSend($id);
         $details = Item::returnDetails($id);
         $General_Serial = GeneralSerial::where('id', $id)->value('generalid');
         return $this->view('superadmin.Items.mail', compact('items', 'details'))->to($request->email)->subject('Computer 4 Schools');
     }
}
