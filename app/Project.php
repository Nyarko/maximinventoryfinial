<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\CollectionPoint;
use App\CollectionPointStatus;
use App\District;
use App\Region;
use App\Country;
use App\Category;
use App\DestinationPointStatus;
use App\Destinationpoint;
use Image;
use App\GeneralSerial;
use App\User;
use App\ItemStatus;
use App\Donator;
use App\School;
use App\Item;
use App\Project;
use App\Allocate;
use DB;
use Session;

class Project extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public static function getProjectPlan($id){

     $project = DB::table('projects')
           ->leftjoin('destination_points','destination_points.id','=','projects.destination_id')
           ->leftjoin('users','users.id','=','destination_points.user_id')
           ->leftjoin('regions','regions.id','=','destination_points.region_id')
           ->leftjoin('districts','districts.id','=','destination_points.district_id')
           ->leftjoin('destination_point_statuses','destination_point_statuses.id','=','destination_points.destination_status_id')
           ->select('projects.*', 'destination_points.*','users.firstname as fname', 'users.othername as surname', 'regions.region_name as region','destination_point_statuses.destination_status_name as dstatus', 'districts.district_name as district')
           ->where('projects.id', $id)->first();

      return $project;
    }


    public static function getSchoolDatails($id){

     $school = DB::table('projects')
           ->leftjoin('destination_points','destination_points.id','=','projects.destination_id')
           ->leftjoin('schools','schools.destination_id','=','projects.destination_id')
           ->select('schools.*')
           ->where('projects.id', $id)
           ->get();

         return $school;
    }


  public static function getItemsDetails($id){
  	 $items = DB::table('allocates')
             ->leftjoin('projects','projects.id','=','allocates.project_id')
             ->leftjoin('items','items.id','=','allocates.item_id')
             ->leftjoin('item_statuses','item_statuses.id','=','items.item_status_id')
             ->leftjoin('general_serials','general_serials.id','=','items.general_id')
             ->leftjoin('categories','categories.id','=','general_serials.category_id')
             ->leftjoin('donators','donators.id','=','general_serials.donorid')
             ->select('items.*','item_statuses.item_status_name as itstatus','general_serials.generalid as genid','general_serials.name as gename','categories.name as catname','donators.name as dname')
             ->where('allocates.project_id', $id)
             ->get();
     return $items;
  }

   public static function getCategory($id){

  	 $cat = DB::table('allocates')
             ->leftjoin('projects','projects.id','=','allocates.project_id')
             ->leftjoin('items','items.id','=','allocates.item_id')
             ->leftjoin('general_serials','general_serials.id','=','items.general_id')
             ->leftjoin('categories','categories.id','=','general_serials.category_id')
             ->select('categories.name as catname', DB::raw('count(*) as total'))
             ->where('allocates.project_id', $id)
             ->groupBy('categories.name')
             ->get();

     return $cat;
  }

  public static function setProject($id){

      $item = Item::findorFail($id);
      $item->projectid = 1;
      $item->save();

      if($item){
          Session::flash('item-project', 'Item added to project');
      }else{
          Session::flash('error', 'Failed to add to project');
      }
  }

  public static function revertProject($id){
      $item = Item::findorFail($id);
      $item->projectid = 0;
      $item->save();

      if($item){
          Session::flash('item-reverted', 'Item is Reverted');
      }else{
          Session::flash('error', 'Failed to revert');
      }
  }


}
