<?php

namespace App;

use DB;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DestinationPointStatus extends Model
{
    use SoftDeletes;
    protected $table = "destination_point_statuses"; 
    protected $dates = ['deleted_at']; 

   

}
