<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CollectionPoint;
use App\User;
use App\CollectionPointStatus;
use App\Country;
use DB;

class PagesController extends Controller
{
    //Returning the index to view

    public function indexPage(){


      $collectionpoint = DB::table('collection_points')
            ->leftJoin('users', 'users.id','=','collection_points.user_id')
            ->leftJoin('collection_point_statuses', 'collection_point_statuses.id','=','collection_points.collection_status_id')
            ->leftJoin('countries', 'countries.id', 'collection_points.country_id')
            ->select('collection_points.*','users.firstname as fname', 'users.othername as surname', 'users.avatar as picture','users.email as email','users.phone as contact','users.unique_number as userid','collection_point_statuses.collection_status_name as status','countries.country_name as country')
            ->where('collection_points.user_id','!=', null)
            ->orderBy('collection_points.created_at','desc')
            ->get();

    	return view('pages.index', compact('collectionpoint'));
    }


    //Returning the tracking page
    function indexTrack(){

    	return view('pages.track');
    }


    //View a collection point
    public function viewCollectionPoint($id){

    $collectionpoint = DB::table('collection_points')
                    ->leftJoin('users', 'users.id','=','collection_points.user_id')
                    ->leftJoin('collection_point_statuses', 'collection_point_statuses.id','=','collection_points.collection_status_id')
                    ->leftJoin('countries', 'countries.id', 'collection_points.country_id')
                    ->select('collection_points.*','users.firstname as fname', 'users.othername as surname', 'users.avatar as picture','users.email as email','users.phone as contact','users.unique_number as userid','collection_point_statuses.collection_status_name as status','countries.country_name as country') 
                    ->where('collection_points.id','=',$id)
                    ->first();         
                 
    return view('pages.view', compact('id','collectionpoint'));
}

}
