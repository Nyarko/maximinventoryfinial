<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DestinationPointController extends Controller
{
    //Collection Point Dashboard
    public function dashboard(){
    	return view('destinationPoint.dashboard');
    }

    //Schools function
    public function allSchools(){
    	return view('destinationPoint.Schools.allSchools');
    }

    public function addSchool(){
    	return view('destinationPoint.Schools.addSchool');
    }

    //Items function
    public function allItems(){
    	return view('destinationPoint.Items.allitems');
    }

    public function addItems(){
    	return view('destinationPoint.Items.addItem');
    } 
}
