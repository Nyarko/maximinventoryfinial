<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MNAgentController extends Controller
{
    //MN Agent Dashboard
    public function dashboard(){
    	return view('mn_agent.dashboard');
    }

    //All Schools
    public function allSchools(){
    	return view('mn_agent.Schools.allSchools');
    }

    public function addSchool(){
    	return view('mn_agent.Schools.addSchool');
    }

    //All Items
    public function allItems(){
    	return view('mn_agent.Items.allitems');
    }

    public function addItems(){
    	return view('mn_agent.Items.addItem');
    }

     //All Dono
    public function allDonators(){
    	return view('mn_agent.Donors.alldonators');
    }

    public function addDonator(){
    	return view('mn_agent.Donors.addDonators');
    }

}
