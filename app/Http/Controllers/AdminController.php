<?php

namespace App\Http\Controllers;

use App\CollectionPoint;
use App\CollectionPointStatus;
use App\District;
use App\Region;
use App\Country;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Category;
use App\DestinationPointStatus;
use App\Destinationpoint;
use Image;
use App\GeneralSerial;
use App\User;
use App\ItemStatus;
use App\Donator;
use App\School;
use App\Item;
use App\Project;
use App\Allocate;

Use DB;
use Session;
use Mail;
use App\mail\sendMail;


class AdminController extends Controller
{
    //returning the admin dashboard

    public function dashboard(){

      $user = User::all()->count();

      $item = Item::all()->count();

      $itemstatus = ItemStatus::all()->count();

      $category = Category::all()->count();

      $collectionpoint = CollectionPoint::all()->count();

      $collectionstatus = CollectionPointStatus::all()->count();

      $destination = DestinationPoint::all()->count();

      $destinationstatus = DestinationPointStatus::all()->count();

      $region = Region::all()->count();

      $district = District::all()->count();

      $school = School::all()->count();

      $donator = Donator::all()->count();

    	return view('superadmin.dashboard', compact('user', 'item', 'itemstatus', 'category', 'collectionpoint', 'collectionstatus', 'destination', 'destinationstatus', 'region', 'district', 'school', 'donator'));
    }


    public function addUser(){

    	return view('superadmin.Users.addusers');
    }

    public function postAddUser(Request $request){

        $this->validate($request, [
            'firstname'=>'required',
            'othername'=>'required',
            'email'=>'required|email|unique:users,email',
            'phone_number'=>'required',
            'usertype'=>'required',
            'password'=>'required',
        ]);

        $unique_number ="C4S".mt_rand(1000000,9999999)."G";

         //checking if user idnumber already exist
        $found_unique_number = DB::table('users')
        ->where('unique_number', $unique_number)
        ->first();
        // if unique number already exists in the database, create a new one
        while($found_unique_number != null) {
         $unique_number="C4S".mt_rand(1000000,9999999)."G";
        }

       $user = new User;
       $user->firstname = $request['firstname'];
       $user->othername = $request['othername'];
       $user->email = $request['email'];
       $user->phone = $request['phone_number'];
       $user->usertype = $request['usertype'];
       $user->password = bcrypt($request['password']);
       $user->unique_number = $unique_number;

        //proces picture and save to customer/pictures folder

         if($request->hasFile('picture')){
          $picture  = $request->file('picture');
          $filename = time().$picture->getClientOriginalName();
          Image::make($picture)->resize(400,400)->save( public_path('img/users/' . $filename));
          $user->avatar=$filename;
        }

        else {
          $user->avatar = 'avatar.png';
        }

       $user->save();
       Session::flash('create-user-success', 'User Created Successfully!');
       return redirect()->route('alluser');

   }


public function allUsers(){
    $allUsers = User::all();
    return view('superadmin.Users.allusers', compact('allUsers'));
}

public function EditUser($id){
    $user = User::where('id', $id)->first();
    return view('superadmin.Users.edit', compact('user'));
}


public function postEditUser(Request $request){

    $this->validate($request, [
            'firstname'=>'required',
            'othername'=>'required',
            'email'=>'required|email',
            'phone_number'=>'required',
            'usertype'=>'required',


        ]);

       $user = User::findorFail($request['user_id']);
       $user->firstname = $request['firstname'];
       $user->othername = $request['othername'];
       $user->email = $request['email'];
       $user->phone = $request['phone_number'];
       $user->usertype = $request['usertype'];

       if($request->hasFile('picture')){
          $picture  = $request->file('picture');
          $filename = time().$picture->getClientOriginalName();
          Image::make($picture)->resize(300,300)->save( public_path('img/users/' . $filename));
          $user->avatar=$filename;
        }

       $user->save();

       Session::flash('update-user-success', 'User Information Updated Successfully!');
       return redirect()->route('alluser');
}

public function deleteUser($id){
        $user = User::findorFail($id);
        $user->delete();
        Session::flash('delete-user-success', 'User Deleted Succcessfully!');
        return redirect()->route('alluser');

  }

public function addCategory(){
    return view('superadmin.Category.addCategory');
}

public function postAddCategory(Request $request){

    $this->validate($request, [

        'categoryname'=>'required'

    ]);

    $category = new Category;
    $category->name = $request['categoryname'];
    $category->save();

    Session::flash('create-category-success', 'Category created Successfully!');
    return redirect()->route('allCategories');

}

public function allCategories(){

    $allCategories = Category::orderBy('name', 'ASC')->get();

   return view('superadmin.Category.categories', compact('allCategories'));
}


public function EditCategory($id){

     $category = Category::where('id', $id)->first();
     return view('superadmin.Category.edit', compact('category'));

}

public function postEditCategory(Request $request){

    $this->validate($request, [

        'categoryname'=>'required'

    ]);

    $category = Category::findorFail($request['category_id']);
    $category->name = $request['categoryname'];
    $category->save();

    Session::flash('update-category-success', 'Category Updated Successfully!');
    return redirect()->route('allCategories');
}


public function deleteCategory($id){

        $category = Category::findorFail($id);
        $category->delete();

        Session::flash('delete-category-success', 'Category Deleted Succcessfully!');
        return redirect()->route('allCategories');

}

public function allRegion(){
   $regions = Region::getallRegions();
   return view('superadmin.Regions.regions', compact('regions'));
}

public function addRegion(){
    return view('superadmin.Regions.addRegions');
}

public function postAddRegion(Request $request){
    $region = Region::saveRegion($request);
    if($region->save()){
      return redirect()->route('allregion');
    }else{
      return redirect()->route('addRegions');
    }
}

public function deleteRegion($id){
    $region = Region::deleteRegion($id);
    if($region){
      return redirect()->route('allregion');
    }else{
      return redirect()->route('allregion');
    }
}

public function editRegion($id){
    $region = Region::editRegion($id);
    return view('superadmin.Regions.edit', compact('region'));
}

public function updateRegion(Request $request){
   $region = Region::updateRegion($request);
   if($region){
      return redirect()->route('allregion');
    }else{
      return redirect()->route('region-edit');
    }
}

public function allDistrict(){
    $districts = District::getallDistrict();
    return view('superadmin.Districts.district', compact('districts'));
}

public function addDistrict(){
    return view('superadmin.Districts.addDistrict');
}

public function postAddDistrict(Request $request){
    $district = District::saveDistrict($request);
    if($district){
        return redirect()->route('allDistrict');
    }else{
        return redirect()->route('addDistrict');
    }
}

public function deleteDistrict($id){
    $district = District::deleteDistrict($id);
    if($district){
        return redirect()->route('allDistrict');
    }else{
        return redirect()->route('allDistrict');
    }
}

public function editDistrict($id){
    $district = District::editDistrict($id);
    return view('superadmin.Districts.edit', compact('district'));
}

public function updateDistrict(Request $request){
    $district = District::updateDistrict($request);
    if($district){
       return redirect()->route('allDistrict');
    }else{
       return redirect()->route('district-edit');
    }
}

public function allCountry(){
    $countries = Country::getallCountry();
    return view('superadmin.Country.country', compact('countries'));
}

public function addCountry(){
    return view('superadmin.Country.addCountry');
}

public function postAddCountry(Request $request){
    $country = Country::saveCountry($request);
    if($country){
      return redirect()->route('allCountry');
    }else{
      return redirect()->route('addCountry');
    }
}

public function deleteCountry($id){
    $country = Country::deleteCountry($id);
    if($country){
        return redirect()->route('allCountry');
    }else{
        return redirect()->route('allCountry');
    }
}

public function editCountry($id){
    $country = Country::editCountry($id);
    return view('superadmin.Country.edit', compact('country'));
}

public function updateCountry(Request $request){
    $country = Country::updateCountry($request);
    if($country){
       return redirect()->route('allCountry');
    }else{
       return redirect()->route('country-edit');
    }
}



public function allItem(){

  $allGeneralItems = DB::table('general_serials')
  ->leftJoin('categories', 'categories.id', '=', 'general_serials.category_id')
  ->leftJoin('collection_points', 'collection_points.id', '=', 'general_serials.collectionpoint_id')
  ->select('general_serials.*','categories.name as categoryName','collection_points.collectionpoint_name as collectionPointName')
  ->orderBy('created_at', 'DESC')
  ->get();


  return view('superadmin.Items.allitems', compact('allGeneralItems'));

}

public function createItem(){

  $categories = Category::all();
  $collectionpoints = CollectionPoint::all();
  $donors = Donator::all();

  return view('superadmin.Items.addItem', compact('categories','collectionpoints','donors'));
}

public function postCreateItem(Request $request){

  $this->validate($request, [

      'itemname'=>'required',
      'item_quantity'=>'required',
      'categoryid'=>'required',
      'collectionpointid'=>'required',
      'donatorid'=>'required',

  ]);


       // GET THE ITEMS(S) QUANTITY
       $item_quantity = $request['item_quantity'];
       $item_name = $request['itemname'];

       $item_unique_number ="C4S".mt_rand(1000, 9999)."TM";

         //checking if item unique number already exist
        $found_unique_number = DB::table('general_serials')
        ->where('generalid', $item_unique_number)
        ->first();
        // if unique number already exists in the database, create a new one
        while($found_unique_number != null) {
           $item_unique_number="C4S".mt_rand(1000, 9999)."TM";

       }

      // CREATE NEW ITEM AND ASSIGN A NEW GENEREL SERIAL ID
      $general_serial = new GeneralSerial;
      $general_serial->name = $request['itemname'];
      $general_serial->quantity = $request['item_quantity'];
      $general_serial->category_id = $request['categoryid'];
      $general_serial->collectionpoint_id = $request['collectionpointid'];
      $general_serial->donorid = $request['donatorid'];
      $general_serial->generalid = $item_unique_number;
      $general_serial->save();

      // find the last insert id
      $generated_general_id = $general_serial->id;

      //counter
      $X=1;
      while ( $X <= $item_quantity) {
        $X++;

        $item_serials = "MN/".date('y')."/".uniqid();

        $item = new Item;
        $item->general_id = $generated_general_id;
        $item->itemid = $item_serials;
        $item->save();
      }

      Session::flash('create-item-success', 'Items Created Successfully!');
      return redirect()->route('all-items');

}


public function viewItems($id){

   $all_items_status = ItemStatus::all();

  $items = DB::table('items')
          ->leftJoin('general_serials','general_serials.id','=','items.general_id')
          ->leftJoin('item_statuses', 'item_statuses.id', '=', 'items.item_status_id')
          ->where('general_serials.id', $id)
          ->where('projectid','=', 0)
          ->select('items.*','item_statuses.item_status_name as item_status')
          ->get();

  $totalQuantity = $items->count();


  $totalReadyForProject = DB::table('items')
                     ->where('general_id', $id)
                     ->where('projectid','=', 1)
                     ->count();


  $totalAddedForProject = DB::table('items')
                     ->where('general_id', $id)
                     ->where('projectid','=', 2)
                     ->count();


  $General_Serial = GeneralSerial::where('id', $id)->value('generalid');
  $General_Serial_quantity = GeneralSerial::where('id', $id)->value('quantity');

          return view('superadmin.Items.viewgeneral', compact('items','General_Serial','General_Serial_quantity','all_items_status', 'totalQuantity', 'totalReadyForProject', 'totalAddedForProject'));

}


public function multipleStatusUpdate(Request $request){



  $status_selected = $request['multiplestatus'];
  $items_selected  = $request['checkallboxes'];
  $general_serial_id = $request['general_id'];

  if($request['checkallboxes']==NULL){

                  Session::flash('no-item-selected','Please Select Items to Update Status');
                  return redirect()->back();
              }

        else{
  foreach($items_selected as $key=>$item){



         DB::table('items')
         ->where('id', $request['checkallboxes'][$key])
         ->update(['item_status_id'=>$status_selected]);

  }

}

  Session::flash('multiplestatusupdate', 'Bulk Items Status updated Successfully !!!');
  return redirect()->route('view-items', [$general_serial_id]);


}


public function updateItemStatus(Request $request){

    $this->validate($request, [

        'ItemStatus'=>'required'

    ]);

      $item = Item::findorFail($request['status_id']);
      $General_Serial = $item->general_id;
      $item->item_status_id = $request['ItemStatus'];
      $item->save();

      if($item){

        Session::flash('item-update-success', 'Item status is updated');
        return redirect()->route('view-items', compact('General_Serial'));
      }else{

          Session::flash('error', 'Failed to update status');
          return redirect()->route('view-items', compact('General_Serial'));
      }

}

public function setProject($id){
    $project = Project::setProject($id);
    $item = Item::findorFail($id);
    $General_Serial = $item->general_id;
    if($project){
        return redirect()->route('view-items', compact('General_Serial'));
    }else{
        return redirect()->route('view-items', compact('General_Serial'));
    }
}

public function revertProject($id){
    $project = Project::revertProject($id);
    $item = Item::findorFail($id);
    $General_Serial = $item->general_id;
    if($project){
        return redirect()->back();
    }else{
        return redirect()->route('view-category-item-project', compact('General_Serial'));
    }
}

public function viewProject($id){

   $plan = Project::getProjectPlan($id);

   $school = Project::getSchoolDatails($id);

   $items = Project::getItemsDetails($id);

   $category = Project::getCategory($id);

   $total = count($items);

   return view('superadmin.project.viewproject', compact('id','plan','school','items','category', 'total'));
}

public function addItemsStatus(){
    return view('superadmin.Items.addItemStatus');
}


public function postadddItemStatus(Request $request){
    $this->validate($request, [
        'itemstatusname'=>'required'
    ]);

    $itemstatus = new ItemStatus;
    $itemstatus->item_status_name = $request['itemstatusname'];
    $itemstatus->save();

    Session::flash('create-item-status-success', 'Item Status created Successfully!');
    return redirect()->route('allItemsStatus');

}

public function allItemsStatus(){

    $allItemStatus = ItemStatus::orderBy('created_at','DESC')->get();
    return view('superadmin.Items.itemsStatus', compact('allItemStatus'));
}


public function EditItemStatus($id){

     $itemstatus = ItemStatus::where('id', $id)->first();
     return view('superadmin.Items.edititemstatus', compact('itemstatus'));

}

public function PostEditItemStatus(Request $request){

  $this->validate($request, [

        'itemstatusname'=>'required'

    ]);

    $itemstatus = ItemStatus::findorFail($request['itemstatus_id']);
    $itemstatus->item_status_name = $request['itemstatusname'];
    $itemstatus->save();

    Session::flash('update-item-status-success', 'Item Status Updated Successfully!');
    return redirect()->route('allItemsStatus');

}


public function deleteItemStatus($id){

        $itemstatus = ItemStatus::findorFail($id);
        $itemstatus->delete();

        Session::flash('delete-item-status-success', 'Item Status Deleted Succcessfully!');
        return redirect()->route('allItemsStatus');

}



// functions for project planning
public function getAllCategoriesDonated(){

  $allCategoriesDonated = DB::table('categories')
          ->select('categories.*')
          ->get();

    return view('superadmin.project.allcategories', compact('allCategoriesDonated'));


}


public function viewItemCategoryProject($id){

  $projectlist = Project::all();

  $viewItemCategoryProject = DB::table('items')
       ->leftjoin('general_serials', 'general_serials.id','=','items.general_id')
       ->leftjoin('categories','categories.id','=','general_serials.category_id')
       ->leftjoin('item_statuses','item_statuses.id','=','items.item_status_id')
       ->select('items.*','general_serials.generalid','general_serials.id as gen_id','item_statuses.item_status_name')
       ->where('items.projectid', 1)
       ->where('categories.id', $id)
       ->get();

   return view('superadmin.project.viewitemcategoryproject', compact('viewItemCategoryProject','projectlist'));
}


public function allocateItemsToProject(Request $request){

        $project_id = $request['projectname'];
        $items_selected  = $request['checkallboxes'];

            if($request['checkallboxes']==NULL){

                  Session::flash('no-item-selected','Please Select Items to add to Project');
                  return redirect()->back();
              }

            else{

               foreach($items_selected as $key=>$item) {
                  if(isset($request['checkallboxes'])){

                  $allocateProject = new Allocate;
                  $allocateProject->item_id = $request['checkallboxes'][$key];
                  $allocateProject->project_id = $request['projectname'];
                  $allocateProject->save();

                  $updateProjectID = DB::table('items')
                              ->where('items.id', $request['checkallboxes'][$key])
                              ->update(['projectid'=>2]);
                    }
                }

            }


            Session::flash('items-allocated-success', "Items Allocated to Project Successfully !!!");
            return redirect()->route('project-index');

}


public function projectIndex(){

  $project = DB::table('projects')
             ->leftjoin('destination_points', 'destination_points.id', '=', 'projects.destination_id')
             ->select('projects.*', 'destination_points.destinationpoint_name as destination')
             ->orderBy('created_at','desc')
             ->get();

  return view('superadmin.project.allprojects', compact('project'));
}

public function addProject(){

   $destination = DestinationPoint::all();

  return view('superadmin.project.addProject', compact('destination'));
}


public function postProject(Request $request){

   $this->validate($request, [
      'proname' => 'required',
      'startdate' => 'required',
      'enddate' => 'required',
      'destiid' => 'required'
   ]);


   $project = new Project();
   $project->name = $request['proname'];
   $project->status = 'New';
   $project->start = $request['startdate'];
   $project->end = $request['enddate'];
   $project->destination_id = $request['destiid'];

   $project->save();

   if($project){

      Session::flash('create-project-success', 'Project Created Successfully !');
      return redirect()->route('project-index');

   }else{

      Session::flash('error', 'Failed to create project!');
      return redirect()->route('add-project-start');
   }

}

public function updateProject(Request $request){

   $this->validate($request, [
      'proname' => 'required',
      'startdate' => 'required',
      'enddate' => 'required',
      'destiid' => 'required'
       ]);

       $project = Project::findorFail($request['project_id']);
       $project->name = $request['proname'];
       $project->start = $request['startdate'];
       $project->end = $request['enddate'];
       $project->destination_id = $request['destiid'];

       $project->save();

      if($project){

        Session::flash('update-project-success', 'Project Updated Successfully !');
        return redirect()->route('project-index');

       }else{

        Session::flash('error', 'Ooops!!! Project update was not Successful!');
        return redirect()->route('edit-project');

       }
}



public function editProject($id){

   $destination = DestinationPoint::all();

   $project = DB::table('projects')
              ->leftjoin('destination_points','destination_points.id','=','projects.destination_id')
              ->select('projects.*', 'destination_points.destinationpoint_name as desti')
              ->where('projects.id',$id)
              ->first();

  return view('superadmin.project.edit', compact('destination', 'project'));
}


public function ongoingProject($id){

    $project = Project::findorFail($id);
    $project->status = "Ongoing";

    $project->save();

    if($project){

        Session::flash('project', 'Project is Initiated');
        return redirect()->back();
    }else{

        Session::flash('error', 'Failed to set project to ogoing');
        return redirect()->back();
    }
}


public function completedProject($id){

    $project = Project::findorFail($id);
    $project->status = "Completed";

    $project->save();

    if($project){

        Session::flash('completed', 'Project is Completed');
        return redirect()->back();
    }else{

        Session::flash('error', 'Failed to revert');
        return redirect()->back();
    }
}


public function addSelectedItemProject(Request $request){

        $items_selected  = $request['name'];
        $general_serial_id = $request['type'];

        if($request['checkallboxes']==NULL){

              Session::flash('no-item-selected','Please Select Items to add to Project');
              return redirect()->back();
          }

        else{

           foreach($items_selected as $key=>$item) {
              if(isset($request['checkallboxes'])){

              $updateProjectID = DB::table('items')
                          ->where('items.id', $request['checkallboxes'][$key])
                          ->update(['projectid'=>1]);
                }
            }


            if($updateProjectID){

                Session::flash('addbulkitemstoproject', 'Bulk Items Added to project Successfully !!!');
                return redirect()->route('view-items', [$general_serial_id]);

            }else{

               Session::flash('error', 'Failed Adding Bulk Items to project !!!');
               return redirect()->route('view-items', [$general_serial_id]);

            }

        }
}




public function allCollectionPoint(){

    $collectionpoint = DB::table('collection_points')
                 ->leftJoin('users', 'users.id','=','collection_points.user_id')
                 ->leftJoin('collection_point_statuses', 'collection_point_statuses.id','=','collection_points.collection_status_id')
                 ->leftJoin('countries', 'countries.id', 'collection_points.country_id')
                 ->select('collection_points.*','users.firstname as fname', 'users.othername as surname', 'collection_point_statuses.collection_status_name as status','countries.country_name as country')
                 ->orderBy('collection_points.created_at','desc')
                 ->get();


    return view('superadmin.Collectionpoint.collectionpoint', compact('collectionpoint'));
}

public function viewCollectionPoint($id){

    $collectionpoint = DB::table('collection_points')
                    ->leftJoin('users', 'users.id','=','collection_points.user_id')
                    ->leftJoin('collection_point_statuses', 'collection_point_statuses.id','=','collection_points.collection_status_id')
                    ->leftJoin('countries', 'countries.id', 'collection_points.country_id')
                    ->select('collection_points.*','users.firstname as fname', 'users.othername as surname', 'users.avatar as picture','users.email as email','users.phone as contact','users.unique_number as userid','collection_point_statuses.collection_status_name as status','countries.country_name as country')
                    ->where('collection_points.id','=',$id)
                    ->first();


                     // ->where('id','=',$id)
                     // ->get();

    return view('superadmin.Collectionpoint.view', compact('id','collectionpoint'));
}

public function addCollectionPoint(){

                $users = DB::table('users')
                        ->where('usertype', '=', 'Collection Manager')
                        ->where('deleted_at','=', null)
                        ->get();

     $collectionStatus = DB::table('collection_point_statuses')
                        ->where('deleted_at','=', null)
                        ->get();

     $country = DB::table('countries')
                        ->where('deleted_at','=', null)
                        ->get();

    return view('superadmin.Collectionpoint.addCollectionpoint', compact('users', 'collectionStatus', 'country'));
}


 public function postAddCollectionPoint(Request $request){

        $this->validate($request, [
            'name'=>'required'
        ]);


        $collectionpoint_unique_number ="C4S".mt_rand(1000000,9999999)."CP";

         //checking if collectionpoint idnumber already exist
        $found_unique_number = DB::table('collection_points')
        ->where('collectionid', $collectionpoint_unique_number)
        ->first();
        // if unique number already exists in the database, create a new one
        while($found_unique_number != null) {
           $collectionpoint_unique_number="C4S".mt_rand(1000000,9999999)."CP";
       }

       $collection = new CollectionPoint;
       $collection->user_id = $request['manager'];
       $collection->collection_status_id = $request['status'];
       $collection->country_id = $request['country'];
       $collection->collectionid = $collectionpoint_unique_number;
       $collection->collectionpoint_name = $request['name'];
       $collection->description = $request['description'];
       $collection->street_address = $request['saddress'];
       $collection->number_address = $request['numberaddress'];
       $collection->postal_address = $request['postaladdress'];
       $collection->map = $request['map'];

      //proces picture and save to img/map folder

       if($request->hasFile('image')){
          $image  = $request->file('image');
          $filename = time().$image->getClientOriginalName();
          Image::make($image)->resize(400,400)->save( public_path('img/map/' . $filename));
          $collection->image=$filename;
        } else {
          $collection->image = 'avatar.png';
        }

       $collection->save();

      if($collection){

        Session::flash('create-point-success', 'Collection Point Created Successfully !');
        return redirect()->route('allCollectionpoint');

       }else{

        Session::flash('error', 'Ooop!!! Collection Point creation was not Successful!');
        return redirect()->route('addCollectionPoint');

       }

   }

public function deleteCollectionPoint($id){

     $collection = CollectionPoint::findorFail($id);
     $collection->delete();

    if($collection){
        Session::flash('create-delete-success', 'Collection Point has been set to Inactive');
        return redirect()->route('allCollectionpoint');
       }else{

        Session::flash('error', 'Ooop!!! Collection Point deleting was not Successful!');
        return redirect()->route('allCollectionpoint');
    }
}

public function editCollectionPoint($id){

                 $users = DB::table('users')
                        ->where('usertype', '=', 'Collection Manager')
                        ->where('deleted_at','=', null)
                        ->get();

     $collectionStatus = DB::table('collection_point_statuses')
                        ->where('deleted_at','=', null)
                        ->get();

             $location = DB::table('countries')
                        ->where('deleted_at','=', null)
                        ->get();

 $collection = DB::table('collection_points')
            ->leftJoin('users', 'users.id','=','collection_points.user_id')
            ->leftJoin('collection_point_statuses', 'collection_point_statuses.id','=','collection_points.collection_status_id')
            ->leftJoin('countries', 'countries.id', 'collection_points.country_id')
            ->select('collection_points.*','users.firstname as fname','users.othername as surname','countries.country_name as country',
                'collection_point_statuses.collection_status_name as status')
            ->where('collection_points.id','=',$id)
            ->first();

    return view('superadmin.Collectionpoint.editcollectionpoint', compact('id', 'collection', 'users', 'collectionStatus', 'location'));
}

public function updateCollectionPoint(Request $request){
       $this->validate($request, [
            'name'=>'required'
       ]);


       $collection = CollectionPoint::findorFail($request['collection_id']);
       $collection->user_id = $request['manager'];
       $collection->collection_status_id = $request['status'];
       $collection->country_id = $request['country'];
       $collection->collectionpoint_name = $request['name'];
       $collection->description = $request['description'];
       $collection->street_address = $request['saddress'];
       $collection->number_address = $request['numberaddress'];
       $collection->postal_address = $request['postaladdress'];
       $collection->map = $request['map'];

      //proces picture and save to img/map folder

       if($request->hasFile('image')){
          $image  = $request->file('image');
          $filename = time().$image->getClientOriginalName();
          Image::make($image)->resize(400,400)->save( public_path('img/map/' . $filename));
          $collection->image=$filename;
        } else {
          $collection->image = $request['avatar'];
        }


       // dd($collection);


       $collection->save();

      if($collection){

        Session::flash('create-point-success', 'Collection Point Updated Successfully !');
        return redirect()->route('allCollectionpoint');

       }else{

        Session::flash('error', 'Ooop!!! Collection Point updating was not Successful!');
        return redirect()->route('edit-collectionpoint');

       }

}

public function activeCollectionPoint($id){

    $collection = CollectionPoint::withTrashed()->findorFail($id);

    if($collection->trashed()){

        $collection->restore();

        if($collection){

          Session::flash('create-delete-success', 'Collection Point is set to active Successfully!');
          return redirect()->route('allCollectionpoint');

        }else{

            Session::flash('error', 'Ooops!!! Collection Point setting was not Successful');
            return redirect()->route('allCollectionpoint');
        }

    }

}

public function allCollectionPointStatus(){
     $collectionStatus = new CollectionPointStatus;
     $collectionStatus = DB::table('collection_point_statuses')
                 ->where('deleted_at','=', null)
                 ->orderBy('created_at', 'desc')
                 ->get();

    return view('superadmin.Collectionpoint.allCollectionpointStatus', compact('collectionStatus'));
}

public function addCollectionpointStatus(){
    return view('superadmin.Collectionpoint.addCollectionpointStatus');
}

public function postAddCollectionPointStatus(Request $request){
   $this->validate($request, [

    'collectionPointStatusName'=>'required'

   ]);

    $collectionStatus = new CollectionPointStatus;
    $collectionStatus->collection_status_name = $request['collectionPointStatusName'];
    $collectionStatus->save();

    if($collectionStatus->save()){

      Session::flash('create-collectionStatus-success', 'Collection Point Status Created Successfully!');
      return redirect()->route('allCollectionPointStatus');

    }else{

        Session::flash('error', 'Ooops!!! Collection Point Status saving was not Successful');
        return redirect()->route('addCollectionpointStatus');
    }


}

public function deleteCollectionPointStatus($id){

    $collectionStatus = DB::table('collection_point_statuses')
              ->where('id',$id);

    $collectionStatus->delete();

    if($collectionStatus){

      Session::flash('delete', 'Collection Point Status deleted Successfully!');
      return redirect()->route('allCollectionPointStatus');

    }else{

        Session::flash('error', 'Ooops!!! Collection Point Status deleting was not Successful');
        return redirect()->route('allCollectionPointStatus');
    }

}

public function editCollectionStatus($id){

    $collection = DB::table('collection_point_statuses')->find($id);

    return view('superadmin.Collectionpoint.editStatus', compact('collection'));
}

public function updateCollectionStatus(Request $request){
   $this->validate($request, [

     'collectionPointStatusName' => 'required'

   ]);

   $collection = CollectionPointStatus::findorFail($request['collection_id']);
   $collection->collection_status_name = $request['collectionPointStatusName'];
   $collection->save();

    if($collection){

      Session::flash('create-collectionStatus-success', 'Collection Point Status updated Successfully!');
      return redirect()->route('allCollectionPointStatus');

    }else{

        Session::flash('error', 'Ooops!!! Collection Point Status updating was not Successful');
        return redirect()->route('edit-collectionstatus');
    }

}


public function allDonators(){

     $donors = DB::table('donators')
              ->orderBy('created_at','desc')
              ->get();

    return view('superadmin.Donors.alldonators', compact('donors'));
}

public function addDonators(){
    return view('superadmin.Donors.addDonators');
}

public function postaddDonators(Request $request){

   $this->validate($request, [
            'name'=>'required',
            'email' => 'required|email',
            'phone' => 'required'
        ]);


        $donor_unique_number ="C4S".mt_rand(10000,99999)."D";

         //checking if cdonor idnumber already exist
        $found_unique_number = DB::table('donators')
        ->where('donatorid', $donor_unique_number)
        ->first();
        // if unique number already exists in the database, create a new one
       while($found_unique_number != null) {
           $donor_unique_number="C4S".mt_rand(10000,99999)."D";
       }

      $emailcom = $request['email'];

      $email = DB::table('donators')
      ->where('email', $emailcom)
      ->first();

        // if email already exists in the database, trow an error message
       while($email == true) {

        Session::flash('error', 'Ooop!!! Email has been registered');
        return redirect()->route('addDonator');

       }



       $donor = new Donator;
       $donor->donatorid = $donor_unique_number;
       $donor->name = $request['name'];
       $donor->organization_name = $request['organization'];
       $donor->phone = $request['phone'];
       $donor->email = $emailcom;

       $donor->save();

      if($donor){

        Session::flash('create-donor-success', 'Donor Created Successfully !');
        return redirect()->route('alldonators');

       }else{

        Session::flash('error', 'Ooop!!! Donor creation was not Successful!');
        return redirect()->route('addDonator');

       }
}

public function editDonator($id){

    $donor = DB::table('donators')->find($id);

    return view('superadmin.Donors.edit', compact('donor'));
}

public function updateDonor(Request $request){
   $this->validate($request, [
            'name'=>'required',
            'email' => 'required|email',
            'phone' => 'required'
        ]);

       $donor = Donator::findorFail($request['donor_id']);
       $donor->name = $request['name'];
       $donor->organization_name = $request['organization'];
       $donor->phone = $request['phone'];
       $donor->email = $request['email'];

       $donor->save();

      if($donor){

        Session::flash('update-donor-success', 'Donor Updated Successfully !');
        return redirect()->route('alldonators');

       }else{

        Session::flash('error', 'Ooop!!! Donor update was not Successful!');
        return redirect()->route('edit-donor');

       }

}

public function deleteDonor($id){

    $donor = Donator::findorFail($id);

    $donor->delete();

    if($donor){

      Session::flash('delete', 'Donor is deleted Successfully!');
      return redirect()->route('alldonators');

    }else{

        Session::flash('error', 'Ooops!!! Donor deleting was not Successful');
        return redirect()->route('alldonators');
    }

}

public function restoreDonors($id){

    $donor = Donator::withTrashed()->findorFail($id);

    if($donor->trashed()){

        $donor->restore();

        if($donor){

          Session::flash('create-delete-success', 'Donor is restore Successfully!');
          return redirect()->route('alldonators');

        }else{

            Session::flash('error', 'Ooops!!! Restoring donor was not Successful');
            return redirect()->route('alldonators');
        }

    }

}





public function allSchools(){

  $schools = DB::table('schools')
             ->leftJoin('regions', 'regions.id','=','schools.region_id')
             ->leftJoin('districts', 'districts.id','=','schools.district_id')
             ->leftJoin('destination_points', 'destination_points.id', 'schools.destination_id')
             ->select('schools.*','districts.district_name as district', 'regions.region_name as region',
              'destination_points.destinationpoint_name as destination')
             ->orderBy('schools.state','ASC')
             ->get();

    return view('superadmin.Schools.allSchools', compact('schools'));
}


public function viewSchools($id){

   $school = DB::table('schools')
             ->leftJoin('regions', 'regions.id','=','schools.region_id')
             ->leftJoin('districts', 'districts.id','=','schools.district_id')
             ->leftJoin('destination_points', 'destination_points.id', 'schools.destination_id')
             ->select('schools.*','districts.district_name as district', 'regions.region_name as region',
              'destination_points.destinationpoint_name as destination')
             ->where('schools.id','=',$id)
             ->first();

    return view('superadmin.Schools.view', compact('id','school'));
}



public function addSchool(){

   $regions = DB::table('regions')
            ->where('deleted_at','=',null)
            ->orderBy('created_at','=','desc')
            ->get();

   $districts = DB::table('districts')
            ->where('deleted_at','=',null)
            ->orderBy('created_at','=','desc')
            ->get();

  $destinations = DB::table('destination_points')
            ->where('deleted_at','=',null)
            ->orderBy('created_at','=','desc')
            ->get();


    return view('superadmin.Schools.addSchool', compact('regions', 'districts', 'destinations'));
}

public function editSchool($id){

 $regions = DB::table('regions')
            ->where('deleted_at','=',null)
            ->orderBy('created_at','=','desc')
            ->get();

   $districts = DB::table('districts')
            ->where('deleted_at','=',null)
            ->orderBy('created_at','=','desc')
            ->get();

  $destinations = DB::table('destination_points')
            ->where('deleted_at','=',null)
            ->orderBy('created_at','=','desc')
            ->get();

 $school = DB::table('schools')
             ->leftJoin('regions', 'regions.id','=','schools.region_id')
             ->leftJoin('districts', 'districts.id','=','schools.district_id')
             ->leftJoin('destination_points', 'destination_points.id', 'schools.destination_id')
             ->select('schools.*','districts.district_name as district','regions.region_name as region',
              'destination_points.destinationpoint_name as destination')
             ->where('schools.id','=',$id)
             ->first();

    return view('superadmin.Schools.edit', compact('id', 'school', 'regions', 'districts', 'destinations'));
}


public function postaddSchool(Request $request){

   $this->validate($request, [
            'name'=>'required',
            'email' => 'required|email',
            'phone' => 'required',
            'address' => 'required',
            'town' => 'required'
        ]);

      $emailcom = $request['email'];

       $email = DB::table('schools')
        ->where('email', $emailcom)
        ->first();

        // if email already exists in the database, trow an error message
       while($email == true) {

        Session::flash('error', 'Ooop!!! Email has been registered');
        return redirect()->route('addschool');

       }


       $school = new School;
       $school->region_id = $request['region'];
       $school->district_id = $request['district'];
       $school->destination_id = $request['destination'];

       $school->school_name = $request['name'];
       $school->phone = $request['phone'];
       $school->email = $emailcom;
       $school->address = $request['address'];
       $school->town = $request['town'];
       $school->map = $request['map'];

       $school->save();

      if($school){

        Session::flash('create-school-success', 'School Created Successfully !');
        return redirect()->route('allschools');

       }else{

        Session::flash('error', 'Ooop!!! School creation was not Successful!');
        return redirect()->route('addschool');

       }
}

public function updateSchool(Request $request){

   $this->validate($request, [
            'name'=>'required',
            'email' => 'required|email',
            'phone' => 'required',
            'address' => 'required',
            'town' => 'required'
        ]);

       $school = School::findorFail($request['school_id']);
       $school->region_id = $request['region'];
       $school->district_id = $request['district'];
       $school->destination_id = $request['destination'];
       $school->school_name = $request['name'];
       $school->phone = $request['phone'];
       $school->email = $request['email'];
       $school->address = $request['address'];
       $school->town = $request['town'];
       $school->map = $request['map'];

       $school->save();

      if($school){

        Session::flash('update-school-success', 'School Updated Successfully !');
        return redirect()->route('allschools');

       }else{

        Session::flash('error', 'Ooops!!! School update was not Successful!');
        return redirect()->route('edit-school');

       }
}

public function setInprogress($id){

    $inprogress = School::findorFail($id);

    $inprogress->state = 1;

    $inprogress->save();

      if($inprogress){

        Session::flash('create-inprogress-success', 'Process has been initiated!');
        return redirect()->route('allschools');

       }else{

        Session::flash('error', 'Ooop!!! Process initiating was not Successful!');
        return redirect()->route('allschools');

       }
}


public function setCompleted($id){

    $inprogress = School::findorFail($id);

    $inprogress->state = 2;

    $inprogress->save();

      if($inprogress){

        Session::flash('create-completed-success', 'Process has been set to Completed');
        return redirect()->route('allschools');

       }else{

        Session::flash('error', 'Ooop!!! Process setting was not Successful!');
        return redirect()->route('allschools');

       }
}


public function deleteSchool($id){

     $school = School::findorFail($id);
     $school->delete();

    if($school){
        Session::flash('delete', 'School has been set to Inactive');
        return redirect()->route('allschools');
       }else{

        Session::flash('error', 'Ooop!!! School deleting was not Successful!');
        return redirect()->route('allschools');
    }
}


public function restoreSchool($id){

    $school = School::withTrashed()->findorFail($id);

    if($school->trashed()){

        $school->restore();

        if($school){

          Session::flash('create-restore-success', 'School is restore Successfully!');
          return redirect()->route('allschools');

        }else{

            Session::flash('error', 'Ooops!!! Restoring school was not Successful');
            return redirect()->route('allschools');
        }

    }

}





public function addDestinationPoint(){

    $all_destination_managers = User::where('usertype', 'Destination Manager')->get();
    $all_destination_status = DestinationPointStatus::all();
    $all_regions = Region::all();
    $all_districts = District::all();



    return view('superadmin.Destinationpoint.addDestinationPoint', compact('all_destination_managers','all_destination_status','all_regions','all_districts'));
}


public function PostDestinationPoint(Request $request){

  $this->validate($request, [

        'destination_point_name'=>'required',
        'destination_point_town'=>'required',
        'destination_point_region'=>'required',
        'destination_point_district'=>'required',
    ]);

    $destinationpoint = new DestinationPoint;
    $destinationpoint->destinationpoint_name = $request['destination_point_name'];
    $destinationpoint->user_id = $request['destination_point_manager'];
    $destinationpoint->region_id = $request['destination_point_region'];
    $destinationpoint->district_id = $request['destination_point_district'];
    $destinationpoint->destination_status_id = $request['destination_point_status'];
    $destinationpoint->town = $request['destination_point_town'];
    $destinationpoint->description = $request['destination_point_description'];


    $destinationpoint->save();

    Session::flash('create-destinationpoint-success', 'Destination Point Added Successfully!');
    return redirect()->route('all-destination-points');
}

public function allDestinationPoint(){

    $allDestinationPoints = DB::table('destination_points')
    ->leftJoin('regions', 'regions.id', '=', 'destination_points.region_id')
    ->leftJoin('districts', 'districts.id', '=', 'destination_points.district_id')
    ->leftJoin('destination_point_statuses', 'destination_point_statuses.id', '=', 'destination_points.destination_status_id')
    ->leftJoin('users','users.id','=','destination_points.user_id')
    ->select('destination_points.*', 'regions.region_name','districts.district_name','users.firstname', 'users.othername', 'destination_point_statuses.destination_status_name')
    ->where('destination_points.deleted_at', NULL)
    ->get();

    return view('superadmin.Destinationpoint.allDestinationPoint', compact('allDestinationPoints'));
}


public function EditDestinationPoint($id){

    $all_destination_managers = User::where('usertype', 'Destination Manager')->get();
    $all_destination_status = DestinationPointStatus::all();
    $all_regions = Region::all();
    $all_districts = District::all();

    $destinationpoint = DB::table('destination_points')
    ->leftJoin('regions', 'regions.id', '=', 'destination_points.region_id')
    ->leftJoin('districts', 'districts.id', '=', 'destination_points.district_id')
    ->leftJoin('destination_point_statuses', 'destination_point_statuses.id', '=', 'destination_points.destination_status_id')
    ->leftJoin('users','users.id','=','destination_points.user_id')
    ->select('destination_points.*', 'regions.region_name','districts.district_name','users.firstname', 'users.othername', 'destination_point_statuses.destination_status_name')
    ->where('destination_points.id', $id)
    ->first();

    return view('superadmin.Destinationpoint.editdestinationpoint', compact('destinationpoint','all_destination_managers','all_destination_status','all_regions','all_districts'));
}

  public function PostEditDestinationPoint(Request $request){

    $this->validate($request, [

        'destination_point_name'=>'required',
        'destination_point_town'=>'required',
        'destination_point_region'=>'required',
        'destination_point_district'=>'required',
    ]);

    $destinationpoint = DestinationPoint::findorFail($request['destination_point_id']);
    $destinationpoint->destinationpoint_name = $request['destination_point_name'];
    $destinationpoint->user_id = $request['destination_point_manager'];
    $destinationpoint->region_id = $request['destination_point_region'];
    $destinationpoint->district_id = $request['destination_point_district'];
    $destinationpoint->destination_status_id = $request['destination_point_status'];
    $destinationpoint->town = $request['destination_point_town'];
    $destinationpoint->description = $request['destination_point_description'];
    $destinationpoint->save();

    Session::flash('update-destinationpoint-success', 'Destination Point Updated Successfully!');
    return redirect()->route('all-destination-points');

  }


   public function viewDestinationPoint($id){

     $destinationpoint_information = DB::table('destination_points')
    ->leftJoin('regions', 'regions.id', '=', 'destination_points.region_id')
    ->leftJoin('districts', 'districts.id', '=', 'destination_points.district_id')
    ->leftJoin('destination_point_statuses', 'destination_point_statuses.id', '=', 'destination_points.destination_status_id')
    ->leftJoin('users','users.id','=','destination_points.user_id')
    ->select('destination_points.*', 'regions.region_name','districts.district_name','users.firstname', 'users.othername', 'destination_point_statuses.destination_status_name')
    ->where('destination_points.id', $id)
    ->first();

    return view('superadmin.Destinationpoint.viewdestinationpoint', compact('destinationpoint_information'));


   }

    public function deleteDestinationPoint($id){

        $destinationpointstatus = DestinationPoint::findorFail($id);
        $destinationpointstatus->delete();

        Session::flash('delete-destinationpoint-success', 'Destination Point Deleted Successfully!');
        return redirect()->route('all-destination-points');

    }


public function addDestinationPointStatus(){
    return view('superadmin.Destinationpoint.addDestinationPointStatus');
}

public function PostDestinationPointStatus(Request $request){

    $this->validate($request, [

        'destinationpointstatus'=>'required',

    ]);

    $destinationpointstatus = new DestinationPointStatus;
    $destinationpointstatus->destination_status_name = $request['destinationpointstatus'];
    $destinationpointstatus->save();

    Session::flash('create-destinationpointstatus-success', 'Destination Point Status Added Successfully!');
    return redirect()->route('allDestinationPointStatus');
}


public function allDestinationPointStatus(){

    $allDestinationPointStatus = DestinationPointStatus::orderBy('destination_status_name', 'ASC')->get();
    return view('superadmin.Destinationpoint.allDestinationPointStatus', compact('allDestinationPointStatus'));
}

public function EditDestinationPointStatus($id){

    $destinationpointstatus = DestinationPointStatus::where('id', $id)->first();
    return view('superadmin.Destinationpoint.editdestinationpointstatus', compact('destinationpointstatus'));
}


public function posEdittDestinationPointStatus(Request $request){

    $this->validate($request, [

        'destinationpointstatus'=>'required',

    ]);

    $destinationpointstatus = DestinationPointStatus::findorFail($request['destinationpointstatus_id']);
    $destinationpointstatus->destination_status_name = $request['destinationpointstatus'];
    $destinationpointstatus->save();

    Session::flash('update-destinationpointstatus-success', 'Destination Point Status Updated Successfully!');
    return redirect()->route('allDestinationPointStatus');
}


public function deleteDestinationPointStatus($id){

        $destinationpointstatus = DestinationPointStatus::findorFail($id);
        $destinationpointstatus->delete();

        Session::flash('delete-destinationpointstatus-success', 'Destination Point Status Deleted Succcessfully!');
        return redirect()->route('allDestinationPointStatus');

}

public function viewItemsToSend($id){
  $items = Item::viewItemsToSend($id);
  $details = Item::returnDetails($id);
  $General_Serial = GeneralSerial::where('id', $id)->value('generalid');
  $General_Serial_quantity = GeneralSerial::where('id', $id)->value('quantity');

  return view('superadmin.Items.itemToSend', compact('items', 'details','General_Serial'));
}

public function send(){
      Mail::send(new sendMail());
     if( count(Mail::failures()) > 0 ) {
       Session::flash('error', 'Message not send, Check your Internet Connection');
       return redirect()->back();
      } else {
        Session::flash('message', 'Email is send to donor Succcessfully!');
        return redirect()->back();
     }
 }



}
