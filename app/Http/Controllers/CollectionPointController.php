<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CollectionPointController extends Controller
{
	 //Function to the Collection Point Dashboard
    public function dashboard(){
    	return view('collectionPoint.dashboard');
    }

    //items function
    public function allItems(){
    	return view('collectionPoint.Items.allitems');
    }

    public function addItems(){
    	return view('collectionPoint.Items.addItem');
    }

    //Donation function
    public function allDonors(){
    	return view('collectionPoint.Donors.alldonators');
    }

    public function addDonators(){
    	return view('collectionPoint.Donors.addDonators');
    }
}
