<?php

namespace App\Http\Controllers;
use App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

use App\User;

Use DB;
use Session;

class LoginController extends Controller
{
    //displaying the login page

    public function loginUser(){

    	return view('Users.login');
    }

    public function postLoginUser(Request $request){

    	
    	if(Auth::attempt(['email'=>$request->email, 'password'=>$request->password, 'usertype'=>'Administrator'], $request->remember)){

    		return redirect()->route('adminindex');
    	}

    	elseif(Auth::attempt(['email'=>$request->email, 'password'=>$request->password, 'usertype'=>'Collection Manager'], $request->remember)){

    	   return redirect()->route('collectionIndex');
    	}


    	elseif(Auth::attempt(['email'=>$request->email, 'password'=>$request->password, 'usertype'=>'MN Agent'], $request->remember)){

    		return redirect()->route('mn_agentindex');
    	}

    	

    	elseif(Auth::attempt(['email'=>$request->email, 'password'=>$request->password, 'usertype'=>'Donator'], $request->remember)){

    		return redirect()->route('donator_index');
    	}


    	elseif(Auth::attempt(['email'=>$request->email, 'password'=>$request->password, 'usertype'=>'Destination Manager'], $request->remember)){
    		
    		return redirect()->route('destination_index');
    	}

    	else{

    		return redirect('login')->with(['errormessage'=>'Invalid Login Credentials']);

    	}

    }

    public function logOut(){

        Auth::logout();
        return redirect()->route('login');
    }
}
