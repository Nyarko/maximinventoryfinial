<?php

namespace App;

use DB;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemStatus extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at']; 

     public static function getName($id){
         
          $name = DB::table('item_statuses')
                        ->where('id','=', $id)
                        ->pluck('item_status_name');

          return $name;  
    }
}
