@extends('layouts.mn_agentlayout')

@section('title', 'Add Donator')

@section('content')

  <div class="content-wrapper d-flex align-items-center auth register-bg-1 theme-one">
  <div class="row w-100">
    <div class="col-lg-7 col-md-8 col-sm-10 col-xs-12 mx-auto">
      <h2 class="text-center mb-4"><i class="menu-icon fas fa-users"></i> Add Donator</h2>
      <div class="auto-form-wrapper">
        <form action="#">

          <div class="form-group">
            <div class="input-group">
              <input type="text" class="form-control" placeholder="Fullname">
              <div class="input-group-append">
                <span class="input-group-text">
                  <i class="fa fa-user"></i>
                </span>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="input-group">
              <input type="email" class="form-control" placeholder="Email Address">
              <div class="input-group-append">
                <span class="input-group-text">
                  <i class="fa fa-envelope"></i>
                </span>
              </div>
            </div>
          </div>

           <div class="form-group">
            <div class="input-group">
              <input type="text" class="form-control" placeholder="Contact Number">
              <div class="input-group-append">
                <span class="input-group-text">
                  <i class="fa fa-phone"></i>
                </span>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="input-group">
              <input type="file" class="form-control" placeholder="Contact Number">
              <div class="input-group-append">
                <span class="input-group-text">
                  <i class="fa fa-image"></i>
                </span>
              </div>
            </div>
          </div>

          <div class="form-group">
            <select class="form-control" name="userType" id="userType">
              <option>Select User Type</option>
              <option>Admin</option>
              <option>Destination Manager</option>
              <option>MN Agent</option>
              <option>Collection Manager</option>
            </select>
          </div>

          <div class="form-group">
            <div class="input-group">
              <input type="password" class="form-control" placeholder="Password">
              <div class="input-group-append">
                <span class="input-group-text">
                  <i class="fa fa-unlock-alt"></i>
                </span>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="input-group">
              <input type="password" class="form-control" placeholder="Confirm Password">
              <div class="input-group-append">
                <span class="input-group-text">
                  <i class="fa fa-unlock-alt"></i>
                </span>
              </div>
            </div>
          </div>
          
          <div class="form-group">
            <button class="btn btn-primary submit-btn btn-block"><i class="menu-icon fas fa-save"></i> Save Collection Point</button>
          </div>
        
        </form>
      </div>
    </div>
  </div>
</div>


@endsection