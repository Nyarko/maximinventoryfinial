@extends('layouts.mn_agentlayout')

@section('title', 'All Items')

@section('content')

  <div class="card">
    <div class="card-body">
      <h2 class="card-title">
        <i class="menu-icon fas fa-th"></i>  Items
      </h2>
      <p class="card-description">
        <a href="{{ route('mn_addItems') }}" target="_SELF" class="btn btn-success float-none float-sm-right">Add Item
          <b> <i class="fa fa-plus-square"></i></b>
         </a>
      </p>
      <div class="table-responsive">
         <hr>
        <table class="display table table-bordered" id="myUsers">
          <thead>
            <tr>
              <th>User</th>
              <th>First name</th>
              <th>Progress</th>
              <th>Amount</th>
              <th>Deadline</th>
              <th style="text-align: center;">View</th>
              <th style="text-align: center;">Edit</th>
              <th style="text-align: center;">Delete</th>
            </tr>
          </thead>
          <tbody>
           
             <tr>
              <td class="py-1"><img src="img/cou12.jpg" alt="image" /></td>
              <td>Herman Beck</td>
              <td>JVHDskjd sdibsdkji</td>
              <td>$ 77.99</td>
              <td>May 15, 2018</td>
              <td style="text-align: center;"><a href="#"><i class="fa fa-eye"></i></a></td>
              <td style="text-align: center;"><a href="#"><i class="fa fa-pen"></i></a></td>
              <td style="text-align: center;"><a href="#"><i class="fa fa-trash"></i></a></td>
             </tr>

              <tr>
              <td class="py-1"><img src="img/cou12.jpg" alt="image" /></td>
              <td>Herman Beck</td>
              <td>JVHDskjd sdibsdkji</td>
              <td>$ 77.99</td>
              <td>May 15, 2018</td>
              <td style="text-align: center;"><a href="#"><i class="fa fa-eye"></i></a></td>
              <td style="text-align: center;"><a href="#"><i class="fa fa-pen"></i></a></td>
              <td style="text-align: center;"><a href="#"><i class="fa fa-trash"></i></a></td>
             </tr>

              <tr>
              <td class="py-1"><img src="img/cou12.jpg" alt="image" /></td>
              <td>Herman Beck</td>
              <td>JVHDskjd sdibsdkji</td>
              <td>$ 77.99</td>
              <td>May 15, 2018</td>
              <td style="text-align: center;"><a href="#"><i class="fa fa-eye"></i></a></td>
              <td style="text-align: center;"><a href="#"><i class="fa fa-pen"></i></a></td>
              <td style="text-align: center;"><a href="#"><i class="fa fa-trash"></i></a></td>
             </tr>

              <tr>
              <td class="py-1"><img src="img/cou12.jpg" alt="image" /></td>
              <td>Herman Beck</td>
              <td>JVHDskjd sdibsdkji</td>
              <td>$ 77.99</td>
              <td>May 15, 2018</td>
              <td style="text-align: center;"><a href="#"><i class="fa fa-eye"></i></a></td>
              <td style="text-align: center;"><a href="#"><i class="fa fa-pen"></i></a></td>
              <td style="text-align: center;"><a href="#"><i class="fa fa-trash"></i></a></td>
             </tr>

              <tr>
              <td class="py-1"><img src="img/cou12.jpg" alt="image" /></td>
              <td>Herman Beck</td>
              <td>JVHDskjd sdibsdkji</td>
              <td>$ 77.99</td>
              <td>May 15, 2018</td>
              <td style="text-align: center;"><a href="#"><i class="fa fa-eye"></i></a></td>
              <td style="text-align: center;"><a href="#"><i class="fa fa-pen"></i></a></td>
              <td style="text-align: center;"><a href="#"><i class="fa fa-trash"></i></a></td>
             </tr>

              <tr>
              <td class="py-1"><img src="img/cou12.jpg" alt="image" /></td>
              <td>Herman Beck</td>
              <td>JVHDskjd sdibsdkji</td>
              <td>$ 77.99</td>
              <td>May 15, 2018</td>
              <td style="text-align: center;"><a href="#"><i class="fa fa-eye"></i></a></td>
              <td style="text-align: center;"><a href="#"><i class="fa fa-pen"></i></a></td>
              <td style="text-align: center;"><a href="#"><i class="fa fa-trash"></i></a></td>
             </tr>

              <tr>
              <td class="py-1"><img src="img/cou12.jpg" alt="image" /></td>
              <td>Herman Beck</td>
              <td>JVHDskjd sdibsdkji</td>
              <td>$ 77.99</td>
              <td>May 15, 2018</td>
              <td style="text-align: center;"><a href="#"><i class="fa fa-eye"></i></a></td>
              <td style="text-align: center;"><a href="#"><i class="fa fa-pen"></i></a></td>
              <td style="text-align: center;"><a href="#"><i class="fa fa-trash"></i></a></td>
             </tr>

              <tr>
              <td class="py-1"><img src="img/cou12.jpg" alt="image" /></td>
              <td>Herman Beck</td>
              <td>JVHDskjd sdibsdkji</td>
              <td>$ 77.99</td>
              <td>May 15, 2018</td>
              <td style="text-align: center;"><a href="#"><i class="fa fa-eye"></i></a></td>
              <td style="text-align: center;"><a href="#"><i class="fa fa-pen"></i></a></td>
              <td style="text-align: center;"><a href="#"><i class="fa fa-trash"></i></a></td>
             </tr>

              <tr>
              <td class="py-1"><img src="img/cou12.jpg" alt="image" /></td>
              <td>Herman Beck</td>
              <td>JVHDskjd sdibsdkji</td>
              <td>$ 77.99</td>
              <td>May 15, 2018</td>
              <td style="text-align: center;"><a href="#"><i class="fa fa-eye"></i></a></td>
              <td style="text-align: center;"><a href="#"><i class="fa fa-pen"></i></a></td>
              <td style="text-align: center;"><a href="#"><i class="fa fa-trash"></i></a></td>
             </tr>

              <tr>
              <td class="py-1"><img src="img/cou12.jpg" alt="image" /></td>
              <td>Herman Beck</td>
              <td>JVHDskjd sdibsdkji</td>
              <td>$ 77.99</td>
              <td>May 15, 2018</td>
              <td style="text-align: center;"><a href="#"><i class="fa fa-eye"></i></a></td>
              <td style="text-align: center;"><a href="#"><i class="fa fa-pen"></i></a></td>
              <td style="text-align: center;"><a href="#"><i class="fa fa-trash"></i></a></td>
             </tr>

              <tr>
              <td class="py-1"><img src="img/cou12.jpg" alt="image" /></td>
              <td>Herman Beck</td>
              <td>JVHDskjd sdibsdkji</td>
              <td>$ 77.99</td>
              <td>May 15, 2018</td>
              <td style="text-align: center;"><a href="#"><i class="fa fa-eye"></i></a></td>
              <td style="text-align: center;"><a href="#"><i class="fa fa-pen"></i></a></td>
              <td style="text-align: center;"><a href="#"><i class="fa fa-trash"></i></a></td>
             </tr>

              <tr>
              <td class="py-1"><img src="img/cou12.jpg" alt="image" /></td>
              <td>Herman Beck</td>
              <td>JVHDskjd sdibsdkji</td>
              <td>$ 77.99</td>
              <td>May 15, 2018</td>
              <td style="text-align: center;"><a href="#"><i class="fa fa-eye"></i></a></td>
              <td style="text-align: center;"><a href="#"><i class="fa fa-pen"></i></a></td>
              <td style="text-align: center;"><a href="#"><i class="fa fa-trash"></i></a></td>
             </tr>

              <tr>
              <td class="py-1"><img src="img/cou12.jpg" alt="image" /></td>
              <td>Herman Beck</td>
              <td>JVHDskjd sdibsdkji</td>
              <td>$ 77.99</td>
              <td>May 15, 2018</td>
              <td style="text-align: center;"><a href="#"><i class="fa fa-eye"></i></a></td>
              <td style="text-align: center;"><a href="#"><i class="fa fa-pen"></i></a></td>
              <td style="text-align: center;"><a href="#"><i class="fa fa-trash"></i></a></td>
             </tr>

              <tr>
              <td class="py-1"><img src="img/cou12.jpg" alt="image" /></td>
              <td>Herman Beck</td>
              <td>JVHDskjd sdibsdkji</td>
              <td>$ 77.99</td>
              <td>May 15, 2018</td>
              <td style="text-align: center;"><a href="#"><i class="fa fa-eye"></i></a></td>
              <td style="text-align: center;"><a href="#"><i class="fa fa-pen"></i></a></td>
              <td style="text-align: center;"><a href="#"><i class="fa fa-trash"></i></a></td>
             </tr>

              <tr>
              <td class="py-1"><img src="img/cou12.jpg" alt="image" /></td>
              <td>Herman Beck</td>
              <td>JVHDskjd sdibsdkji</td>
              <td>$ 77.99</td>
              <td>May 15, 2018</td>
              <td style="text-align: center;"><a href="#"><i class="fa fa-eye"></i></a></td>
              <td style="text-align: center;"><a href="#"><i class="fa fa-pen"></i></a></td>
              <td style="text-align: center;"><a href="#"><i class="fa fa-trash"></i></a></td>
             </tr>

            
                                  
          </tbody>
        </table>
      </div>
    </div>
  </div>
@endsection