@extends('layouts.pageslayouts')


@section('title', 'Collection Point')

@section('content')


<style type="text/css">
  
.mapouter{
  text-align:center;
  height:370px
  width:100%;
 }
                    
#gmap_canvas{
   overflow:hidden;
   background:none!important;
   height:370px;
   width:100%;
 }

</style>
       

       <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 130px">
        <div class="container">

          <div class="row">
            <div class="item">
                @if($collectionpoint->map == null)
                   <center><h3>Location on Map Not Set</h3></center>
                @else
                  <div class="mapouter">
                     <div class="gmap_canvas">
                      <iframe id="gmap_canvas" src="{{ $collectionpoint->map}}" frameborder="0" scrolling="no" marginheight="0" marginwidth="0">
                      </iframe>
                    </div>
                  </div>
                @endif                            
            </div>
          </div>
            
             
             <hr>
               
             <div class="row">
                <div class="item">
                 <h3 class="text-center">Collection Manager's Details</h3><hr>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <img class="img-thumbnail" src="{{ asset('../img/users/'. $collectionpoint->picture) }}">
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                      <table class="table table-hover table-responsive" style="font-size: 18px">
                         <tbody>
                           <tr>
                             <th><i class="fa fa-user"></i></th>
                             <th>Collection Manager Name</th>
                             <td>{{ $collectionpoint->fname }} {{ $collectionpoint->surname }}</td>
                           </tr>
                           <tr>
                             <th><i class="fa fa-envelope"></i></th>
                             <th>Email</th>
                             <td>{{ $collectionpoint->email }}</td>
                           </tr>
                            <tr>
                             <th><i class="fa fa-phone"></i> </th>
                             <th>Contact</th>
                             <td>{{ $collectionpoint->contact }}</td>
                           </tr>
                             <tr>
                               <th><i class="fa fa-snowflake"></i></th>
                               <th>Description</th>
                               <td>{{ $collectionpoint->description }}</td>
                             </tr>
                         </tbody>
                      </table>
                    </div>    
                </div>
             </div>
 
               <hr>
               
              <div class="row">
              <div class="item">
              <h3 class="text-center">Collection Point Details</h3><hr>
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                 <img class="img-thumbnail text-center" src="{{ asset('../img/map/'. $collectionpoint->image) }}">
              </div>
              <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                <table class="table table-hover table-responsive" style="font-size: 18px">
                   <tbody>
                     <tr>
                       <th><i class="fa fa-compass"></i></th>
                       <th>Collection Point Name</th>
                       <td>{{ $collectionpoint->collectionpoint_name }}</td>
                     </tr>
                      <tr>
                       <th><i class="fa fa-building"></i></th>
                       <th>Street Address</th>
                       <td>{{ $collectionpoint->street_address}}</td>
                     </tr>
                      <tr>
                       <th><i class="fa fa-bookmark"></i></th>
                       <th>Address Number</th>
                       <td>{{ $collectionpoint->number_address}}</td>
                     </tr>
                      <tr>
                       <th><i class="fa  fa-address-book"></i></th>
                       <th>Postal Address</th>
                       <td>{{ $collectionpoint->postal_address}}</td>
                     </tr>
                     <tr>
                       <th><i class="fa fa-circle"></i></th>
                       <th>Country</th>
                       <td>{{ $collectionpoint->country}}</td>
                     </tr>
                    
                   </tbody>

                </table>
               </div>
              </div>
              </div>
              <hr>
      
           </div>     
        </div>
      </div>

@endsection
