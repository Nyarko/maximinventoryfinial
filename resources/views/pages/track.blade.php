@extends('layouts.pageslayouts')


@section('title', 'Home')

@section('content')


  <section id="menu" class="menu">
                <div class="container">
                    <header class="text-center">
                        <h2>ENTER YOUR DEVICE ID TO TRACK</h2>
                    </header>

                          <div class="panel panel-primary">
                              <div class="panel-heading">
                                  <div class="col-lg-2 col-md-2 col-sm-1 col-xs-0"></div>
                                  <div class="col-lg-8 col-md-8 col-sm-10 col-xs-12">
                                     <form class="form-inline" style="text-align: center;">
                                      <div class="form-group">
                                          <input type="text" name="" class="form-control" placeholder="Enter device id to track">
                                          <input type="button" name="" class="btn btn-unique" value="track">
                                      </div>
                                     </form>
                                  </div>
                                  <div class="col-lg-2 col-md-2 col-sm-1 col-xs-0"></div>
                                  <div class="clearfix"></div>
                              </div>
                              <div class="panel-body"> 
                                  
                              </div>
                          </div>

                   
                            </div><!-- End Drinks Panel-->
                        </div>
                    </div>
                </div>
            </section>

@endsection
