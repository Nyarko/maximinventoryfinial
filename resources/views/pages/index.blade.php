@extends('layouts.pageslayouts')


@section('title', 'Home')

@section('content')


@include('layouts.carousel')

<!-- About Section -->
<section id="about" class="dishes">
     <div class="container text-center">
        <header>
            <h3>OUR AGENTS COLLECTION POINTS</h3>
            <h3>Our agents Information and Location are provided below</h3>
        </header>
        <!-- Set up your HTML -->
        <div class="owl-carousel owl-theme">
          
          @foreach($collectionpoint as $collection)

              <div class="dish">
                <div class="profile">
                    <img class="img-thumbnail" src="{{ asset('../img/map/'. $collection->image) }}">
                    <div class="price">
                        <i class="fas fa-phone">  {{ $collection->contact}} </i>
                    </div>
                </div>
                <div class="text">
                    <h4>{{ $collection->country }}</h4>
                    <h5><i class="fas fa-location-arrow"></i> {{ $collection->collectionpoint_name }}</h5><br>
                    <p>
                      <table class="table table-striped table-responsive">
                          <tbody>
                            <tr>
                              <th width="130px"><i class="far fa-user"></i> Agent Name</th>
                              <td>{{ $collection->surname }} {{ $collection->fname}}</td>
                            </tr>
                            <tr>
                              <th><i class="far fa-envelope"></i> Email</th>
                              <td>{{ $collection->email}}</td>
                            </tr>
                            <tr rowspan="3" >
                              <th><i class="far fa-address-card"></i> Address</th>
                              <td>{{ $collection->street_address }} {{ $collection->number_address }} {{ $collection->postal_address }}</td>
                            </tr>
                            <tr>
                              <td style="text-align:center" colspan="2">
                                <a href="{{ route('viewcollection', [$collection->id]) }}" class="btn btn-primary btn-xs">More</a>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                    </p>
                </div>
            </div>

          @endforeach

        </div>



        <div class="row">
          <div class="container-fluid">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="item">
                   
                     <h4>Our Location in Ghana</h4>

                      <div class="mapouter">
                        <div class="gmap_canvas">
                          <iframe width="900" height="500" id="gmap_canvas" src="https://maps.google.com/maps?q=Maxim%20Nyansa&t=k&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="yes" marginheight="0" marginwidth="0"></iframe>
                        </div>
                         <style>.mapouter{text-align:center;height:500px;width:100%;}.gmap_canvas {overflow:hidden;background:none!important;height:500px;width:100%px;}
                         </style>
                       </div>
                                            
                </div>
            </div>
           

            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="item">
                    
                  <h4>Our Location in Netherland</h4>
                  <div class="mapouter"><div class="gmap_canvas"><iframe width="600" height="500" id="gmap_canvas" src="https://maps.google.com/maps?q=Kalmoes%2018%20Reeuwijk&t=k&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe></div><style>.mapouter{text-align:center;height:500px;width:100%;}.gmap_canvas {overflow:hidden;background:none!important;height:500px;width:100%;}</style></div>
                    
                </div>
            </div>
        </div>
      </div>
  
    </div>
</section>
<!-- End About Section -->

@endsection
