<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <!-- Responsive meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Compatibility meta -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Description meta -->
        <meta name="description" content="...">
        <!-- Author meta -->
        <meta name="author" content="Bootstrap Temple">

        <!-- Page Title -->
        <title>Computer4Schools | @yield('title')</title>
        <!-- Favicon -->
        <link rel="shortcut icon" href="{{ asset('../img/favicon.ico') }}">

        @include('partials.pagestyle')

    </head>
    <body>

        <div class="page-holder">
            
            @include('partials.pages._pagesheader')

           
            @yield('content')


            @include('partials.pages._pagesfooter')
            
        </div>
         
         @include('partials.pagescript')

    </body>
</html>
