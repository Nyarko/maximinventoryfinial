<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Computer4Schools | @yield('title')</title>


  @include('partials.adminstyles')

  <!-- endinject -->
  <link rel="shortcut icon" href="{{ asset('../img/favicon.ico')}}">
</head>

<body>
     <div class="container-scroller">
     
      @include('partials.mn_agent._mn_agentheader')
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
     
       @include('partials.mn_agent._mn_agentsidebar')

       <div class="main-panel">

       @yield('content')

       
       @include('partials.mn_agent._mn_agentfooter')

       </div>
    <!-- page-body-wrapper ends -->
    </div>
  <!-- container-scroller -->

 </div>

 @include('partials.adminscript')

</body>
</html>