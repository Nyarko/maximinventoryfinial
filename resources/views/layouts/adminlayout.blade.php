<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Computer4Schools | @yield('title')</title>


  @include('partials.adminstyles')

  <!-- endinject -->
  <link rel="shortcut icon" href="{{ asset('../img/favicon.ico')}}">
</head>

<body>
     <div class="container-scroller">
     
      @include('partials.admin._adminheader')
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
     
       @include('partials.admin._adminsidebar')

       <div class="main-panel">

       @yield('content')

       
       @include('partials.admin._adminfooter')

       </div>
    <!-- page-body-wrapper ends -->
    </div>
  <!-- container-scroller -->

 </div>

 @include('partials.adminscript')

</body>
</html>