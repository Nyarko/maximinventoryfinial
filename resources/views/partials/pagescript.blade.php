
<script src="{{ asset('js/jquery.min.js')}}" defer></script>
<script src="{{ asset('js/bootstrap.min.js')}}" defer></script>

<script src="{{ asset('js/jquery.ba-cond.min.js') }}" defer></script>
<script src="{{ asset('js/jquery.slitslider.min.js') }}" defer></script>
<script src="{{ asset('js/owl.carousel.min.js') }}" defer></script>
<script src="{{ asset('js/lightbox.min.js') }}" defer></script>
<script src="{{ asset('js/datepicker.min.js') }}" defer></script>
<script src="{{ asset('js/datepicker.en.min.js') }}" defer></script>
<script src="{{ asset('js/timepicki.min.js') }}" defer></script>
<script src="{{ asset('js/jquery.validate.min.js') }}" defer></script>
<script src="{{ asset('js/smooth.scroll.min.js') }}" defer></script>
<script src="{{ asset('js/script.js') }}" defer></script>