<!-- Navbar -->
<header class="header">
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <a href="{{ route('indexpage') }}" class="navbar-brand" style="letter-spacing: 0px;">
                    <img src="{{ asset('../img/logo.png') }}" alt="Logo" width="50" style="float: left;">
                    <span style="font-size: 13px;">Computer</span><span style="font-weight: bolder; font-size: 30px;">4</span><span style="font-size: 20px;">Schools</span>
                </a>
                <div class="navbar-buttons">
                    <button type="button" data-toggle="collapse" data-target=".navbar-collapse" class="navbar-toggle navbar-btn">Menu <i class="fas fa-bars"></i></button>
                </div>
            </div>
            <div id="navigation" class="collapse navbar-collapse navbar-right">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="{{ route('indexpage') }}"><i class="fas fa-home"></i> Home</a></li>
                    <li><a href="{{ route('trackpage') }}"><i class="fab fa-servicestack"></i> Track</a></li>
                    <li><a href="{{ route('login')}}" target="_SELF"><i class="fas fa-sign-in-alt"></i> Login</a></li>
                </ul>
            </div>
        </div>
    </nav>
</header>
<!-- End Navbar -->