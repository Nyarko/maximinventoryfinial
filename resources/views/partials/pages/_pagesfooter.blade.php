<!-- Footer -->
<footer>
    <div class="copyrights">
        <div class="container">
         <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
                <div class="header">
                    <img src="{{ asset('../img/logo-footer.png') }}" alt="Image" width="100">
                </div>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5">
                <ul class="social list-unstyled list-inline">
                    <li><a href="#" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
                    <li><a href="#" target="_blank" title="Twitter"><i class="fab fa-twitter"></i></a></li>
                    <li><a href="#" target="_blank" title="Instagram"><i class="fab fa-instagram"></i></a></li>
                </ul>
            </div>
        </div>
     </div>
   </div>
</footer>
<!-- End Footer -->