<!-- partial:partials/_sidebar.html -->
<nav class="sidebar sidebar-offcanvas" id="sidebar">
  <ul class="nav">
    <li class="nav-item nav-profile">
      <div class="nav-link">
        <a href="{{ route('collectionIndex') }}" class="btn btn-secondary btn-block">
         <i class="menu-icon mdi mdi-home"></i>
         <span class="menu-title" style="font-size: 16px; font-weight: bolder;">Dashboard</span>
         <!--  <i class="mdi mdi-plus"></i> -->
        </a>
      </div>
    </li>

     <li class="nav-item">
      <a class="nav-link" href="{{ route('collection_allDonators') }}">
        <i class="menu-icon fa fa-users"></i>
        <span class="menu-title">Donators</span>
      </a>
     </li>

     <li class="nav-item">
      <a class="nav-link" href="{{ route('collection_allItems') }}">
        <i class="menu-icon fa fa-th"></i>
        <span class="menu-title">Items</span>
      </a>
     </li>

  </ul>
</nav>