 

 <script type="text/javascript" src="{{ asset('js/jquery.min.js') }}" defer></script>
 <script type="text/javascript" src="{{ asset('js/datatables.js') }}" defer></script>
 
 <!-- container-scroller -->
 <script src="{{ asset('js/vendor.bundle.base.js') }}" defer></script>
 <script src="{{ asset('js/vendor.bundle.addons.js') }}" defer></script>
 <!-- endinject -->
 <!-- inject:js -->
 <script src="{{ asset('js/off-canvas.js') }}" defer></script>
 
  <!-- endinject -->

 <script src="{{ asset('js/dashboard.js') }}" defer></script>

 <script type="text/javascript" src="{{ asset('js/custom.js') }}" defer></script>
<script src="{{ asset('js/misc.js') }}" defer></script>