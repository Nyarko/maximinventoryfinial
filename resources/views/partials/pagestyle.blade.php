 <!-- Bootstrap -->
<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}" defer>
<!--   <link rel="stylesheet" href="css/font-awesome.min.css"> -->
<link href="{{ asset('css/all.css') }}" rel="stylesheet" defer> <!--load all styles -->
<!-- Slider -->
<link rel="stylesheet" href="{{ asset('css/slider.css') }}" defer>
<!-- Lightbox Pop up -->
<link rel="stylesheet" href="{{ asset('css/lightbox.min.css') }}" defer>
<!-- Datepicker -->
<link rel="stylesheet" href="{{ asset('css/datepicker.min.css') }}" defer>
<!-- Datepicker -->
<link rel="stylesheet" href="{{ asset('css/timepicki.min.css') }}" defer>
<!-- Owl Carousel -->
<link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}" defer>
<!-- Custom CSS -->
<link rel="stylesheet" href="{{ asset('css/style.default.css') }}" defer>

{{--  <link rel="stylesheet" href="{{ asset('css/custom.css') }}"> --}}
<!-- Modernizr -->
<script type="text/javascript" src="{{ asset('js/modernizr.custom.79639.min.js') }}" defer></script>

