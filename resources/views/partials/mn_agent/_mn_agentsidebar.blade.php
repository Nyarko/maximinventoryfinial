<!-- partial:partials/_sidebar.html -->
<nav class="sidebar sidebar-offcanvas" id="sidebar">
  <ul class="nav">
    <li class="nav-item nav-profile">
      <div class="nav-link">
        <a href="{{ route('mn_agentindex') }}" class="btn btn-secondary btn-block">
         <i class="menu-icon mdi mdi-home"></i>
         <span class="menu-title" style="font-size: 14px; font-weight: bolder;">Dashboard</span>
         <!--  <i class="mdi mdi-plus"></i> -->
        </a>
      </div>
    </li>

    
     <li class="nav-item">
      <a class="nav-link" href="{{ route('mn_allschools') }}">
        <i class="menu-icon fa fa-graduation-cap"></i>
        <span class="menu-title">Schools</span>
      </a>
     </li>

     <li class="nav-item">
      <a class="nav-link" href="{{ route('mn_allDonators') }}">
        <i class="menu-icon fa fa-users"></i>
        <span class="menu-title">Donators</span>
      </a>
     </li>

    <li class="nav-item">
      <a class="nav-link" href="{{ route('mn_allItems') }}">
        <i class="menu-icon fa fa-th"></i>
        <span class="menu-title">Items</span>
      </a>
    </li>

  </ul>
</nav>