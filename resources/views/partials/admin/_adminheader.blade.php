 <!-- partial:partials/_navbar.html -->
    <nav class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row">

      <div class="navbar-brand-wrapper d-flex align-items-top justify-content-center">
        <a class="navbar-brand brand-logo" href="{{ route('adminindex') }}" style="color: #7fb401">
          <img src="{{ asset('img/logo.png') }}" alt="Logo" style="float: left; width: 50px">
         <span style="font-size: 13px;">Computer</span><span style="font-weight: bolder; font-size: 30px;">4</span><span style="font-size: 20px;">Schools</span>
        </a>
        <a class="navbar-brand brand-logo-mini" href="{{ route('adminindex') }}" style="color: #7fb401">
           <span style="font-size: 18px; font-weight: bolder;">C</span><span style="font-weight: bolder; font-size: 30px;">4</span><span style="font-size: 20px; font-weight: bolder;">S</span>
        </a>
      </div>

      <div class="navbar-menu-wrapper d-flex align-items-center">
        <ul class="navbar-nav navbar-nav-left header-links d-none d-md-flex">
          <li class="nav-item">
            <a href="#" class="nav-link">
            </a>
          </li>
          <li class="nav-item active">
            <a href="#" class="nav-link"></a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link"></a>
          </li>
        </ul>

        <ul class="navbar-nav navbar-nav-right">
          <li class="nav-item dropdown">
            <!-- <a class="nav-link count-indicator dropdown-toggle" href="#" data-toggle="dropdown" aria-expanded="false">
              <i class="mdi mdi-file-document-box"></i>
              <span class="count">7</span>
            </a> -->
          </li>

          <li class="nav-item dropdown">
           <!--  <a class="nav-link count-indicator dropdown-toggle" href="#" data-toggle="dropdown">
              <i class="mdi mdi-bell"></i>
              <span class="count">4</span>
            </a> -->
          </li>

          <li class="nav-item dropdown d-none d-xl-inline-block">
            <a class="nav-link dropdown-toggle" id="UserDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
              <span class="profile-text">Hello, Nyarko Isaac</span>
              <img class="img-xs rounded-circle" src="{{ asset('img/loc4.jpg') }}" alt="Profile image">
            </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown">
              <a class="dropdown-item mt-2">
                <i class="fa fa-user"></i> View Profile
              </a>
              <a class="dropdown-item">
                 <i class="fa fa-sun"></i>  Change Password
              </a>
              <a class="dropdown-item" href="{{ route('logout')}}">
                 <i class="fa fa-sign"></i>  Sign Out
              </a>
            </div>
          </li>
        </ul>

        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
          <span class="mdi mdi-menu"></span>
        </button>
        
      </div>
    </nav>


