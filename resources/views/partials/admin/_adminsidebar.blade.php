<!-- partial:partials/_sidebar.html -->
<nav class="sidebar sidebar-offcanvas" id="sidebar">
  <ul class="nav">
    <li class="nav-item nav-profile">
      <div class="nav-link">
        <a href="{{ route('adminindex') }}" class="btn btn-secondary btn-block">
         <i class="menu-icon mdi mdi-home"></i>
         <span class="menu-title" style="font-size: 16px; font-weight: bolder;">Dashboard</span>
         <!--  <i class="mdi mdi-plus"></i> -->
        </a>
      </div>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="{{ route('alluser') }}">
        <i class="menu-icon text-success fa fa-user"></i>
        <span class="menu-title">Users</span>
      </a>
    </li>

     <li class="nav-item">
         <a class="nav-link" data-toggle="collapse" href="#location" aria-expanded="false" aria-controls="collection-point">
          <i class="menu-icon fa fa-road"></i>
          <span class="menu-title">Location</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="location">
          <ul class="nav flex-column sub-menu">
            
          <li class="nav-item">
            <a class="nav-link" href="{{ route('allregion') }}">
              <i class="menu-icon fa fa-globe"></i>
              <span class="menu-title">Regions</span>
            </a>
          </li>

           <li class="nav-item">
            <a class="nav-link" href="{{ route('allDistrict') }}">
              <i class="menu-icon mdi fa fa-address-card"></i>
              <span class="menu-title">Districts</span>
            </a>
           </li>

           <li class="nav-item">
            <a class="nav-link" href="{{ route('allCountry')}}">
              <i class="menu-icon fa fa-th-list"></i>
              <span class="menu-title">Country</span>
            </a>
           </li>

          </ul>
        </div>
      </li>

     <li class="nav-item">
      <a class="nav-link" href="{{ route('allschools') }}">
        <i class="menu-icon text-primary fa fa-graduation-cap"></i>
        <span class="menu-title">Schools</span>
      </a>
     </li>

    
     <li class="nav-item">
      <a class="nav-link" href="{{ route('alldonators') }}">
        <i class="menu-icon text-warning fa fa-users"></i>
        <span class="menu-title">Donators</span>
      </a>
     </li>

      <li class="nav-item">
         <a class="nav-link" data-toggle="collapse" href="#item" aria-expanded="false" aria-controls="item">
          <i class="menu-icon text-info fa fa-th"></i>
          <span class="menu-title">Items</span>
          <i class="menu-arrow"></i>
        </a>

         <div class="collapse" id="item">

          <ul class="nav flex-column sub-menu">
            <li class="nav-item">
              <a class="nav-link" href="{{ route('all-items') }}"><i class="menu-icon mdi mdi-nfc-variant"></i> All Items</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ route('allItemsStatus') }}"><i class="menu-icon mdi mdi-spotify"></i> Item Status</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ route('allCategories') }}"><i class="menu-icon mdi mdi-google-circles-communities"></i> Item Categories</a>
            </li>
          </ul>
        </div>
      </li>


      <li class="nav-item">
         <a class="nav-link" data-toggle="collapse" href="#collection-point" aria-expanded="false" aria-controls="collection-point">
          <i class="menu-icon text-danger fa fa-compass"></i>
          <span class="menu-title">Collection Point</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="collection-point">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item">
              <a class="nav-link" href="{{ route('allCollectionpoint') }}"><i class="menu-icon fa fa-map"></i>Collection Point</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ route('allCollectionPointStatus') }}"><i class="menu-icon fa fa-bullseye"></i> Collection Status</a>
            </li>
          </ul>
        </div>
      </li>

      <li class="nav-item">
         <a class="nav-link" data-toggle="collapse" href="#desti" aria-expanded="false" aria-controls="desti">
          <i class="menu-icon text-info fa fa-map-marker"></i>
          <span class="menu-title">Destination Point</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="desti">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item">
              <a class="nav-link" href="{{ route('all-destination-points') }}"><i class="menu-icon mdi mdi-code-equal"></i>Destination Points</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ route('allDestinationPointStatus') }}"><i class="menu-icon mdi  mdi-backup-restore"></i> Destination P. Status</a>
            </li>
          </ul>
        </div>
      </li>

       <li class="nav-item" style="margin-bottom: 50px">
         <a class="nav-link" data-toggle="collapse" href="#project" aria-expanded="false" aria-controls="project">
          <i class="menu-icon fa fa-cubes"></i>
          <span class="menu-title">Project Plan</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="project">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item">
              <a class="nav-link" href="{{ route('project-index') }}"><i class="menu-icon fa fa-plus-square"></i>Projects</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ route('all-categories-donated')}}"><i class="menu-icon fa fa-plus-circle"></i>Allocate Items</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#"><i class="menu-icon fa fa-plus"></i>Project Statistics</a>
            </li>
          </ul>
        </div>
      </li>

      
  </ul>
</nav>

