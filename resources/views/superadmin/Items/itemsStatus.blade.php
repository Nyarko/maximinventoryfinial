@extends('layouts.adminlayout')

@section('title', 'All Items Status')

@section('content')

  <div class="card">
    <div class="card-body">
      <h2 class="card-title">
        <i class="menu-icon fas fa-th"></i>  All Items Status
      </h2>

      <!-- error message display div -->
      <div>
        @if(Session::has('create-item-status-success'))

        <div class="alert alert-success alert-dismissible" role="alert" id="myAlert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <strong>Success!</strong> {{ Session::get('create-item-status-success') }}
        </div>

        @endif

        @if(Session::has('update-item-status-success'))

        <div class="alert alert-success alert-dismissible" role="alert" id="myAlert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <strong>Success!</strong> {{ Session::get('update-item-status-success') }}
        </div>

        @endif

        @if(Session::has('delete-item-status-success'))

        <div class="alert alert-success alert-dismissible" role="alert" id="myAlert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>

          <strong>Success!</strong>{{ Session::get('delete-item-status-success') }}

        </div>

        @endif

      </div>

      <!-- end of error message display div -->



      <p class="card-description">
        <a href="{{ route('addItemsStatus') }}" target="_SELF" class="btn btn-success float-none float-sm-right">Add Item Status
          <b> <i class="fa fa-plus-square"></i></b>
         </a>
      </p>
      <div class="table-responsive">
         <hr>
        <table class="table table-striped" id="myTable">
          <thead>
            <tr>
              <th>#</th>
              <th>Item Status</th>
              <th>Created On</th>
              <th style="text-align: center">Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach($allItemStatus as $index=>$itemstatus)
            <tr>
              <td>{{ $index+1}}</td>
              <td>{{ $itemstatus->item_status_name }}</td>
              <td>{{ date('M j, Y', strtotime($itemstatus->created_at )) }}</td>
              <td colspan="2" style="text-align: center">
                <a title="Edit Item Status" style="color: green" href="{{ route('edit-item-status',[$itemstatus->id])}}"><i class="fa fa-pen icon-sm"></i></a> | <a href="{{ route('delete-item-status',[$itemstatus->id])}}" onclick="return confirm('Are you sure you want to Delete this Item Status ?')" title="Delete Item Status" style="color: red"><i class="fa fa-trash icon-sm"></i></a></td>
            </tr>

            @endforeach

          </tbody>
        </table>
      </div>
    </div>
  </div>
@endsection