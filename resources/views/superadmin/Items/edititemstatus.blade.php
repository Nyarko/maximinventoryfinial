@extends('layouts.adminlayout')

@section('title', 'Add Item Status')

@section('content')

  <div class="content-wrapper d-flex align-items-center auth register-bg-1 theme-one">
  <div class="row w-100">
    <div class="col-lg-5 col-md-5 col-sm-8 col-xs-12 mx-auto">
      <div class="auto-form-wrapper" style="padding-top: 50px;padding-bottom: 50px">
      <h4 class="text-center mb-4"><i class="mdi  mdi mdi-spotify"></i> Add Item Status</h4>

        <form class="form" method="post" action="{{ route('post-edit-item-status', [$itemstatus->id])}}">
          {{ csrf_field() }}
          <input type="hidden" name="itemstatus_id" value="{{$itemstatus->id}}">
          <div class="form-group">
            <div class="input-group">
              <input type="text" required="required" class="form-control" value="{{ $itemstatus->item_status_name }}" name="itemstatusname" id="itemstatusname"> 
              <div class="input-group-append">
                <span class="input-group-text">
                  <i class="mdi  mdi mdi-spotify"></i>
                </span>
              </div>
            </div>
          </div>
          <div class="form-group">
            <button class="btn btn-success submit-btn btn-block"><i class="menu-icon fa fa-save"></i>Edit Item Status</button>
          </div>
        
        </form>

      </div>
    </div>
  </div>
</div>

@endsection