@extends('layouts.adminlayout')

@section('title', 'Add Item')

@section('content')

  <div class="content-wrapper d-flex align-items-center auth register-bg-1 theme-one">
  <div class="row w-100">
    <div class="col-lg-6 col-md-6 col-sm-8 mx-auto">
      <div class="auto-form-wrapper" style="padding-top: 50px;padding-bottom: 50px">
       <h4 class="text-center mb-4"><i class="menu-icon fa fa-th"></i> Add Item</h4>
        <form action="{{ route('post-create-item')}}" method="post" enctype="multipart/form-data">
          {{ csrf_field()}}

          <div class="form-group">
            <div class="input-group">
              <input type="text" class="form-control" name="itemname" id="itemname" placeholder="Item Name Eg. Acer Mouse">
              <div class="input-group-append">
                <span class="input-group-text">
                  <i class="fa fa-user"></i>
                </span>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="input-group">
              <input type="text" class="form-control" name="item_quantity" placeholder="Quantity Eg. 200">
              <div class="input-group-append">
                <span class="input-group-text">
                  <i class="fa fa-heart"></i>
                </span>
              </div>
            </div>
          </div>

         <div class="form-group">
            <select class="form-control" name="categoryid" id="categoryid">
              <option selected="selected" value=""> -- Select Category of Item -- </option>
              @foreach($categories as $category)
              <option value="{{ $category->id }}">{{ $category->name }}</option>
              @endforeach
            </select>
          </div>

          <div class="form-group">
            <select class="form-control" name="collectionpointid" id="collectionpointid">
              <option selected="selected" value=""> -- Select Collection Point -- </option>
              @foreach($collectionpoints as $collectionpoint)
              <option value="{{ $collectionpoint->id }}">{{ $collectionpoint->collectionpoint_name }}</option>
              @endforeach
            </select>
          </div>

          <div class="form-group">
            <select class="form-control" name="donatorid" id="donatorid">
              <option selected="selected" value=""> -- Donated By -- </option>
              @foreach($donors as $donor)
              <option value="{{ $donor->id }}">{{ $donor->name }}</option>
              @endforeach
            </select>
          </div>

                   
          <div class="form-group">
            <button class="btn btn-success submit-btn btn-block"><i class="menu-icon fas fa-save"></i> Create Item</button>
          </div>
        
        </form>
      </div>
    </div>
  </div>
</div>


@endsection