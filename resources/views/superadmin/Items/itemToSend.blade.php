@extends('layouts.adminlayout')

@section('title', 'All Items')

@section('content')

  <div class="card">
    <div class="card-body">
      <h2 class="card-title">
        <i class="menu-icon fas fa-th"></i> Send Message to Donor
      </h2>
      <!-- error message display div -->
      <div>
        @if(Session::has('message'))
        <div class="alert alert-success alert-dismissible" role="alert" id="myAlert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <strong>Success!</strong> {{ Session::get('message') }}
        </div>
        @endif

        @if(Session::has('error'))
        <div class="alert alert-danger alert-dismissible" role="alert" id="myAlert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <strong>Success!</strong> {{ Session::get('error') }}
        </div>
        @endif


       <div id="myModal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
         <div class="modal-dialog modal-sm">
           <div class="modal-content" style="background-color:#FFFFFF">
             <div class="modal-header">
               <h4 class="modal-title">Sending Mail...</h4>
             </div>
             <div class="modal-body">
               <p style="text-align:center"><img src="{{ asset('img/loading.gif')}}" alt="image"></p>
             </div>

           </div>
         </div>
       </div>

      </div>

      <!-- end of error message display div -->
      <p class="card-description">
        <a href="{{ route('all-items') }}" target="_SELF" class="btn btn-danger float-none float-sm-right"><< Back</a>
      </p>
      <table>
        <tbody>
          <tr>
            <form action="{{ route('send') }}" method="post">
              {{ csrf_field() }}
               <input type="hidden" name="email" value="{{ $details->donatoremail }}">
               <input type="hidden" name="gen_id" value="{{ $details->gen_id }}">
               <button class="btn btn-success float-sm-center btn-xl" type="submit" name="send" data-toggle="modal" data-target="#myModal">
                 <i class="fa fa-envelope icon-sm"></i> Send Mail to Donor
               </button>
            </form>
          </tr>
        </tbody>
      </table>
      <div class="table-responsive">
         <hr>
        <table class="table table-striped">
          <thead>
            <tr>
              <th>#</th>
              <th>Item ID</th>
              <th>Status</th>
            </tr>
          </thead>
          <tbody>
           @foreach ($items as $key => $item)
           <tr>
             <td>{{$key+1}}</td>
             <td>{{strtoupper($item->itemid)}}</td>
              @if($item->item_status == null)
                 <td>NOT AVAILABLE</td>
              @else
                 <td>{{$item->item_status}}</td>
              @endif
           </tr>
           @endforeach
          </tbody>
        </table>
      </div>
      <hr>
      <table>
        <tbody>
          <tr>
            <form action="{{ route('send') }}" method="post">
              {{ csrf_field() }}
               <input type="hidden" name="email" value="{{ $details->donatoremail }}">
               <input type="hidden" name="gen_id" value="{{ $details->gen_id }}">
               <button class="btn btn-success float-sm-center btn-xl" type="submit" name="send" data-toggle="modal" data-target="#myModal">
                 <i class="fa fa-envelope icon-sm"></i> Send Mail to Donor
               </button>
            </form>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
@endsection
