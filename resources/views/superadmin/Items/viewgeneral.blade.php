@extends('layouts.adminlayout')

@section('title', 'All Items')

@section('content')

<style type="text/css">
    #pageloader
{
  background: rgba( 255, 255, 255, 0.8 );
  display: none;
  height: 100%;
  position: fixed;
  width: 100%;
  z-index: 9999;
}

#pageloader img
{
  left: 50%;
  margin-left: -32px;
  margin-top: -32px;
  position: absolute;
  top: 50%;
}
</style>

<div id="pageloader">
   <img src="{{ asset('img/loader.gif') }}" alt="processing..." />
</div>



<div class="card">
  <div class="card-body">
    <h2 class="card-title">
      <i class="menu-icon fas fa-th"></i>  All Items under General Serial <span style="font-size: 18px;"> <b class="badge badge-danger">{{ $General_Serial }}</b></span>
    </h2>

    <!-- error message display div -->
    <div>
      @if(Session::has('create-item-success'))

      <div class="alert alert-success alert-dismissible" role="alert" id="myAlert">
       <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
        <strong>Success!</strong> {{ Session::get('create-item-success') }}
      </div>

      @endif

      @if(Session::has('update-item-success'))

      <div class="alert alert-success alert-dismissible" role="alert" id="myAlert">
       <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
        <strong>Success!</strong> {{ Session::get('update-item-success') }}
      </div>

      @endif

      @if(Session::has('delete-item-success'))

      <div class="alert alert-success alert-dismissible" role="alert" id="myAlert">
       <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>

        <strong>Success!</strong>{{ Session::get('delete-item-success') }}

      </div>

      @endif

      @if(Session::has('no-item-selected'))

      <div class="alert alert-danger alert-dismissible" role="alert" id="myAlert">
       <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>

        <strong>Ooops!</strong>{{ Session::get('no-item-selected') }}

      </div>

      @endif

      @if (session('error'))
      <div class="alert alert-danger" id="myAlert">
        <p>{{ session('error') }}</p>
      </div>
      @endif

      @if(Session::has('item-project'))

      <div class="alert alert-success alert-dismissible" role="alert" id="myAlert">
       <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
        <strong>Success!</strong> {{ Session::get('item-project') }}
      </div>

      @endif

      @if(Session::has('multiplestatusupdate'))

      <div class="alert alert-success alert-dismissible" role="alert" id="myAlert">
       <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
        <strong>Success!</strong> {{ Session::get('multiplestatusupdate') }}
      </div>

      @endif

       @if(Session::has('addbulkitemstoproject'))

      <div class="alert alert-success alert-dismissible" role="alert" id="myAlert">
       <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
        <strong>Success!</strong> {{ Session::get('addbulkitemstoproject') }}
      </div>

      @endif



      @if(Session::has('item-update-success'))

      <div class="alert alert-success alert-dismissible" role="alert" id="myAlert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <strong>Success!</strong> {{ Session::get('item-update-success') }}
        </div>

        @endif

      </div>

      <!-- end of error message display div -->
      <p class="card-description">

        <span style="font-size: 18px;">Qty of items : <b class="badge badge-primary"> {{ $General_Serial_quantity }} </b></span> |<span style="font-size: 18px;"> Qty To be reviewed  : <b class="badge badge-info">{{ $totalQuantity }}</b></span> |
        <span style="font-size: 18px;"> Qty Ready for Project : <b class="badge badge-warning"> {{ $totalReadyForProject }} </b></span> |
        <span style="font-size: 18px;"> Qty Added to Project : <b class="badge badge-success">{{ $totalAddedForProject }}</b></span>
      </p>
       <a href="{{ route('all-items') }}" target="_SELF" class="btn btn-danger btn-xs float-none float-sm-right"> << Back</a>
      <div class="table-responsive" id="divCheckAll">
       <hr>
       <form action="{{ route('item-multiple-status-update')}}" method="post" id="checklist">
        {{ csrf_field()}}
       <table>
        <tr>
          <td><input type="checkbox" name="checkall" id="checkall" onClick="check_uncheck_checkbox(this.checked);"></td>
          <td><select class="form-control" name="multiplestatus" required="required">
            <option value="" selected="selected"><span style="font-weight: bold;">-- Select Status To Assign To Items --</span></option>
            @foreach($all_items_status as $status)
              <option value="{{ $status->id }}">{{ $status->item_status_name }}</option>
            @endforeach
          </select>
          </td>
           <td><button class="btn btn-primary btn-xs" type="submit">Update Status</button></td>
           <!-- <td><button class="btn btn-success btn-xs" type="button" onclick="save()">Add Selected Items To Project</button></td> -->
          </tr>
       </table>

      <table class="table table-striped" id="myTable" data-page-length='25'>
       <thead>
        <tr>
         <th></th>
         <th>#</th>
         <th>ITEM ID</th>
         <th>ITEM STATUS</th>
         <th style="text-align: center">UPDATE | PROJECT</th>
         <!-- <th style="text-align: center">ACTION</th> -->
       </tr>
     </thead>
     <tbody>

      @foreach($items as $index=>$item)
      <tr>
        <td><input type="checkbox" name="checkallboxes[]" class="divCheckboxItem" id="checkallboxes" value="{{ $item->id }}" /></td>
        <input type="hidden" name="general_id" value="{{ $item->general_id }}" id="gen_id">
      </form>
     <td>{{ $index+1 }}</td>
        <td>{{ strtoupper($item->itemid)}}</td>

        <form class="form-horizontal" action="{{ route('item-status-update', [$item->id]) }}" method="post">
          {{ csrf_field() }}
          <input type="hidden" name="status_id" id="status_id" value="{{ $item->id }}"/>
          <td>
            <select class="form-control" name="ItemStatus" id="ItemStatus">

             @if($item->item_status_id == NULL)
             <option value=""> NOT AVAILABLE </option>
             @foreach($all_items_status as $status)
             <option value="{{ $status->id }}">{{ $status->item_status_name }}</option>
             @endforeach
             @else
             <option value="{{ $item->item_status_id }}">{{ $item->item_status}}</option>
             @foreach($all_items_status as $status)
             <option value="{{ $status->id }}">{{ $status->item_status_name }}</option>
             @endforeach
             @endif

           </select>
         </td>

         <td><button  type="submit" class="btn btn-success btn-xs">
          <i class="fa fa-edit"></i> Change Status</button> |
          <a href="{{ route('add-project', [$item->id]) }}" class="btn btn-primary btn-xs"  onclick="return confirm('Are you sure you want to add this Item to project ?')"><i class="fa fa-plus"></i> Add to Project</a>
         </td>

      </form>


      <!-- <td colspan="3" style="text-align: center">
        <a title="View items"  href="#"><i class="fa fa-eye icon-sm"></i></a> |
        <a title="Edit Item" style="color: green" href="#"><i class="fa fa-pen icon-sm"></i></a> | <a href="#" onclick="return confirm('Are you sure you want to Delete this Item Status ?')" title="Delete Item" style="color: red"><i class="fa fa-trash icon-sm"></i></a>
      </td> -->
    </tr>

    @endforeach

  </tbody>
</table>
</div>
</div>
</div>


<div>




</div>

<script type="text/javascript">
  $(document).ready(function(){
  $("#checklist").on("submit", function(){
    $("#pageloader").fadeIn();
  });
});
</script>

@endsection

<script type="text/javascript">
 function check_uncheck_checkbox(isChecked) {
  if(isChecked) {
    $('input[name="checkallboxes[]"]').each(function() {
      this.checked = true;
    });
  } else {
    $('input[name="checkallboxes[]"]').each(function() {
      this.checked = false;
    });
  }
}

</script>

<!-- <script type="text/javascript">

function save(){
     $.ajax({
        url: "{{ url('/admin/selected/add/project') }}",
        method: 'post',
        data: {
           name: $('#checkallboxes').val(),
           type: $('#gen_id').val()
        },
        success: function(result){
           console.log(result);
        }});
}

</script> -->
