<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>A Simple Responsive HTML Email</title>

        <style type="text/css">
            body{
               margin: 0;
               padding: 0;
               min-width: 100%!important;
               background-color:#EAEAEA;
               color:#313A45;
               font-family:museo-sans-1, museo-sans-2, "Helvetica Neue", sans-serif;
             }
           .content {
             width: 100%;
             max-width: 600px;
             padding:10px;
             font-size: 14px;
           }

           .page{
               text-align: center;
               width:600px;
               margin-left:200px;
               background-color:#FFFFFF;
               border: 15px solid #50D987;
             }

            .header{
               height:50px;
               margin-top: 2px;
               background-image:linear-gradient(to right, #3FDB96, #8BD253);
               padding-bottom:30px;
            }

            .footer{
               height:25px;
               background-image:linear-gradient(to right, #3FDB96, #8BD253);
            }

           .main{
              text-align: left;
              padding-left:10px;
              font-size: 14px;
            }
        </style>

    </head>
   <body>
        <div class="page">
           <div class="header">
             <b style="color: #7fb401;">
              <span style="font-size: 30px;">Computer</span><span style="font-weight: bolder; font-size: 45px;">4</span><span style="font-size:35px;">Schools</span>
             </b>
           </div>
           <div class="main">
                <p><b>Hello :</b> Mr/Mrs. {{ $details->donorname }},</p>
                <p>Computer4School appreciate your contribution of <b>{{ $details->quantity}} {{ $details->name }} {{ $details->catname }}</b> to support our Campaign</p>
                <p>Your <b>User ID: {{ strtoupper($details->donid) }}</b> can be use to track your donated Items on our home page</p>
                <p>Below is the IDs assigned to each item and their status, these items have <b>General ID : {{ strtoupper($details->generalID) }}</b></p>
                <p></p>
                <p>Our Regards</p>
           </div>
           <table class="content" align="center" cellpadding="0" cellspacing="0" border="0">
            <thead>
              <tr>
                <th style="text-align:left">#</th>
                <th style="text-align:left">Item ID</th>
                <th style="text-align:left">Status</th>
              </tr>
            </thead>
            @foreach ($items as $key => $item)
            <tr>
              <td style="text-align:left">{{$key+1}}</td>
              <td style="text-align:left">{{strtoupper($item->itemid)}}</td>
               @if($item->item_status == null)
                  <td style="text-align:left">Not Set</td>
               @else
                  <td style="text-align:left">{{$item->item_status}}</td>
               @endif
            </tr>
            @endforeach
          </table>
          <div class="footer">
            <b style="color: #7fb401;">
             <span style="font-size: 15px;">Computer</span><span style="font-weight: bolder; font-size: 23px;">4</span><span style="font-size:16px;">Schools</span>
            </b>
          </div>
        </div>
    </body>
</html>
