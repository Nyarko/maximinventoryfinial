@extends('layouts.adminlayout')

@section('title', 'All Items')

@section('content')

  <div class="card">
    <div class="card-body">
      <h2 class="card-title">
        <i class="menu-icon fas fa-th"></i> All General  Items - Bulk Registration
      </h2>

      <!-- error message display div -->
      <div>
        @if(Session::has('create-item-success'))

        <div class="alert alert-success alert-dismissible" role="alert" id="myAlert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <strong>Success!</strong> {{ Session::get('create-item-success') }}
        </div>

        @endif

        @if(Session::has('update-item-success'))

        <div class="alert alert-success alert-dismissible" role="alert" id="myAlert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <strong>Success!</strong> {{ Session::get('update-item-success') }}
        </div>

        @endif

        @if(Session::has('delete-item-success'))

        <div class="alert alert-success alert-dismissible" role="alert" id="myAlert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>

          <strong>Success!</strong>{{ Session::get('delete-item-success') }}

        </div>

        @endif

      </div>

      <!-- end of error message display div -->
      <p class="card-description">
       <span class="text-danger"><b>** Click on General item name to see all items</b> </span> <a href="{{ route('create-item') }}" target="_SELF" class="btn btn-success float-none float-sm-right">Add Item
          <b> <i class="fa fa-plus-square"></i></b>
         </a>
      </p>
      <div class="table-responsive">
         <hr>
          <table class="table table-striped" id="myTable" data-page-length='25'>
          <thead>
            <tr>
              <th>#</th>
              <th>GeneralID</th>
              <th>General Item Name</th>
              <th>Quantity</th>
              <th>Category</th>
              <th>CollectionPoint</th>
              <th>Added On</th>
              <th style="text-align: center">Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach($allGeneralItems as $index=>$item)
            <tr>
              <td>{{ $index+1}}</td>
              <td><a href="{{ route('view-items', [$item->id])}}">{{ $item->generalid }}</a></td>
              <td><a href="{{ route('view-items', [$item->id])}}">{{ $item->name }}</a></td>
              <td>{{ $item->quantity }}</td>
              <td>{{ $item->categoryName }}</td>
              <td>{{ $item->collectionPointName}}</td>
              <td>{{ date('M j, Y', strtotime($item->created_at )) }}</td>
              <td colspan="3" style="text-align: center">
               <a title="View items"  href="{{ route('view-items', [$item->id])}}"><i class="fa fa-eye icon-sm"></i></a>  |
               <a title="Edit Item" style="color: green" href="#"><i class="fa fa-pen icon-sm"></i></a> |
               <a href="#" onclick="return confirm('Are you sure you want to Delete this Item Status ?')" title="Delete Item" style="color: red"><i class="fa fa-trash icon-sm"></i></a> |
               <a href="{{ route('item-to-send', [$item->id])}}" title="Click to show items ready to be send as message to donor" style="color:gray"><i class="fa fa-envelope icon-sm"></i></a>
              </td>
            </tr>

            @endforeach

          </tbody>
        </table>
      </div>
    </div>
  </div>
@endsection
