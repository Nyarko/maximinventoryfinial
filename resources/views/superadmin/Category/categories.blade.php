@extends('layouts.adminlayout')

@section('title', 'All Categories')

@section('content')

<div class="stretch-card">
  <div class="card">
    <div class="card-body">
       <p class="card-description">
       <h2 class="card-title">
         <i class="mdi mdi-google-circles-communities"></i>All  Item Categories
       </h2>

       <!-- error message display div -->
      <div>
        @if(Session::has('create-category-success'))

        <div class="alert alert-success alert-dismissible" role="alert" id="myAlert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <strong>Sucess!</strong> {{ Session::get('create-category-success') }}
        </div>

        @endif

        @if(Session::has('update-category-success'))

        <div class="alert alert-success alert-dismissible" role="alert" id="myAlert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <strong>Sucess!</strong> {{ Session::get('update-category-success') }}
        </div>

        @endif

        @if(Session::has('delete-category-success'))

        <div class="alert alert-success alert-dismissible" role="alert" id="myAlert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <strong>Sucess!</strong> {{ Session::get('delete-category-success') }}
        </div>

        @endif
      </div>

      <!-- end of error message display div -->

        <a href="{{ route('addCategory') }}" target="_SELF" class="btn btn-success float-none float-sm-right">Add New Category
          <b><i class="mdi mdi-plus"></i></b>
         </a>
      </p>
      <div class="table-responsive">
         <hr>
        <table class="table table-striped" id="myTable" data-page-length='25'>
          <thead>
            <tr>
              <th>#</th>
              <th>Category</th>
              <th>Created On</th>
              <th style="text-align: center">Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach($allCategories as $index=>$category)
            <tr>
              <td>{{ $index+1}}</td>
              <td>{{ $category->name }}</td>
              <td>{{ date('M j, Y', strtotime($category->created_at )) }}</td>
              <td colspan="2" style="text-align: center">
                <a title="Edit Item Category" style="color: green" href="{{ route('edit-category',[$category->id])}}"><i class="fa fa-pen icon-sm"></i></a> | <a href="{{ route('delete-category',[$category->id])}}" onclick="return confirm('Are you sure you want to Delete this CATEGORY ?')" title="Delete Category" style="color: red"><i class="fa fa-trash icon-sm"></i></a></td>
            </tr>

            @endforeach

          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>


@endsection