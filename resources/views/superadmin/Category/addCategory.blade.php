@extends('layouts.adminlayout')

@section('title', 'Add Category')

@section('content')

  <div class="content-wrapper d-flex align-items-center auth register-bg-1 theme-one">
  <div class="row w-100">
    <div class="col-lg-6 col-md-7 col-sm-8 col-xs-12 mx-auto">
      <div class="auto-form-wrapper" style="padding-top: 50px;padding-bottom: 50px">
      <h4 class="text-center mb-4"><i class="mdi mdi-google-circles-communities"></i> Add Category</h4>

        <form class="form" method="post" action="{{ route('post-add-category')}}">
          {{ csrf_field() }}
          <div class="form-group">
            <div class="input-group">
              <input type="text" required="required" class="form-control" placeholder="Enter Category Name" name="categoryname" id="categoryname"> 
              <div class="input-group-append">
                <span class="input-group-text">
                  <i class="mdi mdi-google-circles-communities"></i>
                </span>
              </div>
            </div>
          </div>
          <div class="form-group">
            <button class="btn btn-success submit-btn btn-block"><i class="menu-icon fas fa-save"></i>Add Category</button>
          </div>
        
        </form>

      </div>
    </div>
  </div>
</div>

@endsection