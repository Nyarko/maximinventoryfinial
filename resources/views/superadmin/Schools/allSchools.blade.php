@extends('layouts.adminlayout')

@section('title', 'All Schools')

@section('content')

  <div class="card">
    <div class="card-body">
      <h2 class="card-title">
        <i class="menu-icon fas fa-graduation-cap"></i>  All Schools
      </h2>
      <p class="card-description">
        
         @if (session('create-school-success'))
            <div class="alert alert-success" id="myAlert">
                {{ session('create-school-success') }}
            </div>
         @endif

         @if (session('update-school-success'))
            <div class="alert alert-success" id="myAlert">
                {{ session('update-school-success') }}
            </div>
         @endif

         @if (session('create-completed-success'))
            <div class="alert alert-success" id="myAlert">
                {{ session('create-completed-success') }}
            </div>
         @endif

         @if (session('create-inprogress-success'))
            <div class="alert alert-success" id="myAlert">
                {{ session('create-inprogress-success') }}
            </div>
         @endif

          @if (session('create-restore-success'))
            <div class="alert alert-success" id="myAlert">
                {{ session('create-restore-success') }}
            </div>
         @endif

          @if (session('delete'))
            <div class="alert alert-success" id="myAlert">
                {{ session('delete') }}
            </div>
         @endif

    

        <a href="{{ route('addschool') }}" target="_SELF" class="btn btn-success float-none float-sm-right">Add School
          <b> <i class="fa fa-plus-square"></i></b>
         </a>
      </p>
      <div class="table-responsive">
         <hr>
        <table class="display table table-bordered" id="myUsers">
          <thead>
            <tr>
              <th>#</th>
              <th>School Name</th>
              <th>Email</th>
              <th>Phone</th>
              <th>Town</th>
              <th>State</th>
              <th style="text-align: center;">Action</th>
            </tr>
          </thead>
          <tbody>
           
           @foreach($schools as $id => $school)
              
            <tr>
              <td>{{ $id += 1 }}</td>
              <td>{{ $school->school_name }}</td>
              <td>{{ $school->email}}</td>
              <td>{{ $school->phone}}</td>
              <td>{{ $school->town }}</td>
         
             

                @if($school->deleted_at == null)
                
                    @if($school->state == 0)

                    <td style="text-align: center"><a href="{{ route('inprogress', [$id => $school->id ]) }}" class="badge badge-warning">Candidate</a></td>
                 
                   @elseif($school->state == 1)

                    <td style="text-align: center"><a href="{{ route('completed', [$id => $school->id]) }}" class="badge badge-info">In progress</a></td>

                   @elseif($school->state == 2)

                     <td style="text-align: center"><a href="{{ route('inprogress', [$id => $school->id ]) }}" class="badge badge-success">Completed</a></td>
 
                    @endif
 
                @else


                    @if($school->state == 0)

                    <td style="text-align: center; pointer-events: none" title="restore to perform activity"><a href="#" class="badge badge-danger">deleted</a></td>
                 
                   @elseif($school->state == 1)

                    <td style="text-align: center; pointer-events: none" title="restore to perform activity"><a href="#" class="badge badge-danger">deleted</a></td>

                   @elseif($school->state == 2)

                     <td style="text-align: center; pointer-events: none" title="restore to perform activity"><a href="#" class="badge badge-danger">deleted</a></td>
 
                    @endif

                @endif



              <td style="text-align: center;" colspan="3">
               
                
                @if($school->deleted_at == null)
                    <a href="{{ route('view-school', [$id => $school->id ]) }}" style="color: green" title="view"><i class="fa fa-eye icon-sm"></i></a>  |
                    <a href="{{ route('edit-school', [$id => $school->id ]) }}" style="color: blue" title="edit"><i class="fa fa-pen icon-sm"></i></a>  |
                   <a href="{{ route('delete-school', [$id => $school->id]) }}" style="color: red" title="delete"><i class="fa fa-trash icon-sm"></i></a>
                @else

                  <a href="#" style="color: lightgray; pointer-events: none" title="restore to view"><i class="fa fa-eye icon-sm"></i></a>  |
                  <a href="#" style="color:lightgray; pointer-events: none" title="restore to edit"><i class="fa fa-pen icon-sm"></i></a>  |
                  <a href="#" style="color: gray" title="restore"><i class="fas fa-window-restore"></i></a>

                @endif
              
              </td>
             </tr>
             
           @endforeach

            </tbody>
        </table>
      </div>
    </div>
  </div>
@endsection