@extends('layouts.adminlayout')

@section('title', 'View School')

@section('content')

<div class="content-wrapper d-flex align-items-center auth register-bg-1 theme-one">
  <div class="row w-100" >
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <h2 class="text-center mb-4"><i class="menu-icon fas fa-compass"></i> School Details</h2>
      <div class="auto-form-wrapper" style="padding: 10px -5px 10px 10px; margin-right: -20px">
     
          <div class="row" style="width:100%; height:350px">
             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

              @if($school->map == null)
                  <center><h3>Location on Map Not Set</h3></center>
              @else
                <div class="mapouter">
                   <div class="gmap_canvas">
                    <iframe id="gmap_canvas" src="{{ $school->map}}" frameborder="0" scrolling="no" marginheight="0" marginwidth="0">  
                    </iframe>
                  </div>
                </div>
              @endif  
             </div>
          </div>
          <hr>
          <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <table class="table table-hover table-responsive">
                   <tbody>
                     <tr>
                       <th><i class="fa fa-graduation-cap"></i></th>
                       <th>School Name</th>
                       <td>{{ $school->school_name }}</td>
                     </tr>
                      <tr>
                       <th><i class="fa fa-envelope"></i></th>
                       <th>Email</th>
                       <td>{{ $school->email }}</td>
                     </tr>
                     <tr>
                       <th><i class="fa fa-phone"></i></th>
                       <th>Phone</th>
                       <td>{{ $school->phone }}</td>
                     </tr>
                     <tr>
                       <th><i class="fa fa-address-card"></i></th>
                       <th>Address</th>
                       <td>{{ $school->address }}</td>
                     </tr>
                     <tr>
                       <th><i class="fa fa-snowflake"></i></th>
                       <th>Town</th>
                       <td>{{ $school->town }}</td>
                     </tr>
                     <tr>
                       <th><i class="fa fa-asterisk"></i></th>
                       <th>District</th>
                       <td>{{ $school->district }}</td>
                     </tr>
                     <tr>
                       <th><i class="fa fa-compass"></i></th>
                       <th>Region</th>
                       <td>{{ $school->region }}</td>
                     </tr>
                     <tr>
                       <th><i class="fa fa-compass"></i></th>
                       <th>Destination Point Assigned</th>
                       <td>{{ $school->destination }}</td>
                     </tr>
                     <tr>
                       <th><i class="fa fa-clock"></i></th>
                       <th>Date Registered</th>
                       <td><b>{{ date('M j, Y', strtotime($school->created_at)) }}</b></td>
                     </tr>
                   </tbody>
                </table>
              </div>
          </div>
          <hr>
          <div class="row">
            <table>
               <tfoot style="text-align: center;">
               <tr>
                  <td><a href="{{ route('allschools') }}" class="btn btn-success"><i class="fa fa-arrow-left"></i> Back</a></td>
                  <td><a href="{{ route('edit-school', [$id => $school->id]) }}" class="btn btn-primary"><i class="fa fa-edit"></i> Edit</a></td>
                </tr>
             </tfoot>
            </table>
          </div>
         
                   

      </div>
    </div>
  </div>
</div>

@endsection