@extends('layouts.adminlayout')

@section('title', 'Add School')

@section('content')

  <div class="content-wrapper d-flex align-items-center auth register-bg-1 theme-one">
  <div class="row w-100">
    <div class="col-lg-6 col-md-8 col-sm-10 col-xs-12 mx-auto">

         @if (session('error'))
            <div class="alert alert-success" id="myAlert">
                {{ session('error') }}
            </div>
         @endif
      
      <div class="auto-form-wrapper">
        <h4 class="text-center mb-4"><i class="menu-icon fas fa-graduation-cap"></i> Add School</h4>

        <form action="{{ route('post-school') }}"  method="post" enctype="multipart/form-data">
          {{ csrf_field() }}

          <div class="form-group">
            <div class="input-group">
              <input type="text" name="name" id="name" class="form-control" placeholder="Enter School Name" required="required" autocomplete autofocus>
              <div class="input-group-append">
                <span class="input-group-text">
                  <i class="fa fa-graduation-cap"></i>
                </span>
              </div>
               @if ($errors->has('name'))
                  <span class="invalid-feedback">
                      <strong>{{ $errors->first('name') }}</strong>
                  </span>
              @endif
            </div>
          </div>

          <div class="form-group">
            <div class="input-group">
              <input type="email" name="email" id="email" class="form-control" placeholder="Enter Email" required="required" autocomplete>
              <div class="input-group-append">
                <span class="input-group-text">
                  <i class="fa fa-envelope"></i>
                </span>
              </div>
               @if ($errors->has('email'))
                  <span class="invalid-feedback">
                      <strong>{{ $errors->first('email') }}</strong>
                  </span>
              @endif
            </div>
          </div>

          <div class="form-group">
            <div class="input-group">
              <input type="text" name="phone" id="phone" minlength="10" maxlength="12" class="form-control" placeholder="Enter Contact Number" required="required">
              <div class="input-group-append">
                <span class="input-group-text">
                  <i class="fa fa-phone"></i>
                </span>
              </div>
              @if ($errors->has('phone'))
                  <span class="invalid-feedback">
                      <strong>{{ $errors->first('phone') }}</strong>
                  </span>
              @endif
            </div>
          </div>

           <div class="form-group">
            <div class="input-group">
              <input type="text" name="address" id="address" class="form-control" placeholder="Enter Address" required="required">
              <div class="input-group-append">
                <span class="input-group-text">
                  <i class="fa fa-address-card"></i>
                </span>
              </div>
              @if ($errors->has('address'))
                  <span class="invalid-feedback">
                      <strong>{{ $errors->first('address') }}</strong>
                  </span>
              @endif
            </div>
          </div>


          <div class="form-group">
            <div class="input-group">
              <input name="town" id="town" type="text" class="form-control" placeholder="Town of School Eg. Apemso" required="required" autocomplete autofocus>
              <div class="input-group-append">
                <span class="input-group-text">
                  <i class="fa fa-snowflake"></i>
                </span>
              </div>
               @if ($errors->has('town'))
                  <span class="invalid-feedback">
                      <strong>{{ $errors->first('town') }}</strong>
                  </span>
              @endif
            </div>
          </div>

        
          <div class="form-group">
            <select class="form-control" name="district" id="district">
              <option value="">Select District</option>
               @foreach($districts as $id => $district)
                <option value="{{ $district->id }}">{{ $district->district_name }}</option> 
               @endforeach
            </select> 
            @if ($errors->has('district'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('district') }}</strong>
                </span>
            @endif
          </div>


          <div class="form-group">
            <select class="form-control" name="region" id="region">
              <option value="">Select Region</option>
               @foreach($regions as $id => $region)
                <option value="{{ $region->id }}">{{ $region->region_name }}</option> 
               @endforeach
            </select>
            @if ($errors->has('region'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('region') }}</strong>
                </span>
            @endif
          </div>


          <div class="form-group">
            <select class="form-control" name="destination" id="destination">
              <option value="">Assign a Destination Point</option>
              <option value="">Not Available</option>
               @foreach($destinations as $id => $destina)
                <option value="{{ $destina->id }}">{{ $destina->destinationpoint_name }}</option> 
               @endforeach
            </select>
            @if ($errors->has('destination'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('destination') }}</strong>
                </span>
            @endif
          </div>

          <div class="form-group">
              <p><label for="map">Add Location of School</label></p>
              <textarea rows="6" cols="10" class="form-control" name="map" id="map" style="resize: none" placeholder="Please Note this well. Click the link below to map page. 2. Search name of location. 3. Click on Get HTML-Code. 4. copy the text between the 'src=' quote and paste here"></textarea>
            
              @if ($errors->has('map'))
              <span class="help-block text-danger">
                <h6>{{ $errors->first('map') }}</h6>
              </span>
              @endif
          </div>
          <p>
            <a href="https://www.embedgooglemap.net/" target="_blank" style="font-size:12px">Click this link to search for location and copy map source</a>
          </p>

          <div class="form-group">
            <button type="submit" name="submit" id="submit" class="btn btn-success submit-btn btn-block"><i class="menu-icon fas fa-save"></i> Save School</button>
          </div>
        
        </form>
      </div>
    </div>
  </div>
</div>


@endsection