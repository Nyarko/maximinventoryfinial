@extends('layouts.adminlayout')

@section('title', 'Edit Donor')

@section('content')

  <div class="content-wrapper d-flex align-items-center auth register-bg-1 theme-one">
  <div class="row w-100">
    <div class="col-lg-6 col-md-8 col-sm-10 col-xs-12 mx-auto">
       @if (session('error'))
            <div class="alert alert-success" id="myAlert">
                {{ session('error') }}
            </div>
       @endif
      
      <div class="auto-form-wrapper">
     
        <h4 class="text-center mb-4"><i class="menu-icon fas fa-users"></i> Edit Donator</h4>

        <form action="{{ route('update-donor', [$donor->id]) }}" method="post">
           {{ csrf_field() }}
          
          <input type="hidden" name="donor_id" value="{{$donor->id}}">

          <div class="form-group">
            <div class="input-group">
              <input type="text" class="form-control" name="name" id="name" value="{{ $donor->name }}" placeholder="Donor Fullname" autocomplete autofocus required="required">
              <div class="input-group-append">
                <span class="input-group-text">
                  <i class="fa fa-user"></i>
                </span>
              </div>
               @if ($errors->has('name'))
                  <span class="invalid-feedback">
                      <strong>{{ $errors->first('name') }}</strong>
                  </span>
              @endif
            </div>
          </div>

           <div class="form-group">
            <div class="input-group">
              <input type="text" class="form-control" name="organization" id="organization" value="{{ $donor->organization_name }}" placeholder="Organization Name" autocomplete>
              <div class="input-group-append">
                <span class="input-group-text">
                  <i class="fa fa-podcast"></i>
                </span>
              </div>
               @if ($errors->has('organization'))
                  <span class="invalid-feedback">
                      <strong>{{ $errors->first('organization') }}</strong>
                  </span>
              @endif
            </div>
          </div>

          <div class="form-group">
            <div class="input-group">
              <input type="email" name="email" id="email" class="form-control" value="{{ $donor->email }}" placeholder="Email Address" autocomplete required="required">
              <div class="input-group-append">
                <span class="input-group-text">
                  <i class="fa fa-envelope"></i>
                </span>
              </div>
               @if ($errors->has('organization'))
                  <span class="invalid-feedback">
                      <strong>{{ $errors->first('organization') }}</strong>
                  </span>
              @endif
            </div>
          </div>

           <div class="form-group">
            <div class="input-group">
              <input type="text" minlength="10" maxlength="15" class="form-control" value="{{ $donor->phone }}" name="phone" id="phone" placeholder="Contact Number" autocomplete required="required">
              <div class="input-group-append">
                <span class="input-group-text">
                  <i class="fa fa-phone"></i>
                </span>
              </div>
               @if ($errors->has('phone'))
                  <span class="invalid-feedback">
                      <strong>{{ $errors->first('phone') }}</strong>
                  </span>
              @endif
            </div>
          </div>

          <div class="form-group">
            <button class="btn btn-success submit-btn btn-block" name="savedonor" id="savedonor"><i class="menu-icon fas fa-edit"></i> Update Donor</button>
          </div>
        
        </form>
      </div>
    </div>
  </div>
</div>


@endsection