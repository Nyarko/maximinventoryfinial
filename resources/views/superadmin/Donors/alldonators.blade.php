@extends('layouts.adminlayout')

@section('title', 'All Donators')

@section('content')

  <div class="card">
    <div class="card-body">
      <h2 class="card-title">
        <i class="menu-icon fas fa-users"></i>  All Donators
      </h2>
      <p class="card-description">
         @if (session('create-donor-success'))
            <div class="alert alert-success" id="myAlert">
                {{ session('create-donor-success') }}
            </div>
         @endif

         @if (session('update-donor-success'))
            <div class="alert alert-success" id="myAlert">
                {{ session('update-donor-success') }}
            </div>
         @endif

          @if (session('delete'))
            <div class="alert alert-success" id="myAlert">
                {{ session('delete') }}
            </div>
         @endif


        <a href="{{ route('addDonator') }}" target="_SELF" class="btn btn-success float-none float-sm-right">Add Donator
          <b> <i class="fa fa-plus-square"></i></b>
         </a>
      </p>
      <div class="table-responsive">
         <hr>
        <table class="display table table-bordered" id="myUsers">
          <thead>
            <tr>
              <th>#</th>
              <th>Id</th>
              <th>Name</th>
              <th>Organization</th>
              <th>Phone</th>
              <th>Email</th>
              <th>Date</th>
              <th style="text-align: center;">Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach($donors as $id => $donor)
            <tr>
              <td>{{ $id += 1 }}</td>
              <td>{{ $donor->donatorid }}</td>
              <td>{{ $donor->name }}</td>
              <td>{{ $donor->organization_name }}</td>
              <td>{{ $donor->phone }}</td>
              <td>{{ $donor->email }}</td>
              <td>{{ date('M j, Y', strtotime($donor->created_at)) }}</td>
              <td style="text-align: center;" colspan="2">
                
                 
                 @if($donor->deleted_at == null)
                   <a href="{{ route('edit-donor', [$id => $donor->id]) }}" style="color: blue" title="edit"><i class="fa fa-pen icon-sm"></i></a> |
                  <a href="{{ route('delete-donor', [$id => $donor->id]) }}" style="color: red" title="delete"><i class="fa fa-trash icon-sm"></i></a>
               
                 @else
                  <a href="{{ route('edit-donor', [$id => $donor->id]) }}" style="color: lightgray; pointer-events: none" title="edit"><i class="fa fa-pen icon-sm"></i></a> |
                  <a href="{{ route('restore', [$id => $donor->id]) }}" style="color: lightgray" title="restore"><i class="fa fa-unlock icon-sm"></i></a>

                 @endif

              </td>
             </tr>

            @endforeach

          </tbody>
        </table>
      </div>
    </div>
  </div>
@endsection