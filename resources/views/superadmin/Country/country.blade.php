@extends('layouts.adminlayout')

@section('title', 'All Country')

@section('content')

  <div class="card">
    <div class="card-body">
      <h2 class="card-title">
        <i class="menu-icon fa fa-th-list"></i> Country
      </h2>
      <p class="card-description">
         @if (session('create-country-success'))
        <div class="alert alert-success alert-dismissible" role="alert" id="myAlert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <strong>Sucess!</strong> {{ session('create-country-success') }}
        </div>

         @endif

         @if (session('delete'))
        <div class="alert alert-success alert-dismissible" role="alert" id="myAlert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <strong>Sucess!</strong> {{ session('delete') }}
        </div>
         @endif

          @if (session('error'))
          <div class="alert alert-danger alert-dismissible" role="alert" id="myAlert">
           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
            <strong>Errors!</strong> {{ session('error') }}
          </div>
         @endif

        <a href="{{ route('addCountry') }}" target="_SELF" class="btn btn-success float-none float-sm-right">Add Country
          <b><i class="mdi mdi-plus"></i></b>
         </a>
      </p>
      <div class="table-responsive">
         <hr>
        <table class="table table-striped" id="myTable">
          <thead>
            <tr>
              <th>#</th>
              <th>Country</th>
              <th>Created_at</th>
              <th style="text-align: center">Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach($countries as $id => $country)

            <tr>
              <td>{{ $id+=1 }}</td>
              <td>{{ $country->country_name }}</td>
              <td>{{ date('M j, Y', strtotime($country->created_at)) }}</td>
              <td style="text-align: center" colspan="2">
                 <a href="{{ route('country-edit', [$id => $country->id]) }}"  style="color: blue">
                  <i class="fa fa-pen icon-sm"></i></a>  |
                 <a href="{{ route('deleteCountry', [$id => $country->id]) }}" style="color: red"  onclick="return confirm('Are you sure you want to Delete this COUNTRY?')"><i class="fa fa-trash icon-sm"></i></a>
              </td>
            </tr>

            @endforeach

          </tbody>
        </table>
      </div>
    </div>
  </div>
@endsection
