@extends('layouts.adminlayout')

@section('title', 'Add Country')

@section('content')

  <div class="content-wrapper d-flex align-items-center auth register-bg-1 theme-one">
  <div class="row w-100">
    <div class="col-lg-5 col-md-5 col-sm-8 col-xs-12 mx-auto" style="padding-top: 30px;padding-bottom: 30px;">
     @if (session('error'))
      <div class="alert alert-danger alert-dismissible" role="alert" id="myAlert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
        <strong>Errors!</strong> {{ session('error') }}
      </div>
     @endif
      <div class="auto-form-wrapper">
      <h4 class="text-center mb-4"><i class="menu-icon  fa fa-th-list"></i> Add Country</h4>

        <form class="form" method="post" action="{{ route('postAddCountry') }}">
         {{ csrf_field() }}
          <div class="form-group">
            <div class="input-group">
              <input type="text" class="form-control" placeholder="Enter Country" name="countryName" id="countryName" required autofocus>
              <div class="input-group-append">
                <span class="input-group-text">
                  <i class="fa fa-address-card"></i>
                </span>
              </div>
              @if ($errors->has('countryName'))
                  <span class="invalid-feedback">
                      <strong>{{ $errors->first('countryName') }}</strong>
                  </span>
              @endif
            </div>
          </div>
          <div class="form-group">
            <button type="submit" name="saveCountry"  id="saveCountry" class="btn btn-success submit-btn btn-block"><i class="menu-icon fas fa-save"></i> Save Country</button>
          </div>

        </form>

      </div>
    </div>
  </div>
</div>

@endsection
