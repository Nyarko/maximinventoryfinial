@extends('layouts.adminlayout')

@section('title', 'Edit Country')

@section('content')

  <div class="content-wrapper d-flex align-items-center auth register-bg-1 theme-one">
  <div class="row w-100">
    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 mx-auto">
         @if (session('error'))
          <div class="alert alert-danger alert-dismissible" role="alert" id="myAlert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
            <strong>Errors!</strong> {{ session('error') }}
          </div>
         @endif
      <h2 class="text-center mb-4"><i class="menu-icon  fa fa-th-list"></i> Edit Country</h2>
      <div class="auto-form-wrapper">

        <form class="form" method="post" action="{{ route('update-country', [$country->id]) }}">
         {{ csrf_field() }}

         <input type="hidden" name="country_id" value="{{ $country->id }}">

          <div class="form-group">
            <div class="input-group">
              <input type="text" class="form-control" value="{{ $country->country_name }}" placeholder="Enter Country" name="countryName" id="countryName" required="required" autofocus>
              <div class="input-group-append">
                <span class="input-group-text">
                  <i class="fa fa-address-card"></i>
                </span>
              </div>
              @if ($errors->has('countryName'))
                  <span class="invalid-feedback">
                      <strong>{{ $errors->first('countryName') }}</strong>
                  </span>
              @endif
            </div>
          </div>
          <div class="form-group">
            <button type="submit" name="updateCountry"  id="updateCountry" class="btn btn-success submit-btn btn-block"><i class="menu-icon fas fa-edit"></i> Update Country</button>
          </div>

        </form>

      </div>
    </div>
  </div>
</div>

@endsection
