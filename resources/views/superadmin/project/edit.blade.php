@extends('layouts.adminlayout')

@section('title', 'Edit Item')

@section('content')

  <div class="content-wrapper d-flex align-items-center auth register-bg-1 theme-one">
  <div class="row w-100">
    <div class="col-lg-6 col-md-6 col-sm-8 mx-auto">
      <div class="auto-form-wrapper" style="padding-top: 50px;padding-bottom: 50px">
       <h4 class="text-center mb-4"><i class="menu-icon fa fa-plus-square"></i> Edit Project</h4>

        <form action="{{ route('update-project', [$project->id]) }}" method="post" enctype="multipart/form-data">
          {{ csrf_field()}}

          <input type="hidden" name="project_id" id="project_id" value="{{ $project->id }}">

          <div class="form-group">
            <div class="input-group">
              <input type="text" class="form-control" name="proname" id="proname" value="{{ $project->name }}" placeholder="Project Name Eg. Trinity Project" required>
              <div class="input-group-append">
                <span class="input-group-text">
                  <i class="fa fa-plus-square"></i>
                </span>
              </div>
            </div>
          </div>

          <div class="form-group">
            <label for="startdate">Select Start Date of Project</label>
            <div class="input-group">
              <input type="date" class="form-control" name="startdate" id="startdate" value="{{ $project->start }}" required>
              <div class="input-group-append">
                <span class="input-group-text">
                  <i class="fa fa-clock"></i>
                </span>
              </div>
            </div>
          </div>

           <div class="form-group">
             <label for="enddate">Select End Date of Project</label>
            <div class="input-group">
              <input type="date" class="form-control" name="enddate" id="enddate" value="{{ $project->end }}" required>
              <div class="input-group-append">
                <span class="input-group-text">
                  <i class="fa fa-clock"></i>
                </span>
              </div>
            </div>
          </div>

         <div class="form-group">
            <select class="form-control" name="destiid" id="destiid" required>
              <option selected="selected" value="{{ $project->destination_id }}">{{ $project->desti }} </option>
              @foreach($destination as $dest)
                <option value="{{ $dest->id }}">{{ $dest->destinationpoint_name }}</option>
              @endforeach
            </select>
          </div>

                   
          <div class="form-group">
            <button class="btn btn-success submit-btn btn-block"><i class="menu-icon fas fa-edit"></i> Update Project</button>
          </div>
        
        </form>
      </div>
    </div>
  </div>
</div>


@endsection