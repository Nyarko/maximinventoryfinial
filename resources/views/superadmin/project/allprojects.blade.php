@extends('layouts.adminlayout')

@section('title', 'view items ready for project')

@section('content')



<div class="card">
  <div class="card-body">
    <h2 class="card-title">
      <i class="menu-icon fas fa-th"></i>  Add Project
    </h2>

    <!-- error message display div -->
    <div>
      @if(Session::has('create-project-success'))

      <div class="alert alert-success alert-dismissible" role="alert" id="myAlert">
       <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
        <strong>Success!</strong> {{ Session::get('create-project-success') }}
      </div>

      @endif

      @if(Session::has('update-item-success'))

      <div class="alert alert-success alert-dismissible" role="alert" id="myAlert">
       <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
        <strong>Success!</strong> {{ Session::get('update-item-success') }}
      </div>

      @endif

      @if(Session::has('delete-item-success'))

      <div class="alert alert-success alert-dismissible" role="alert" id="myAlert">
       <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>

        <strong>Success!</strong>{{ Session::get('delete-item-success') }}

      </div>

      @endif

      @if (session('error'))
      <div class="alert alert-danger" id="myAlert">
        <p>{{ session('error') }}</p>
      </div>
      @endif

      @if(Session::has('item-project'))

      <div class="alert alert-success alert-dismissible" role="alert" id="myAlert">
       <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
        <strong>Success!</strong> {{ Session::get('item-project') }}
      </div>

      @endif

      @if(Session::has('items-allocated-success'))

      <div class="alert alert-success alert-dismissible" role="alert" id="myAlert">
       <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
        <strong>Success!</strong> {{ Session::get('items-allocated-success') }}
      </div>

      @endif

     

      @if(Session::has('item-update-success'))

      <div class="alert alert-success alert-dismissible" role="alert" id="myAlert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <strong>Success!</strong> {{ Session::get('item-update-success') }}
        </div>

        @endif

      @if(Session::has('project'))

       <div class="alert alert-success alert-dismissible" role="alert" id="myAlert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <strong>Success!</strong> {{ Session::get('project') }}
        </div>

        @endif

        @if(Session::has('completed'))
         <div class="alert alert-success alert-dismissible" role="alert" id="myAlert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <strong>Success!</strong> {{ Session::get('completed') }}
         </div>
        @endif

        @if(Session::has('update-project-success'))
         <div class="alert alert-success alert-dismissible" role="alert" id="myAlert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <strong>Success!</strong> {{ Session::get('update-project-success') }}
         </div>
        @endif

      </div>

      <!-- end of error message display div -->
      <p class="card-description">
         <a href="{{ route('add-project-start') }}" target="_SELF" class="btn btn-success float-none float-sm-right">Add Project
          <b> <i class="fa fa-plus-square"></i></b>
         </a>
      </p>

      <div class="table-responsive">
      <hr>
      <table class="table table-striped" id="myTable" data-page-length='25'>
       <thead>
        <tr>
          <th>#</th>
          <th>Project Name</th>
          <th>Destination Point</th>
          <th>Start Date</th>
          <th>End Date</th>
          <th style="text-align:center;">Status</th>
          <th style="text-align:center;">Action</th>
       </tr>
     </thead>
     <tbody>
       
        @foreach($project as $id => $pro)
       <tr>
         <td>{{ $id += 1 }}</td>
         <td><a href="{{ route('view-project-plan', [$id => $pro->id]) }}">{{ $pro->name }}</a></td>
         <td>{{ $pro->destination }}</td>
         <td>{{ date('M j, Y', strtotime($pro->start)) }}</td>
         <td>{{ date('M j, Y', strtotime($pro->end)) }}</td>
         @if($pro->status == "New" || $pro->status == "new")
           <td style="text-align:center;"><a href="{{ route('set-ongoing', [$id => $pro->id]) }}" onclick="return confirm('Are you sure you want to INITIATE this Project?')" title="Click to INITIATE project"><span class="badge badge-danger">{{ $pro->status }}</span></a></td>
         @elseif($pro->status == "Ongoing" || $pro->status == "ongoing")
            <td style="text-align:center;"><a href="{{ route('set-completed', [$id=>$pro->id]) }}" onclick="return confirm('Are you sure you want to set Project to COMPLETED?')" title="Click to set project to COMPLETED"><span class="badge badge-info">{{ $pro->status }}</span></a></td>
         @else
            <td style="text-align:center;"><a href="#"><span class="badge badge-success">{{ $pro->status }}</span></a></td>
         @endif
         <td colspan="3" style="text-align:center;">
            <a title="View Project Plan" style="color: green" href="{{ route('view-project-plan', [$id => $pro->id]) }}"><i class="fa fa-eye icon-sm"></i></a> |
            <a title="Edit Project" style="color: blue" href="{{ route('edit-project', [$id => $pro->id]) }}"><i class="fa fa-pen icon-sm"></i></a> |
            <a title="Edit Item" style="color: red" href="#"><i class="fa fa-trash icon-sm"></i></a>
         </td>
       </tr>
        @endforeach

     </tbody>
  </table>
</div>
</div>
</div>
@endsection



