@extends('layouts.adminlayout')

@section('title', 'All categories of items')

@section('content')

  <!-- partial -->
     
        <div class="content-wrapper">
          
          <div class="row">

              <table class="table table-bordered"> 
                  <tbody>

                    @foreach($allCategoriesDonated->chunk(3) as $category)
                    <tr>
                      @foreach($category as $index=>$cat)
                      <td>
                          
                          <div class="card card-statistics">
                            <a href="{{ route('view-category-item-project',[$cat->id])}}">
                            <div class="card-body">
                              <div class="clearfix">
                                <div class="float-left">
                                 <i class="fa fa-folder text-success icon-lg"></i>
                                </div>
                                <div class="float-right">
                                  <p class="mb-0 text-right" style="color: #000"><span class="badge badge-success">{{ $index+1 }}</span></p>
                                  <div class="fluid-container">
                                    <h3 class="font-weight-medium text-right mb-0" style="color: #000">{{ $cat->name }}</h3>
                                  </div>
                                </div>
                              </div>
                           </div>
                         </a>
                          </div>
                       
                      </td>
                      @endforeach
                    </tr>
                    @endforeach
                  </tbody>
                </table>
                   
          </div>

        </div>
       
@endsection