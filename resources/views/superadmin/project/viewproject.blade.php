@extends('layouts.adminlayout')

@section('title', 'View Project')

@section('content')

<style type="text/css">
  
.mapouter{
  text-align:center;
  height:350px
  width:100%;
 }
                    
#gmap_canvas{
   overflow:hidden;
   background:none!important;
   height:350px;
   width:100%;
 }

</style>

<div class="content-wrapper d-flex align-items-center auth register-bg-1 theme-one">
  <div class="row w-100">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <h2 class="text-center mb-4"><i class="menu-icon fas fa-plus-square"></i> Project Plan Description</h2>
      
      <div class="auto-form-wrapper" style="padding: 10px -5px 10px 10px; margin-right: -20px">
         <h5><i class="fa fa-plus-square"></i> Project Information</h5>
          <hr>
          <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <table class="table table-hover table-responsive">
                   <tbody>
                     <tr>
                       <th><i class="fa fa-road"></i></th>
                       <th>Project Name</th>
                       <td>{{ $plan->name}}</td>
                     </tr>
                      <tr>
                       <th><i class="fa fa-globe"></i></th>
                       <th>Status</th>
                       <td><b class="badge badge-info">{{ $plan->status }}</b></td>
                     </tr>
                     <tr>
                       <th><i class="fa fa-clock"></i></th>
                       <th>Start Date</th>
                       <td>{{ date('M j, Y', strtotime($plan->start)) }}</td>
                     </tr>
                      <tr>
                       <th><i class="fa fa-clock"></i> </th>
                       <th>End Date</th>
                       <td>{{ date('M j, Y', strtotime($plan->end)) }}</td>
                     </tr>
                   </tbody>
                </table>
              </div>
          </div>

          <hr>
          <h5><i class="fa fa-compass"></i> Destination Information</h5>
          <hr>
          <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <table class="table table-hover table-responsive">
                   <tbody>
                     <tr>
                       <th><i class="fa fa-key"></i></th>
                       <th> Destination Point Name</th>
                       <td>{{ $plan->destinationpoint_name }}</td>
                     </tr>
                     <tr>
                       <th><i class="fa fa-compass"></i></th>
                       <th>Status</th>
                       <td><b class="badge badge-success">{{ $plan->dstatus }}</b></td>
                     </tr>
                      <tr>
                       <th><i class="fa fa-building"></i></th>
                       <th>Town</th>
                       <td>{{ $plan->town }}</td>
                     </tr>
                      <tr>
                       <th><i class="fa fa-bookmark"></i></th>
                       <th>District</th>
                       <td>{{ $plan->district }}</td>
                     </tr>
                      <tr>
                       <th><i class="fa  fa-address-book"></i></th>
                       <th>Region</th>
                       <td>{{ $plan->region }}</td>
                     </tr>
                     <tr>
                       <th><i class="fa fa-snowflake"></i></th>
                       <th>Description</th>
                       <td>{{ $plan->description }}</td>
                     </tr>
                   </tbody>
                </table>
              </div>
          </div>

          <hr>
          <h5><i class="fa fa-compass"></i> Schools Under Destination Point</h5>
          <hr>
          <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <table class="table table-hover table-responsive">
                   <tbody>
                     <tr>
                       <th>School Name</th>
                       <th>Phone</th>
                       <th>Email</th>
                       <th>Town</th>
                       <th>Map</th>
                     </tr>
                    @foreach($school as $sch)

                     <tr>
                       <td>{{ $sch->school_name }}</td>
                       <td>{{ $sch->phone }}</td>
                       <td>{{ $sch->email }}</td>
                       <td>{{ $sch->town }}</td>
                       <td><a href="" data-toggle="modal" data-target="#myModal{{ $sch->id }}"><i class="fa fa-map"></i></a></td>

                    <!-- Modal -->
                      <div id="myModal{{ $sch->id }}" class="modal fade" role="dialog">
                      <div class="modal-dialog modal-lg">
                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                           School Location <button type="button" class="close" data-dismiss="modal">&times;</button>
                          </div>
                          <div class="modal-body">
                            @if($sch->map == null)
                                <center><h3>Location on Map Not Set</h3></center>
                            @else
                              <div class="mapouter">
                                 <div class="gmap_canvas">
                                  <iframe id="gmap_canvas" src="{{ $sch->map}}" frameborder="0" scrolling="yes" marginheight="0" marginwidth="0">  
                                  </iframe>
                                </div>
                              </div>
                            @endif  
                          </div>
                        </div>
                      </div>
                      </div>
                     </tr>

                    @endforeach
                   </tbody>
                </table>
              </div>
          </div>
           <hr>
          <h5><i class="fa fa-dot-circle"></i> Item Category</h5>
          <hr>
          <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <table class="table table-hover table-responsive">
                   <tbody>
                     <tr> 
                      <td class="alert alert-success">
                        Total Items : <b class="badge badge-primary">{{ $total }}</b> =>
                       @foreach($category as $cat)
                        | {{ $cat->catname}} <b class="badge badge-danger">{{ $cat->total }}</b>
                        @endforeach
                       </td>
                     </tr>
                   </tbody>
                </table>
              </div>
          </div>
          <hr>
          <h5><i class="fa fa-compass"></i> Items Allocated</h5>
          <hr>
          <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <table class="table table-hover table-responsive" id="myTable" data-page-length='25'>
                  <thead>
                    <tr>
                       <th>#</th>
                       <th>Item ID</th> 
                       <th>Status</th>
                       <th>General ID</th>
                       <th>Brand</th>
                       <th>Category Name</th>
                       <th>Donor</th>
                     </tr>
                  </thead>
                   <tbody>
                    @foreach($items as $id => $item)
                     <tr>
                       <td>{{ $id+= 1}}</td>
                       <td>{{ mb_strtoupper($item->itemid) }}</td>
                       <td>{{ $item->itstatus }}</td>
                       <td>{{ $item->genid }}</td>
                       <td>{{ $item->gename }}</td>
                       <td>{{ $item->catname }}</td>
                       <td>{{ $item->dname }}</td>
                     </tr>
                    @endforeach
                   </tbody>
                </table>
              </div>
          </div>

          <hr>
          <div class="row">
            <table>
             <tfoot style="text-align: center;">
              <tr>
                <td><a href="{{ route('project-index') }}" class="btn btn-success"><i class="fa fa-arrow-left"></i> Back</a></td>
              </tr>
             </tfoot>
            </table>
          </div>
         
                   

      </div>
    </div>
  </div>
</div>

@endsection