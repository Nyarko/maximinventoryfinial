@extends('layouts.adminlayout')

@section('title', 'view items ready for project')

@section('content')



<div class="card">
  <div class="card-body">
    <h2 class="card-title">
      <i class="menu-icon fas fa-th"></i>  Allocate items for project         <a href="{{ route('all-categories-donated') }}" target="_SELF" class="btn btn-danger float-none float-sm-right">Back >>  </a>

    </h2>

    <!-- error message display div -->
    <div>
      @if(Session::has('create-item-success'))

      <div class="alert alert-success alert-dismissible" role="alert" id="myAlert">
       <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
        <strong>Success!</strong> {{ Session::get('create-item-success') }}
      </div>

      @endif

      @if(Session::has('update-item-success'))

      <div class="alert alert-success alert-dismissible" role="alert" id="myAlert">
       <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
        <strong>Success!</strong> {{ Session::get('update-item-success') }}
      </div>

      @endif

      @if(Session::has('delete-item-success'))

      <div class="alert alert-success alert-dismissible" role="alert" id="myAlert">
       <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>

        <strong>Success!</strong>{{ Session::get('delete-item-success') }}

      </div>

      @endif

       @if(Session::has('item-reverted'))

      <div class="alert alert-success alert-dismissible" role="alert" id="myAlert">
       <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>

        <strong>Success!</strong>{{ Session::get('item-reverted') }}

      </div>

      @endif

      @if (session('error'))
      <div class="alert alert-danger" id="myAlert">
        <p>{{ session('error') }}</p>
      </div>
      @endif

      @if(Session::has('item-project'))

      <div class="alert alert-success alert-dismissible" role="alert" id="myAlert">
       <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
        <strong>Success!</strong> {{ Session::get('item-project') }}
      </div>

      @endif

      @if(Session::has('no-item-selected'))

      <div class="alert alert-danger alert-dismissible" role="alert" id="myAlert">
       <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
        <strong>Ooops!</strong> {{ Session::get('no-item-selected') }}
      </div>

      @endif

     

      @if(Session::has('item-update-success'))

      <div class="alert alert-success alert-dismissible" role="alert" id="myAlert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <strong>Success!</strong> {{ Session::get('item-update-success') }}
        </div>

        @endif

      </div>

      <!-- end of error message display div -->
      <p class="card-description">
      </p>
      <div class="table-responsive" id="divCheckAll">
       <hr>
       <form action="{{ route('allocate-items-to-project')}}" method="post">
        {{ csrf_field()}}
       <table>
        <tr>
          <td><input type="checkbox" name="checkall" id="checkall" onClick="check_uncheck_checkbox(this.checked);"></td>
          <!-- <td style="font-weight: bold;">- Select Project to Allocate Items To -</td> -->
          <td>
            <select class="form-control" name="projectname" required="required">
              <option value="" selected="selected">-- Select Project to Allocate Items To --</option>
              @foreach($projectlist as $project)
               <option value="{{ $project->id }}">{{ $project->name }}</option>
              @endforeach

          </select>
          </td>
          <td><button class="btn btn-primary btn-xs" type="submit">Allocate to Project</button></td>
        </tr>
      </table>
   
      <table class="table table-striped" id="myTable" data-page-length='25'>
       <thead>
        <tr>
         <th></th>
         <th>#</th>
         <th>ITEM ID</th>
         <th>ITEM STATUS</th>
         <th>ITEM GENERAL ID</th>
         <th style="text-align:center;">Revert</th>
       </tr>
     </thead>
     <tbody>

      @foreach($viewItemCategoryProject as $index=>$item)
      <tr>
        <td><input type="checkbox" name="checkallboxes[]" class="divCheckboxItem" id="checkallboxes" value="{{ $item->id }}" /></td>
      </form>
        <td>{{ $index+1 }}</td>
        <td>{{ strtoupper($item->itemid)}}</td>
        <td>{{ $item->item_status_name }}</td>
        <td>{{ $item->generalid }}</td>
        <td style="text-align:center;"><a href="{{ route('revert-project', [$item->id]) }}" title="Revert process">
        <i class="fa fa-share icon-sm"></i></a>
        </td>
        </tr>
    @endforeach

  </tbody>
</table>
</div>
</div>
</div>
@endsection

<script type="text/javascript">
 function check_uncheck_checkbox(isChecked) {
  if(isChecked) {
    $('input[name="checkallboxes[]"]').each(function() { 
      this.checked = true; 
    });
  } else {
    $('input[name="checkallboxes[]"]').each(function() {
      this.checked = false;
    });
  }
}

</script>


