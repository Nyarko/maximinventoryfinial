@extends('layouts.adminlayout')

@section('title', 'All District')

@section('content')

  <div class="card">
    <div class="card-body">
       <h2 class="card-title">
        <i class="menu-icon fa fa-address-card"></i> District
      </h2>
       @if (session('delete'))
        <div class="alert alert-success alert-dismissible" role="alert" id="myAlert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <strong>Sucess!</strong> {{ session('delete') }}
        </div>
         @endif

         @if (session('error'))
          <div class="alert alert-danger alert-dismissible" role="alert" id="myAlert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
            <strong>Errors!</strong> {{ session('error') }}
          </div>
         @endif
         
        @if (session('create-district-success'))
        <div class="alert alert-success alert-dismissible" role="alert" id="myAlert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <strong>Sucess!</strong> {{ session('create-district-success') }}
        </div>
        @endif
      <p class="card-description">
        <a href="{{ route('addDistrict') }}" target="_SELF" class="btn btn-success float-none float-sm-right">Add District
          <b><i class="mdi mdi-plus"></i></b>
         </a>
      </p>
      <div class="table-responsive">
         <hr>
        <table class="table table-striped" id="myTable">
          <thead>
            <tr>
              <th>#</th>
              <th>District</th>
              <th>Created_at</th>
              <th style="text-align: center">Action</th>
            </tr>
          </thead>
          <tbody>

            @foreach($districts as $id => $district)

            <tr>
              <td>{{ $id += 1 }}</td>
              <td>{{ $district->district_name }}</td>
              <td>{{ date('M j, Y', strtotime($district->created_at)) }}</td>
              <td style="text-align: center" colspan="2">
                 <a href="{{ route('district-edit', [$id => $district->id]) }}" style="color: blue"><i class="fa fa-pen icon-sm"></i></a>  |
                 <a href="{{ route('deleteDistrict', [$id => $district->id])}}" style="color: red" onclick="return confirm('Are you sure you want to Delete this DISTRICT ?')"><i class="fa fa-trash icon-sm"></i></a>
              </td>
            </tr>

            @endforeach

          </tbody>
        </table>
      </div>
    </div>
  </div>


@endsection
