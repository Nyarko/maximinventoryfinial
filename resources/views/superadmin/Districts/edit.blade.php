@extends('layouts.adminlayout')

@section('title', 'Edit District')

@section('content')

  <div class="content-wrapper d-flex align-items-center auth register-bg-1 theme-one">
  <div class="row w-100">
    <div class="col-lg-6 col-md-7 col-sm-8 col-xs-12 mx-auto">
        @if (session('error'))
            <div class="alert alert-success" id="myAlert">
                {{ session('error') }}
            </div>
        @endif
    
      <div class="auto-form-wrapper">
        <h4 class="text-center mb-4"><i class="menu-icon  fa fa-address-card"></i> Edit District</h4>
        <form action="{{ route('update-district', [$district->id]) }}" method="post">
          {{ csrf_field() }}
         
          <input type="hidden" name="district_id" value="{{ $district->id}}">

          <div class="form-group">
            <div class="input-group">
              <input type="text" name="districtName" value="{{ $district->district_name }}" id="districtName" class="form-control" placeholder="Enter District" required="required" autofocus>
              <div class="input-group-append">
                <span class="input-group-text">
                  <i class="fa fa-address-card"></i>
                </span>
              </div>
               @if ($errors->has('districtName'))
                  <span class="invalid-feedback">
                      <strong>{{ $errors->first('districtName') }}</strong>
                  </span>
               @endif
            </div>
          </div>
          <div class="form-group">
            <button type="submit" name="updateDistrict" id="updateDistrict" class="btn btn-success submit-btn btn-block"><i class="menu-icon fas fa-edit"></i> Update District</button>
          </div>
        
        </form>

      </div>
    </div>
  </div>
</div>


@endsection