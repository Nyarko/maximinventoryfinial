@extends('layouts.adminlayout')

@section('title', 'Add New Destination Point')

@section('content')

  <div class="content-wrapper d-flex align-items-center auth register-bg-1 theme-one">
  <div class="row w-100">
    <div class="col-lg-5 col-md-5 col-sm-10 col-xs-12 mx-auto">
       <h4 class="text-center mb-4"><i class="menu-icon mdi mdi-houzz"></i> Destination Point Description</h4>
        
        <p>
          {{ $destinationpoint_information->description}}
        </p>


    </div>
  </div>
</div>


@endsection