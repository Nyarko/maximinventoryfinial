@extends('layouts.adminlayout')

@section('title', 'All Destination Point Status')

@section('content')

<div class="stretch-card">
  <div class="card">
    <div class="card-body">
      <h2 class="card-title">
        <i class="menu-icon fas fa-map-marker"></i>  All Destination Point Statuses
      </h2>

      <!-- error message display div -->
      <div>
        @if(Session::has('create-destinationpointstatus-success'))

        <div class="alert alert-success alert-dismissible" role="alert" id="myAlert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <strong>Sucess!</strong>{{ Session::get('create-destinationpointstatus-success') }}
        </div>

        @endif

        @if(Session::has('update-destinationpointstatus-success'))

        <div class="alert alert-success alert-dismissible" role="alert" id="myAlert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <strong>Sucess!</strong>{{ Session::get('update-destinationpointstatus-success') }}
        </div>

        @endif


        @if(Session::has('delete-destinationpointstatus-success'))

        <div class="alert alert-success alert-dismissible" role="alert" id="myAlert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <strong>Sucess!</strong>{{ Session::get('delete-destinationpointstatus-success') }}
        </div>

        @endif
      </div>

      <!-- end of error message display div -->


      <p class="card-description">
        <a href="{{ route('addDestinationpointStatus') }}" target="_SELF" class="btn btn-success float-none float-sm-right">Add Destination Point Status
          <b> <i class="fa fa-plus-square"></i></b>
         </a>
      </p>
      <div class="table-responsive">
         <hr>
         <table class="table table-striped" id="myTable">
          <thead>
            <tr>
              <th>#</th>
              <th>Destination Point Status</th>
              <th>Created On</th>
              <th style="text-align: center">Action</th>
            </tr>
          </thead>

          <tbody>
            @foreach($allDestinationPointStatus as $index=>$destinapointstatus)
            <tr>
              <td>{{ $index+1}}</td>
              <td>{{ $destinapointstatus->destination_status_name }}</td>
              <td>{{ date('M j, Y', strtotime($destinapointstatus->created_at )) }}</td>
              <td colspan="2" style="text-align: center">
                <a title="Edit destination point status" href="{{ route('edit-destinationpointstatus', [$destinapointstatus->id])}}"><i class="fa fa-pen icon-sm" style="color: green"></i></a> | <a href="{{ route('delete-destinationpointstatus', [$destinapointstatus->id])}}" onclick="return confirm('Are you sure you want to Delete this DESTINATION POINT STATUS ?')" title="Delete Category" style="color: red"><i class="fa fa-trash icon-sm"></i></a></td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
  </div>
@endsection