@extends('layouts.adminlayout')

@section('title', 'All Destination Points')

@section('content')

 <div class="stretch-card">
  <div class="card">
    <div class="card-body">
      <h2 class="card-title">
        <i class="menu-icon fa fa-tint"></i> All Destination Points
      </h2>

       <!-- error message display div -->
      <div>
        @if(Session::has('create-destinationpoint-success'))

        <div class="alert alert-success alert-dismissible" role="alert" id="myAlert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <strong>Success!</strong> {{ Session::get('create-destinationpoint-success') }}
        </div>

        @endif

        @if(Session::has('update-destinationpoint-success'))

        <div class="alert alert-success alert-dismissible" role="alert" id="myAlert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <strong>Success!</strong> {{ Session::get('update-destinationpoint-success') }}
        </div>

        @endif

        @if(Session::has('delete-destinationpoint-success'))

        <div class="alert alert-success alert-dismissible" role="alert" id="myAlert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>

          <strong>Success!</strong>{{ Session::get('delete-destinationpoint-success') }}

        </div>

        @endif

      </div>

      <!-- end of error message display div -->


      <p class="card-description">
        <a href="{{ route('addDestinationPoint') }}" target="_SELF" class="btn btn-success float-none float-sm-right"> Add Destination Point
          <b> <i class="fa fa-plus-square"></i></b>
         </a>
      </p>
      <div class="table-responsive">
         <hr>
         <table class="display table table-bordered" id="myTable">
          <thead>
            <tr>
              <th>#</th>
              <th>Destination point</th>
              <th>District</th>
              <th>Town</th>
              <th>Region</th>
              <th>Dest. P Manager</th>
              <th>Dest. P. Status</th>
              {{-- <th>Created On</th> --}}
              <th style="text-align: center">Action</th>
              
            </tr>
          </thead>
          <tbody>
            @foreach($allDestinationPoints as $index=>$destinationpoint)
            <tr>
              <td>{{ $index+1 }}</td>
              <td>{{ $destinationpoint->destinationpoint_name  }}</td>
               <td>{{ $destinationpoint->district_name }}</td>
              <td>{{ $destinationpoint->town}}</td>
              <td>{{ $destinationpoint->region_name }}</td>
              <td>{{ $destinationpoint->firstname . " " . $destinationpoint->othername }}</td>
              <td>{{ $destinationpoint->destination_status_name }}</td>
              {{-- <td>{{ date('M j, Y', strtotime($destinationpoint->created_at )) }}</td> --}}
              <td colspan="3" style="text-align: center;"><a href="#"><i class="fa fa-eye icon-sm" title="View Destination Point"></i></a> | <a href="{{ route('edit-destinationpoint', [$destinationpoint->id])}}" title="Edit Destination Point" style="color: green;"><i class="fa fa-pen icon-sm"></i></a> |  <a href="{{ route('delete-destinationpoint', [$destinationpoint->id])}}"  onclick="return confirm('Are you sure you want to Delete this DESTINATION POINT ?')" title="Delete Destination Point"><i class="fa fa-trash icon-sm" style="color: red"></i></a></td>
              
            </tr>
            @endforeach
            </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection