@extends('layouts.adminlayout')

@section('title', 'Add Destination Point Status')

@section('content')

  <div class="content-wrapper d-flex align-items-center auth register-bg-1 theme-one">
  <div class="row w-100">
    <div class="col-lg-6 col-md-7 col-sm-10 col-xs-12 mx-auto">

      <!-- display common errors -->
        @if(count($errors)>0)

                <div class="alert alert-danger" role="alert">
                  <strong>Oops:</strong>

                  @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                  @endforeach

                </div>

                @endif

      <div class="auto-form-wrapper" style="padding-top: 50px;padding-bottom: 50px">
       <h4 class="text-center mb-4"><i class="menu-icon mdi mdi-houzz"></i> Add Destination Point Status</h4>
        <form action="{{ route('post-destinationpoint-status')}}" method="post">
          {{csrf_field()}}
          <div class="form-group">
            <div class="input-group">
              <input type="text" class="form-control" required="required" name="destinationpointstatus" placeholder="Eg. Candidate or Quick Scan Planned">
              <div class="input-group-append">
                <span class="input-group-text">
                  <i class="mdi mdi-houzz"></i>
                </span>
              </div>
            </div>
          </div>

          <div class="form-group">
            <button class="btn btn-success submit-btn btn-block"><i class="menu-icon fas fa-save"></i> Add Destination Point Status</button>
          </div>
        
        </form>
      </div>
    </div>
  </div>
</div>


@endsection