@extends('layouts.adminlayout')

@section('title', 'Add New Destination Point')

@section('content')

  <div class="content-wrapper d-flex align-items-center auth register-bg-1 theme-one">
  <div class="row w-100">
    <div class="col-lg-6 col-md-7 col-sm-10 col-xs-12 mx-auto">
       <h4 class="text-center mb-4"><i class="menu-icon mdi mdi-houzz"></i> Edit Destination Point</h4>
      <div class="auto-form-wrapper">
        <form action="{{ route('post-edit-destinationpoint', [$destinationpoint->id ])}}" method="post" enctype="multipart/form-data">
          {{ csrf_field() }}
          <input type="hidden" name="destination_point_id" value="{{ $destinationpoint->id}}">
          <div class="form-group">
            <div class="input-group">
              <input type="text" name="destination_point_name" class="form-control" value="{{ $destinationpoint->destinationpoint_name }}">
              <div class="input-group-append">
                <span class="input-group-text">
                  <i class="fa fa-user"></i>
                </span>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="input-group">
              <input type="text" name="destination_point_town" class="form-control" value="{{ $destinationpoint->town }}">
              <div class="input-group-append">
                <span class="input-group-text">
                  <i class="fa fa-envelope"></i>
                </span>
              </div>
            </div>
          </div>

          <div class="form-group">
            <select class="form-control" name="destination_point_region" id="destination_point_region">
              <option value="{{$destinationpoint->region_id }}" selected="selected">{{ $destinationpoint->region_name }}</option>
              @foreach($all_regions as $region)
              <option value="{{ $region->id }}">{{ $region->region_name }}</option>
              @endforeach
            </select>
          </div>

          <div class="form-group">
            <select class="form-control" name="destination_point_district" id="destination_point_district">
              <option value="{{ $destinationpoint->district_id }}">{{ $destinationpoint->district_name }}</option>
               @foreach($all_districts as $district)
              <option value="{{ $district->id }}">{{ $district->district_name }}</option>
              @endforeach
            </select>
          </div>

           <div class="form-group">
            <div class="form-group">
              <label for="exampleTextarea1">Description of Destination point </label>
             <textarea class="form-control" id="exampleTextarea1" placeholder="Eg. Access by Car, Social Amenities available , etc" rows="5" name="destination_point_description" >{{ $destinationpoint->description }}</textarea>
            </div>
          </div>

          <div class="form-group">
            <select class="form-control" name="destination_point_manager" id="destination_point_manager">
              <option value="{{ $destinationpoint->user_id }}" selected="selected">{{ $destinationpoint->firstname . " " . $destinationpoint->othername }}</option>
              @foreach($all_destination_managers as $manager)
              <option value="{{ $manager->id}}">{{ $manager->firstname . "  " . $manager->othername }}</option>
              @endforeach
             <option value=""> NOT AVAILABLE</option>
              </select>
          </div>

           <div class="form-group">
            <select class="form-control" name="destination_point_status" id="destination_point_status">
              <option value="{{ $destinationpoint->destination_status_id}}" selected="selected">{{ $destinationpoint->destination_status_name }}</option>
             @foreach($all_destination_status as $status)
              <option value="{{ $status->id }}">{{ $status->destination_status_name }}</option>
              @endforeach

              </select>
          </div>
          
          <div class="form-group">
            <button class="btn btn-success submit-btn btn-block"><i class="menu-icon fas fa-save"></i> Update Destination Point</button>
          </div>
        
        </form>
      </div>
    </div>
  </div>
</div>


@endsection