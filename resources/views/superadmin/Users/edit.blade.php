@extends('layouts.adminlayout')

@section('title', 'Add User')

@section('content')

<div class="content-wrapper d-flex align-items-center auth register-bg-1 theme-one">
  <div class="row w-100">
    <div class="col-lg-5 col-md-5 col-sm-8 mx-auto">

      <!-- display common errors -->
        @if(count($errors)>0)

                <div class="alert alert-danger" role="alert">
                  <strong>Oops:</strong>

                  @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                  @endforeach

                </div>

                @endif

      <div class="auto-form-wrapper" style="padding-top: 30px;padding-bottom: 30px;">
        <span><img src="{{ asset('/img/users/'.$user->avatar)}}" width="50px;" class="img-rounded"></span>
      <h4 class="text-center mb-4"><span><i class="menu-icon fas fa-user"></i> Edit User</span> </h4>
        <form action="{{ route('post-edit-user',[$user->id])}}" method="post" enctype="multipart/form-data">
          {{ csrf_field() }}
          <input type="hidden" name="user_id" value="{{ $user->id }}">
          <div class="form-group">
            <div class="input-group">
              <input type="text" class="form-control" name="firstname" value="{{ $user->firstname }}" required="required">
              <div class="input-group-append">
                <span class="input-group-text">
                  <i class="fa fa-user"></i>
                </span>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="input-group">
              <input type="text" class="form-control" name="othername" value="{{ $user->othername }}" required="required">
              <div class="input-group-append">
                <span class="input-group-text">
                  <i class="fa fa-user"></i>
                </span>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="input-group">
              <input type="email" class="form-control {{ $errors->has('email') ? 'has-error' : '' }}" name="email" value="{{ $user->email }}" required="required">
              <div class="input-group-append">
                <span class="input-group-text">
                  <i class="fa fa-envelope"></i>
                </span>
              </div>

              @if ($errors->has('email'))
              <span class="help-block text-danger">
                <h6>{{ $errors->first('email') }}</h6>
              </span>
              @endif

            </div>
          </div>

          <div class="form-group">
            <div class="input-group">
              <input type="text" class="form-control" name="phone_number" value="{{ $user->phone }}" required="required">
              <div class="input-group-append">
                <span class="input-group-text">
                  <i class="fa fa-phone"></i>
                </span>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="input-group">

              <input type="file" class="form-control" name="picture" placeholder="Profile photo">
              <div class="input-group-append">
                <span class="input-group-text">
                  <label>Upload new profile <i class="fa fa-image"></i> </label>
                </span>
              </div>
            </div>
          </div>

          <div class="form-group">
            <select class="form-control" name="usertype" id="usertype" required="required">
              <option value="{{ $user->usertype }}" selected="selected">{{ $user->usertype }}</option>
              <option value="Administrator">Administrator</option>
              <option value="Collection Manager">Collection Manager</option>
              <option value="Destination Manager">Destination Manager</option>
              <option value="MN Agent">MN Agent</option>
              <option value="Donator">Donator</option>

            </select>
          </div>

                   
          <div class="form-group">
            <button class="btn btn-success submit-btn btn-block"><i class="menu-icon fas fa-save"></i> Update User</button>
          </div>

        </form>
      </div>
    </div>
  </div>
</div>


@endsection