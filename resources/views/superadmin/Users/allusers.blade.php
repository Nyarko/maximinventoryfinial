@extends('layouts.adminlayout')

@section('title', 'All Users')

@section('content')

<div class="stretch-card">
  <div class="card">
    <div class="card-body">
      <h2 class="card-title">
        <i class="menu-icon fas fa-user"></i> Users 
      </h2>

      <!-- error message display div -->
      <div>
        @if(Session::has('create-user-success'))

        <div class="alert alert-success alert-dismissible" role="alert" id="myAlert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <strong>Sucess!</strong>{{ Session::get('create-user-success') }}
        </div>

        @endif

        @if(Session::has('update-user-success'))

        <div class="alert alert-success alert-dismissible" role="alert" id="myAlert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <strong>Sucess!</strong>{{ Session::get('update-user-success') }}
        </div>

        @endif

        @if(Session::has('delete-user-success'))

        <div class="alert alert-success alert-dismissible" role="alert" id="myAlert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <strong>Sucess!</strong>{{ Session::get('delete-user-success') }}
        </div>

        @endif

      </div>

      <!-- end of error message display div -->



      <p class="card-description">
        <a href="{{ route('addusers') }}" target="_SELF" class="btn btn-success float-none float-sm-right">Add User
          <b><i class="mdi mdi-plus"></i></b>
         </a>
      </p>
      <div class="table-responsive">
         <hr>
        <table class="table table-striped" id="myUsers">
          <thead>
            <tr>
              <th>#</th>
              <th>Photo</th>
              <th>#ID</th>
              <th>Name</th>
              <th>Role</th>
              <th>Email</th>
              <th>Phone</th>
              <th style="text-align: center;">Action</th>
              
            </tr>
          </thead>
          
      
          <tbody>
           @foreach($allUsers as $index=>$user)
             <tr>
              <td>{{$index+1}}</td>
              <td class="py-1"><img src="{{ asset('/img/users/'.$user->avatar)}}" alt="image" /></td>
              <td>{{ $user->unique_number }}</td>
              <td>{{ $user->firstname ." ". $user->othername }} </td>
              <td>{{ $user->usertype }}</td>
              <td>{{ $user->email }}</td>
              <td>{{ $user->phone }}</td>
              <td colspan="2" style="text-align: center;"><a href="#"><i class="fa fa-key icon-sm" title="Change User Password"></i></a> | <a href="{{ route('edit-user',[$user->id])}}" title="Edit User Information" style="color: green;"><i class="fa fa-pen icon-sm"></i></a> |  <a href="{{ route('delete-user', [$user->id]) }}"  onclick="return confirm('Are you sure you want to Delete this USER ?')" title="Delete User"><i class="fa fa-trash icon-sm" style="color: red"></i></a></td>
              
             </tr>                    
          @endforeach                 
          </tbody>

         
        </table>
      </div>
    </div>
  </div>
</div>




@endsection