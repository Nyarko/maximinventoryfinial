@extends('layouts.adminlayout')

@section('title', 'Add User')

@section('content')

<div class="content-wrapper d-flex align-items-center auth register-bg-1 theme-one">
  <div class="row w-100">
    <div class="col-lg-5 col-md-5 col-sm-8 mx-auto">

      <!-- display common errors -->
        @if(count($errors)>0)

                <div class="alert alert-danger" role="alert" id="myAlert">
                  

                  @foreach($errors->all() as $error)
                    <li><strong>Oops:</strong> {{ $error }}</li>
                  @endforeach

                </div>

                @endif

      <div class="auto-form-wrapper" style="padding-top: 30px;padding-bottom: 30px;">
      <h4 class="text-center mb-4"><i class="menu-icon fas fa-user"></i> Add User</h4>
        <form action="{{ route('post-add-user')}}" method="post" enctype="multipart/form-data">
          {{ csrf_field() }}
          <div class="form-group">
            <div class="input-group">
              <input type="text" class="form-control" name="firstname" placeholder="FirstName" required="required">
              <div class="input-group-append">
                <span class="input-group-text">
                  <i class="fa fa-user"></i>
                </span>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="input-group">
              <input type="text" class="form-control" name="othername" placeholder="OtherNames" required="required">
              <div class="input-group-append">
                <span class="input-group-text">
                  <i class="fa fa-user"></i>
                </span>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="input-group">
              <input type="email" class="form-control {{ $errors->has('email') ? 'has-error' : '' }}" name="email" placeholder="Email" required="required">
              <div class="input-group-append">
                <span class="input-group-text">
                  <i class="fa fa-envelope"></i>
                </span>
              </div>

              @if ($errors->has('email'))
              <span class="help-block text-danger">
                <h6>{{ $errors->first('email') }}</h6>
              </span>
              @endif

            </div>
          </div>

          <div class="form-group">
            <div class="input-group">
              <input type="text" class="form-control" name="phone_number" placeholder="Phone Number" required="required">
              <div class="input-group-append">
                <span class="input-group-text">
                  <i class="fa fa-phone"></i>
                </span>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="input-group">
              <input type="file" class="form-control" name="picture" placeholder="Profile photo">
              <div class="input-group-append">
                <span class="input-group-text">
                  <i class="fa fa-image"></i>
                </span>
              </div>
            </div>
          </div>

          <div class="form-group">
            <select class="form-control" name="usertype" id="usertype" required="required">

              <option value="" selected="selected">Select User Type</option>
              <option value="Administrator">Administrator</option>
              <option value="Collection Manager">Collection Manager</option>
              <option value="Destination Manager">Destination Manager</option>
              <option value="MN Agent">MN Agent</option>
           
            </select>
          </div>

          <div class="form-group">
            <div class="input-group">
              <input type="password" class="form-control" name="password" placeholder="Password" required="">
              <div class="input-group-append">
                <span class="input-group-text">
                  <i class="fa fa-unlock-alt"></i>
                </span>
              </div>
            </div>
          </div>

          
          <div class="form-group">
            <button class="btn btn-success submit-btn btn-block"><i class="menu-icon fas fa-save"></i> Create User</button>
          </div>

        </form>
      </div>
    </div>
  </div>
</div>


@endsection