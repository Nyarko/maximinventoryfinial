@extends('layouts.adminlayout')

@section('title', 'Dashboard')

@section('content')

  <!-- partial -->
     
        <div class="content-wrapper">
          
          <div class="row">
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
              <div class="card card-statistics">
              <a href="{{ route('alluser')}}">
                <div class="card-body">
                  <div class="clearfix">
                    <div class="float-left">
                      <i class="fa fa-user text-danger icon-lg"></i>
                    </div>
                     <div class="float-right">
                      <p class="mb-0 text-right" style="color: #000">Users</p>
                      <div class="fluid-container">
                        <h3 class="font-weight-medium text-right mb-0" style="color: #000">{{ $user }}</h3>
                      </div>
                    </div>
                 
                  </div>
                  <p class="text-muted mt-3 mb-0">
                 </p>
                </div>
              </a>
              </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
              <div class="card card-statistics">
                <a href="{{ route('all-items')}}">
                <div class="card-body">
                  <div class="clearfix">
                    <div class="float-left">
                      <i class="mdi mdi-nfc-variant text-warning icon-lg"></i>
                    </div>
                    <div class="float-right">
                      <p class="mb-0 text-right" style="color: #000">All Items</p>
                      <div class="fluid-container">
                        <h3 class="font-weight-medium text-right mb-0" style="color: #000">{{ $item }}</h3>
                      </div>
                    </div>
                  </div>
               </div>
             </a>
              </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
              <div class="card card-statistics">
             <a href="{{ route('allItemsStatus')}}">
                <div class="card-body">
                  <div class="clearfix">
                    <div class="float-left">
                      <i class="mdi mdi-spotify text-success icon-lg"></i>
                    </div>
                    <div class="float-right">
                      <p class="mb-0 text-right" style="color: #000">All Item Status</p>
                      <div class="fluid-container">
                        <h3 class="font-weight-medium text-right mb-0" style="color: #000">{{ $itemstatus }}</h3>
                      </div>
                    </div>
                  </div>
                  <p class="text-muted mt-3 mb-0">
                 </p>
                </div>
              </a>
              </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
              <div class="card card-statistics">
               <a href="{{ route('allCategories')}}">
                <div class="card-body">
                  <div class="clearfix">
                    <div class="float-left">
                      <i class="mdi mdi-google-circles-communities text-info icon-lg"></i>
                    </div>
                    <div class="float-right">
                      <p class="mb-0 text-right" style="color: #000">Items Categories</p>
                      <div class="fluid-container">
                        <h3 class="font-weight-medium text-right mb-0" style="color: #000">{{ $category }}</h3>
                      </div>
                    </div>
                  </div>
                  <p class="text-muted mt-3 mb-0">
                  </p>
                </div>
              </a>
              </div>
            </div>
          </div>

          <!-- row 2 -->
            
            <div class="row">
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
              <div class="card card-statistics">
                <a href="{{ route('allCollectionpoint')}}">
                <div class="card-body">
                  <div class="clearfix">
                    <div class="float-left">
                      <i class="mdi mdi-cube text-info icon-lg"></i>
                    </div>
                    <div class="float-right">
                      <p class="mb-0 text-right" style="color: #000">Collection Points</p>
                      <div class="fluid-container">
                        <h3 class="font-weight-medium text-right mb-0" style="color: #000">{{ $collectionpoint }}</h3>
                      </div>
                    </div>
                  </div>
                </div>
              </a>
              </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
              <div class="card card-statistics">
                <a href="{{ route('allCollectionPointStatus')}}">
                <div class="card-body">
                  <div class="clearfix">
                    <div class="float-left">
                      <i class="mdi mdi-receipt text-success icon-lg"></i>
                    </div>
                    <div class="float-right">
                      <p class="mb-0 text-right" style="color: #000">Collection Status</p>
                      <div class="fluid-container">
                        <h3 class="font-weight-medium text-right mb-0" style="color: #000">{{ $collectionstatus }}</h3>
                      </div>
                    </div>
                  </div>
                </div>
              </a>
              </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
              <div class="card card-statistics">
                <a href="{{ route('all-destination-points')}}">
                <div class="card-body">
                  <div class="clearfix">
                    <div class="float-left">
                      <i class="mdi mdi-code-equal text-warning icon-lg"></i>
                    </div>
                    <div class="float-right">
                      <p class="mb-0 text-right" style="color: #000">Destination Points</p>
                      <div class="fluid-container">
                        <h3 class="font-weight-medium text-right mb-0" style="color: #000">{{ $destination }}</h3>
                      </div>
                    </div>
                  </div>
                </div>
              </a>
              </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
              <div class="card card-statistics">
                <a href="{{ route('allDestinationPointStatus')}}">
                <div class="card-body">
                  <div class="clearfix">
                    <div class="float-left">
                      <i class="mdi  mdi-backup-restore text-danger icon-lg"></i>
                    </div>
                    <div class="float-right">
                      <p class="mb-0 text-right" style="color: #000">Destination Status</p>
                      <div class="fluid-container">
                        <h3 class="font-weight-medium text-right mb-0" style="color: #000">{{ $destinationstatus }}</h3>
                      </div>
                    </div>
                  </div>
                 </div>
               </a>
              </div>
            </div>
          </div>


          <!-- row 3 -->
            
          <div class="row">
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
              <div class="card card-statistics">
                <a href="{{ route('allregion')}}">
                <div class="card-body">
                  <div class="clearfix">
                    <div class="float-left">
                      <i class="fa fa-globe text-danger icon-lg"></i>
                    </div>
                    <div class="float-right">
                      <p class="mb-0 text-right" style="color: #000">Regions</p>
                      <div class="fluid-container">
                        <h3 class="font-weight-medium text-right mb-0" style="color: #000">{{ $region }}</h3>
                      </div>
                    </div>
                  </div>
                  
                </div>
              </a>
              </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
              <div class="card card-statistics">
               <a href="{{ route('allDistrict')}}">
                <div class="card-body">
                  <div class="clearfix">
                    <div class="float-left">
                      <i class="mdi fa fa-address-card text-warning icon-lg"></i>
                    </div>
                    <div class="float-right">
                      <p class="mb-0 text-right" style="color: #000">Districts</p>
                      <div class="fluid-container">
                        <h3 class="font-weight-medium text-right mb-0" style="color: #000">{{ $district }}</h3>
                      </div>
                    </div>
                  </div>
                  
                </div>
              </a>
              </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
              <div class="card card-statistics">
              <a href="#">
                <div class="card-body">
                  <div class="clearfix">
                    <div class="float-left">
                      <i class="fa fa-graduation-cap text-success icon-lg"></i>
                    </div>
                    <div class="float-right">
                      <p class="mb-0 text-right" style="color: #000">Schools</p>
                      <div class="fluid-container">
                        <h3 class="font-weight-medium text-right mb-0" style="color: #000">{{ $school }}</h3>
                      </div>
                    </div>
                  </div>
                 
                </div>
              </a>
              </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
              <div class="card card-statistics">
              <a href="#">
                <div class="card-body">
                  <div class="clearfix">
                    <div class="float-left">
                      <i class="fa fa-users text-info icon-lg"></i>
                    </div>
                    <div class="float-right">
                      <p class="mb-0 text-right" style="color: #000">Donators</p>
                      <div class="fluid-container">
                        <h3 class="font-weight-medium text-right mb-0" style="color: #000">{{ $donator }}</h3>
                      </div>
                    </div>
                  </div>
                  
                </div>
              </a>
              </div>
            </div>
          </div>

        </div>
       
@endsection