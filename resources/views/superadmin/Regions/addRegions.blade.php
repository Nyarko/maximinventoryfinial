@extends('layouts.adminlayout')

@section('title', 'Add Region')

@section('content')

  <div class="content-wrapper d-flex align-items-center auth register-bg-1 theme-one">
  <div class="row w-100">
    <div class="col-lg-6 col-md-7 col-sm-8 col-xs-12 mx-auto" style="padding-top: 30px;padding-bottom: 30px;">
        @if (session('error'))
         <div class="alert alert-danger alert-dismissible" role="alert" id="myAlert">
           <button type="button" class="close" data-dismiss="alert" aria-label="Close">
           <span aria-hidden="true">&times;</span></button>
           <strong>Errors!</strong> {{ session('error') }}
         </div>
        @endif
      <div class="auto-form-wrapper">
      <h4 class="text-center mb-4"><i class="menu-icon fa fa-globe"></i> Add Region</h4>

        <form method="post" action="{{ route('postAddRegion') }}">
          {{ csrf_field() }}
          <div class="form-group">
            <div class="input-group">
              <input type="text" name="regionName" id="regionName" class="form-control" placeholder="Enter Region" required="required" autofocus>
              <div class="input-group-append">
                <span class="input-group-text">
                  <i class="fa fa-globe"></i>
                </span>
              </div>
               @if ($errors->has('regionName'))
                  <span class="invalid-feedback">
                      <strong>{{ $errors->first('regionName') }}</strong>
                  </span>
               @endif
            </div>
          </div>
          <div class="form-group">
            <button type="submit" name="saveRegion" id="saveRegion" class="btn btn-success submit-btn btn-block"><i class="menu-icon fas fa-save"></i> Save Region</button>
          </div>

        </form>

      </div>
    </div>
  </div>
</div>

@endsection
