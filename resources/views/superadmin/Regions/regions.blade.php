@extends('layouts.adminlayout')

@section('title', 'All Region')

@section('content')

  <div class="card">
    <div class="card-body">
      <h2 class="card-title">
        <i class="menu-icon fa fa-globe"></i> Region
      </h2>
       @if (session('delete'))
        <div class="alert alert-success alert-dismissible" role="alert" id="myAlert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <strong>Sucess!</strong> {{ session('delete') }}
        </div>
         @endif
         @if (session('error'))
          <div class="alert alert-danger alert-dismissible" role="alert" id="myAlert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
            <strong>Errors!</strong> {{ session('error') }}
          </div>
         @endif
        @if (session('create-region-success'))
        <div class="alert alert-success alert-dismissible" role="alert" id="myAlert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <strong>Sucess!</strong> {{ session('create-region-success') }}
        </div>
        @endif
        <p class="card-description">
          <a href="{{ route('addRegions') }}" target="_SELF" class="btn btn-success float-none float-sm-right">Add Region
            <b><i class="mdi mdi-plus"></i></b>
           </a>
        </p>
      <div class="table-responsive">
         <hr>
        <table class="table table-striped" id="myTable">
          <thead>
            <tr>
              <th>#</th>
              <th>Region</th>
              <th>Created_at</th>
              <th style="text-align: center">Action</th>
            </tr>
          </thead>
          <tbody>
           @foreach($regions as $id => $region)
            <tr>
              <td>{{ $id += 1}}</td>
              <td>{{ $region->region_name }}</td>
              <td>{{ date('M j, Y', strtotime($region->created_at)) }}</td>
              <td style="text-align: center" colspan="2">
                <a href="{{ route('region-edit', [$id => $region->id]) }}"  style="color: blue"><i class="fa fa-pen icon-sm"></i></a>  |
                <a href="{{ route('deleteRegion', [$id => $region->id]) }}" style="color: red"  onclick="return confirm('Are you sure you want to Delete this REGION ?')"><i class="fa fa-trash icon-sm"></i></a>
              </td>
            </tr>
           @endforeach()
         </tbody>
        </table>
      </div>
    </div>
  </div>


@endsection
