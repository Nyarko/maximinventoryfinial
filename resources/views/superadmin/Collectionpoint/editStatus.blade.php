@extends('layouts.adminlayout')

@section('title', 'Edit Collection Point Status')

@section('content')

  <div class="content-wrapper d-flex align-items-center auth register-bg-1 theme-one">
  <div class="row w-100">
    <div class="col-lg-7 col-md-8 col-sm-10 col-xs-12 mx-auto">
       @if (session('error'))
            <div class="alert alert-success" id="myAlert">
                {{ session('error') }}
            </div>
        @endif
      <h2 class="text-center mb-4"><i class="menu-icon fas fa-compass"></i> Edit Collection Point Status</h2>
      <div class="auto-form-wrapper">

        <form action="{{ route('update-collectionStatus', [$collection->id]) }}" method="post">
          {{ csrf_field() }}
          
          <input type="hidden" name="collection_id" value="{{ $collection->id}}">

          <div class="form-group">
            <div class="input-group">
              <input type="text" name="collectionPointStatusName" value="{{ $collection->collection_status_name}}" id="collectionPointStatusName" class="form-control" placeholder="Enter Collection Point Status Name" required="required" autofocus>
              <div class="input-group-append">
                <span class="input-group-text">
                  <i class="fa fa-compass"></i>
                </span>
              </div>
              @if ($errors->has('collectionPointStatusName'))
                  <span class="invalid-feedback">
                      <strong>{{ $errors->first('collectionPointStatusName') }}</strong>
                  </span>
               @endif
            </div>
          </div>

          <div class="form-group">
            <button name="savecollectionStatus" id="savecollectionStatus" class="btn btn-success submit-btn btn-block"><i class="menu-icon fas fa-edit"></i> Update Collection Point Status</button>
          </div>
        
        </form>
      </div>
    </div>
  </div>
</div>


@endsection