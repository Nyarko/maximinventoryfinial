@extends('layouts.adminlayout')

@section('title', 'All Users')

@section('content')

  <div class="card">
    <div class="card-body">
      <h2 class="card-title">
        <i class="menu-icon fas fa-compass"></i>  Collection Point
      </h2>
        @if (session('create-point-success'))
        <div class="alert alert-success alert-dismissible" role="alert" id="myAlert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <strong>Sucess!</strong> {{ session('create-point-success') }}
        </div>
        @endif

         @if (session('create-delete-success'))
        <div class="alert alert-success alert-dismissible" role="alert" id="myAlert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <strong>Sucess!</strong> {{ session('create-delete-success') }}
        </div>

        @endif

         @if (session('error'))
            <div class="alert alert-danger" id="myAlert">
                <p>{{ session('error') }}</p>
            </div>
        @endif
      <p class="card-description">
        <a href="{{ route('addCollectionPoint') }}" target="_SELF" class="btn btn-success float-none float-sm-right">Add Collection Point
          <b> <i class="fa fa-plus-square"></i></b>
         </a>
      </p>
      <div class="table-responsive">
         <hr>
        <table class="table table-striped" id="myTable">
          <thead>
            <tr>
             {{--  <th>#</th> --}}
              <th>Picture</th>
              <th>Collection ID</th>
              <th>Collection Point Name</th>
              <th>Collection Manager</th>
              <th>Country</th>
              <th>Status</th>
              <th>State</th>
              <th style="text-align: center">Actions</th>
            </tr>
          </thead>

          <tbody>

           @foreach($collectionpoint as $id => $collection) 
            <tr>
              {{-- <td>{{ $id += 1 }}</td> --}}
               <td class="py-1">
                 <img src="{{ asset('../img/map/'. $collection->image) }}" alt="image" />
               </td>
              <td>{{ $collection->collectionid }}</td>
               <td>{{ $collection->collectionpoint_name }}</td>
               <td>{{ $collection->fname }} {{ $collection->surname }}</td>
               <td>{{ $collection->country }}</td>
                @if($collection->status == 'Active' || $collection->status == 'active')
                  <td style="text-align: center"> <label class="badge badge-success">{{ $collection->status}}</label></td>
                @elseif($collection->status == 'Inactive' || $collection->status == 'inactive')
                   <td style="text-align: center"> <label class="badge badge-danger">{{ $collection->status}}</label></td>
                @else
                    <td style="text-align: center"> <label class="badge badge-info">{{ $collection->status}}</label></td>
                @endif

               
               @if($collection->deleted_at == null)

                  <td style="text-align: center">
                    <a href="{{ route('inactive', [$id => $collection->id]) }}" title="Clicking will set it to Inactive"><label class="badge badge-primary">Active</label></a>
                  </td>

               @else
                  <td style="text-align: center">
                    <a href="{{ route('active', [$id => $collection->id]) }}" title="Clicking will set it to Active"><label class="badge badge-danger">Inactive</label></a>
                  </td>
               @endif

              <td style="text-align: center" colspan="3">
                <a href="{{ route('view-collectionpoint', [$id => $collection->id]) }}" style="color: green" title="view"><i class="fa fa-eye icon-sm"></i></a>   |
               
                 @if($collection->deleted_at == null)

                   <a href="{{ route('edit-collectionpoint', [$id => $collection->id]) }}" style="color: blue" title="edit"><i class="fa fa-pen icon-sm"></i></a>

                 @else

                   <a style="pointer-events: none" href="{{ route('edit-collectionpoint', [$id => $collection->id]) }}" style="color: blue" title="edit"><i class="fa fa-pen icon-sm"></i></a>

                 @endif

              </td>
            </tr>

           @endforeach 
          
          </tbody>
        </table>
      </div>
    </div>
  </div>
@endsection