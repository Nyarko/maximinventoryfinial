@extends('layouts.adminlayout')

@section('title', 'All Collection Point Status')

@section('content')

  <div class="card">
    <div class="card-body">
      <h2 class="card-title">
        <i class="menu-icon fas fa-compass"></i>  Collection Point Status
      </h2>
       @if (session('delete'))
        <div class="alert alert-success alert-dismissible" role="alert" id="myAlert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <strong>Sucess!</strong> {{ session('delete') }}
        </div>
         @endif

          @if (session('error'))
            <div class="alert alert-success" id="myAlert">
                {{ session('error') }}
            </div>
         @endif
        @if (session('create-collectionStatus-success'))
        <div class="alert alert-success alert-dismissible" role="alert" id="myAlert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <strong>Sucess!</strong> {{ session('create-collectionStatus-success') }}
        </div>
        @endif
      <p class="card-description">
        <a href="{{ route('addCollectionpointStatus') }}" target="_SELF" class="btn btn-success float-none float-sm-right">Add Collection Point Status
          <b> <i class="fa fa-plus-square"></i></b>
         </a>
      </p>
      <div class="table-responsive">
         <hr>
        <table class="table table-striped" id="myTable">
          <thead>
            <tr>
              <th>#</th>
              <th>Collection Status</th>
              <th>Created_at</th>
              <th style="text-align: center">Action</th>
            </tr>
          </thead>
          <tbody>

            @foreach($collectionStatus as $id => $collection)

            <tr>
              <td>{{ $id += 1 }}</td>
              <td>{{ $collection->collection_status_name }}</td>
              <td>{{ date('M j, Y', strtotime($collection->created_at)) }}</td>
              <td style="text-align: center" colspan="2">
                <a href="{{ route('edit-collectionstatus',[$id => $collection->id]) }}"  style="color: blue"><i class="fa fa-pen icon-sm"></i></a>  |  
                <a href="{{ route('deleteCollectionPointStatus', [$id => $collection->id]) }}" style="color: red"  onclick="return confirm('Are you sure you want to Delete this COLLECTION POINT STATUS ?')"><i class="fa fa-trash icon-sm"></i></a>
              </td>
            </tr>

           @endforeach



          </tbody>
        </table>
      </div>
    </div>
  </div>
@endsection