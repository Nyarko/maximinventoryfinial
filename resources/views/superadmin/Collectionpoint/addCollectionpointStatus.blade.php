@extends('layouts.adminlayout')

@section('title', 'Add Collection Point Status')

@section('content')

  <div class="content-wrapper d-flex align-items-center auth register-bg-1 theme-one">
  <div class="row w-100">
    <div class="col-lg-6 col-md-7 col-sm-10 col-xs-12 mx-auto" style="padding-top: 30px;padding-bottom: 30px;">
       @if (session('error'))
            <div class="alert alert-success" id="myAlert">
                {{ session('error') }}
            </div>
        @endif
      <div class="auto-form-wrapper">
      <h4 class="text-center mb-4"><i class="menu-icon fas fa-compass"></i> Add Collection Point Status</h4>

        <form action="{{ route('postCollectionPointStatus') }}" method="post">
          {{ csrf_field() }}
          <div class="form-group">
            <div class="input-group">
              <input type="text" name="collectionPointStatusName" id="collectionPointStatusName" class="form-control" placeholder="Enter Collection Point Status Name" required="required" autofocus>
              <div class="input-group-append">
                <span class="input-group-text">
                  <i class="fa fa-compass"></i>
                </span>
              </div>
              @if ($errors->has('collectionPointStatusName'))
                  <span class="invalid-feedback">
                      <strong>{{ $errors->first('collectionPointStatusName') }}</strong>
                  </span>
               @endif
            </div>
          </div>

          <div class="form-group">
            <button name="savecollectionStatus" id="savecollectionStatus" class="btn btn-success submit-btn btn-block"><i class="menu-icon fas fa-save"></i> Save Collection Point Status</button>
          </div>
        
        </form>
      </div>
    </div>
  </div>
</div>


@endsection