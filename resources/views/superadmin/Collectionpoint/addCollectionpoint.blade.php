@extends('layouts.adminlayout')

@section('title', 'Add Collection Point')

@section('content')

  <div class="content-wrapper d-flex align-items-center auth register-bg-1 theme-one">
  <div class="row w-100">
      @if (session('error'))
          <div class="alert alert-success" id="myAlert">
              {{ session('error') }}
          </div>
       @endif
    <div class="col-lg-6 col-md-8 col-sm-12 col-xs-12 mx-auto" style="padding-top: 30px;padding-bottom: 30px;">
      <div class="auto-form-wrapper">
      <h4 class="text-center mb-4"><i class="menu-icon fas fa-compass"></i> Add Collection Point</h4>


        <form action="{{ route('add-collectionpoint') }}" method="post" enctype="multipart/form-data">
          {{ csrf_field() }}
          <div class="form-group">
            <div class="input-group">
              <input type="text" name="name" id="name" class="form-control" placeholder="Enter Collection Point Name" required="required">
              <div class="input-group-append">
                <span class="input-group-text">
                  <i class="fa fa-compass"></i>
                </span>
              </div>
               @if ($errors->has('name'))
              <span class="help-block text-danger">
                <h6>{{ $errors->first('name') }}</h6>
              </span>
              @endif
            </div>
          </div>

           <div class="form-group">
            <select class="form-control" name="manager" id="manager">
              <option value="">Select Collection Manager</option>
              <option value="">Not Available</option>
              @foreach($users as $id => $user)
                <option value="{{ $user->id }}">{{ $user->firstname }} {{ $user->othername }}</option>
              @endforeach

            </select>
             @if ($errors->has('manager'))
              <span class="help-block text-danger">
                <h6>{{ $errors->first('manager') }}</h6>
              </span>
              @endif
          </div>

         
          <div class="form-group">
            <select class="form-control" name="status" id="status">
              <option value="">Select Collection Point Status</option>

              @foreach($collectionStatus as $id => $collection)
                <option value="{{ $collection->id }}">{{ $collection->collection_status_name }}</option>
              @endforeach

            </select>
             @if ($errors->has('status'))
              <span class="help-block text-danger">
                <h6>{{ $errors->first('status') }}</h6>
              </span>
              @endif
          </div>

           <div class="form-group">
            <select class="form-control" name="country" id="country">
              <option value="">Select Country</option>

              @foreach($country as $id => $count)
                <option value="{{ $count->id }}">{{ $count->country_name }}</option>
              @endforeach

            </select>
             @if ($errors->has('country'))
              <span class="help-block text-danger">
                <h6>{{ $errors->first('country') }}</h6>
              </span>
              @endif
          </div>

           <div class="form-group">
              <textarea rows="5" cols="10" class="form-control" name="description" id="description" style="resize: none"
               placeholder="Describe Collection Point Eg. Opening hours 7am t0 5pm"></textarea>
            
               @if ($errors->has('description'))
              <span class="help-block text-danger">
                <h6>{{ $errors->first('description') }}</h6>
              </span>
              @endif
          </div>

           <div class="form-group">
            <div class="input-group">
              <input type="text" name="saddress" id="saddress" class="form-control" placeholder="Enter Street Address">
              <div class="input-group-append">
                <span class="input-group-text">
                  <i class="fa fa-address-book"></i>
                </span>
              </div>
             @if ($errors->has('saddress'))
              <span class="help-block text-danger">
                <h6>{{ $errors->first('saddress') }}</h6>
              </span>
              @endif
            </div>
          </div>

          <div class="form-group">
            <div class="input-group">
              <input type="text" name="numberaddress" id="numberaddress" class="form-control" placeholder="Enter Address Number">
              <div class="input-group-append">
                <span class="input-group-text">
                  <i class="fa fa-address-book"></i>
                </span>
              </div>
             @if ($errors->has('numberaddress'))
              <span class="help-block text-danger">
                <h6>{{ $errors->first('numberaddress') }}</h6>
              </span>
              @endif
            </div>
          </div>

           <div class="form-group">
            <div class="input-group">
              <input type="text" name="postaladdress" id="postaladdress" class="form-control" placeholder="Enter Postal Address">
              <div class="input-group-append">
                <span class="input-group-text">
                  <i class="fa fa-address-book"></i>
                </span>
              </div>
             @if ($errors->has('postaladdress'))
              <span class="help-block text-danger">
                <h6>{{ $errors->first('postaladdress') }}</h6>
              </span>
              @endif
            </div>
          </div>

          <div class="form-group">
             <label for="image">Select Image of Collection Point</label>
            <div class="input-group">
              <input type="file" name="image" id="image" class="form-control" placeholder="Contact Number">
              <div class="input-group-append">
                <span class="input-group-text">
                  <i class="fa fa-image"></i>
                </span>
              </div>
               @if ($errors->has('image'))
              <span class="help-block text-danger">
                <h6>{{ $errors->first('image') }}</h6>
              </span>
              @endif
            </div>
          </div>
        
          <div class="form-group">
              <textarea rows="6" cols="10" class="form-control" name="map" id="map" style="resize: none" placeholder="Please Note this well. Click the link below to map page. 2. Search name of location. 3. Click on Get HTML-Code. 4. copy the text between the 'src=' quote and paste here"></textarea>
            
               @if ($errors->has('map'))
              <span class="help-block text-danger">
                <h6>{{ $errors->first('map') }}</h6>
              </span>
              @endif
          </div>
          <p>
            <a href="https://www.embedgooglemap.net/" target="_blank" style="font-size:12px">Click this link to search for location and copy map source</a>
          </p>
          
          <div class="form-group">
            <button type="submit" name="collectionpoint" id="collectionpoint" class="btn btn-success submit-btn btn-block"><i class="menu-icon fas fa-save"></i> Save Collection Point</button>
          </div>

        </form>
      </div>
    </div>
  </div>
</div>

@endsection