@extends('layouts.adminlayout')

@section('title', 'Add Collection Point')

@section('content')

  <div class="content-wrapper d-flex align-items-center auth register-bg-1 theme-one">
  <div class="row w-100">
      @if (session('error'))
          <div class="alert alert-success" id="myAlert">
              {{ session('error') }}
          </div>
       @endif
    <div class="col-lg-7 col-md-8 col-sm-10 col-xs-12 mx-auto">
      <h2 class="text-center mb-4"><i class="menu-icon fas fa-compass"></i> Edit Collection Point</h2>
      <div class="auto-form-wrapper">


        <form action="{{ route('update-collectionpoint', [$collection->id]) }}" method="post" enctype="multipart/form-data">
          {{ csrf_field() }}

           <input type="hidden" name="collection_id" value="{{ $collection->id}}">

          <div class="form-group">
            <div class="input-group">
              <input type="text" name="name" id="name" value="{{ $collection->collectionpoint_name }}" class="form-control" placeholder="Enter Collection Point Name" required="required">
              <div class="input-group-append">
                <span class="input-group-text">
                  <i class="fa fa-compass"></i>
                </span>
              </div>
               @if ($errors->has('name'))
              <span class="help-block text-danger">
                <h6>{{ $errors->first('name') }}</h6>
              </span>
              @endif
            </div>
          </div>

           <div class="form-group">
            <select class="form-control" name="manager" id="manager">
              <option selected value="{{ $collection->user_id}}">{{ $collection->fname }} {{ $collection->surname }}</option>
              <option value="">Not Available</option>
              @foreach($users as $id => $user)
                <option value="{{ $user->id }}">{{ $user->firstname }} {{ $user->othername }}</option>
              @endforeach

            </select>
             @if ($errors->has('manager'))
              <span class="help-block text-danger">
                <h6>{{ $errors->first('manager') }}</h6>
              </span>
              @endif
          </div>

         
          <div class="form-group">
            <select class="form-control" name="status" id="status">
              <option selected value="{{ $collection->collection_status_id }}">{{ $collection->status }}</option>
              @foreach($collectionStatus as $id => $coll)
                <option value="{{ $coll->id }}">{{ $coll->collection_status_name }}</option>
              @endforeach

            </select>
             @if ($errors->has('status'))
              <span class="help-block text-danger">
                <h6>{{ $errors->first('status') }}</h6>
              </span>
              @endif
          </div>

           <div class="form-group">
            <select class="form-control" name="country" id="country">
              <option selected value="{{ $collection->country_id }}">{{ $collection->country }}</option>

              @foreach($location as $id => $count)
                <option value="{{ $count->id }}">{{ $count->country_name }}</option>
              @endforeach

            </select>
             @if ($errors->has('country'))
              <span class="help-block text-danger">
                <h6>{{ $errors->first('country') }}</h6>
              </span>
              @endif
          </div>

           <div class="form-group">
              <textarea rows="5" cols="5" class="form-control" name="description" id="description" style="resize: none" placeholder="Describe Collection Point">{{ $collection->description}}</textarea>
               @if ($errors->has('description'))
              <span class="help-block text-danger">
                <h6>{{ $errors->first('description') }}</h6>
              </span>
              @endif
          </div>

           <div class="form-group">
            <div class="input-group">
              <input type="text" name="saddress" value="{{ $collection->street_address }}" id="saddress" class="form-control" placeholder="Enter Street Address">
              <div class="input-group-append">
                <span class="input-group-text">
                  <i class="fa fa-address-book"></i>
                </span>
              </div>
             @if ($errors->has('saddress'))
              <span class="help-block text-danger">
                <h6>{{ $errors->first('saddress') }}</h6>
              </span>
              @endif
            </div>
          </div>

          <div class="form-group">
            <div class="input-group">
              <input type="text" name="numberaddress" value="{{ $collection->number_address }}" id="numberaddress" class="form-control" placeholder="Enter Address Number">
              <div class="input-group-append">
                <span class="input-group-text">
                  <i class="fa fa-address-book"></i>
                </span>
              </div>
             @if ($errors->has('numberaddress'))
              <span class="help-block text-danger">
                <h6>{{ $errors->first('numberaddress') }}</h6>
              </span>
              @endif
            </div>
          </div>

           <div class="form-group">
            <div class="input-group">
              <input type="text" name="postaladdress" value="{{ $collection->postal_address }}" id="postaladdress" class="form-control" placeholder="Enter Postal Address">
              <div class="input-group-append">
                <span class="input-group-text">
                  <i class="fa fa-address-book"></i>
                </span>
              </div>
             @if ($errors->has('postaladdress'))
              <span class="help-block text-danger">
                <h6>{{ $errors->first('postaladdress') }}</h6>
              </span>
              @endif
            </div>
          </div>
           
          <div class="form-group">
             <label for="image">Select Image of Collection Point</label>
            <div class="input-group">
              <input type="file" name="image" id="image" class="form-control">
              <input type="hidden" value="{{ $collection->image }}" name="avatar" id="avatar" class="form-control">
              <div class="input-group-append">
                <span class="input-group-text">
                  <img class="img-thumbnail" width="40px" height="40px" src="{{ asset('../img/map/'. $collection->image) }}">
                </span>
              </div>
               @if ($errors->has('image'))
              <span class="help-block text-danger">
                <h6>{{ $errors->first('image') }}</h6>
              </span>
              @endif
            </div>
          </div>
        
          <div class="form-group">
              <textarea rows="10" cols="2" class="form-control" name="map" id="map" style="resize: none" placeholder="Enter Map Html">
                {{$collection->map}}
              </textarea>
               @if ($errors->has('map'))
              <span class="help-block text-danger">
                <h6>{{ $errors->first('map') }}</h6>
              </span>
              @endif
          </div>
          
          <p>
           <a href="https://www.embedgooglemap.net/" style="font-size:12px">Click this link to search for location and copy map source</a>
          </p>
          
          <div class="form-group">
            <button type="submit" name="collectionpoint" id="collectionpoint" class="btn btn-success submit-btn btn-block"><i class="menu-icon fas fa-edit"></i> Update Collection Point</button>
          </div>

        </form>
      </div>
    </div>
  </div>
</div>

@endsection