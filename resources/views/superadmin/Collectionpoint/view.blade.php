@extends('layouts.adminlayout')

@section('title', 'View Collection Point')

@section('content')

<div class="content-wrapper d-flex align-items-center auth register-bg-1 theme-one">
  <div class="row w-100" >
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <h2 class="text-center mb-4"><i class="menu-icon fas fa-compass"></i> Collection Point Description</h2>
      <div class="auto-form-wrapper" style="padding: 10px -5px 10px 10px; margin-right: -20px">
     
          <div class="row" style="width:100%; height:350px">
             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

              @if($collectionpoint->map == null)
                  <center><h3>Location on Map Not Set</h3></center>
              @else
                <div class="mapouter">
                   <div class="gmap_canvas">
                    <iframe id="gmap_canvas" src="{{ $collectionpoint->map}}" frameborder="0" scrolling="no" marginheight="0" marginwidth="0">  
                    </iframe>
                  </div>
                </div>
              @endif  
             </div>
          </div>
          <hr>
          <div class="row">
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                 <img class="img-thumbnail" src="{{ asset('../img/users/'. $collectionpoint->picture) }}">
              </div>
              <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                <table class="table table-hover table-responsive">
                   <tbody>
                     <tr>
                       <th><i class="fa fa-user"></i></th>
                       <th>Collection Manager Name</th>
                       <td>{{ $collectionpoint->fname }} {{ $collectionpoint->surname }}</td>
                     </tr>
                      <tr>
                       <th><i class="fa fa-key"></i></th>
                       <th> Manager User ID</th>
                       <td>{{ $collectionpoint->userid }}</td>
                     </tr>
                     <tr>
                       <th><i class="fa fa-envelope"></i></th>
                       <th>Email</th>
                       <td>{{ $collectionpoint->email }}</td>
                     </tr>
                      <tr>
                       <th><i class="fa fa-phone"></i> </th>
                       <th>Contact</th>
                       <td>{{ $collectionpoint->contact }}</td>
                     </tr>
                   </tbody>
                </table>
              </div>
          </div>
          <hr>
          <div class="row">
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                 <img class="img-thumbnail" src="{{ asset('../img/map/'. $collectionpoint->image) }}">
              </div>
              <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                <table class="table table-hover table-responsive">
                   <tbody>
                     <tr>
                       <th><i class="fa fa-key"></i></th>
                       <th> Collection Point ID</th>
                       <td>{{ $collectionpoint->collectionid }}</td>
                     </tr>
                     <tr>
                       <th><i class="fa fa-compass"></i></th>
                       <th>Collection Point Name</th>
                       <td>{{ $collectionpoint->collectionpoint_name }}</td>
                     </tr>
                      <tr>
                       <th><i class="fa fa-building"></i></th>
                       <th>Street Address</th>
                       <td>{{ $collectionpoint->street_address}}</td>
                     </tr>
                      <tr>
                       <th><i class="fa fa-bookmark"></i></th>
                       <th>Address Number</th>
                       <td>{{ $collectionpoint->number_address}}</td>
                     </tr>
                      <tr>
                       <th><i class="fa  fa-address-book"></i></th>
                       <th>Postal Address</th>
                       <td>{{ $collectionpoint->postal_address}}</td>
                     </tr>
                     <tr>
                       <th><i class="fa fa-circle"></i></th>
                       <th>Country</th>
                       <td>{{ $collectionpoint->country}}</td>
                     </tr>
                     <tr>
                       <th><i class="fa fa-snowflake"></i></th>
                       <th>Description</th>
                       <td>{{ $collectionpoint->description }}</td>
                     </tr>
                   </tbody>

                </table>
              </div>
          </div>
          <hr>
          <div class="row">
            <table>
               <tfoot style="text-align: center;">
               <tr>
                  <td><a href="{{ route('allCollectionpoint') }}" class="btn btn-success"><i class="fa fa-arrow-left"></i> Back</a></td>
                  <td><a href="{{ route('edit-collectionpoint', [$id => $collectionpoint->id]) }}" class="btn btn-primary"><i class="fa fa-edit"></i> Edit</a></td>
                </tr>
             </tfoot>
            </table>
          </div>
         
                   

      </div>
    </div>
  </div>
</div>

@endsection