@extends('layouts.userlayout')

@section('title', 'Login')

@section('content')

<!-- error message display div -->
<div>
  @if(Session::has('errormessage'))

  <div class="alert alert-danger alert-dismissible" role="alert">
   <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span></button>
    <strong>Ooops!</strong>{{ Session::get('errormessage') }}
  </div>

  @endif
</div>

<!-- end of error message display div -->
  <div style="margin-bottom: 20px;text-align: center;color: #00CE68">
    <h4>Log In</h4>
  </div>

<form  method="post" action="{{ route('post-login')}}" enctype="multipart/form-data">
  {{ csrf_field() }}
  <div class="form-group">
    <label class="label">Email*</label>
    <div class="input-group">
      <input type="text" class="form-control" required="required" name="email" placeholder="admin@computer4schools.org" id="email">
      <div class="input-group-append">
        <span class="input-group-text">
          <i class="mdi mdi-email"></i>
        </span>
      </div>
    </div>
  </div>

  <div class="form-group">
    <label class="label">Password*</label>
    <div class="input-group">
      <input type="password" name="password" required="required" class="form-control" placeholder="Password" id="password">
      <div class="input-group-append">
        <span class="input-group-text">
          <i class="mdi mdi-key"></i>
        </span>
      </div>
    </div>
  </div>

  <div class="form-group">
    <button class="btn btn-success submit-btn btn-block">Login</button>
  </div>

  <div class="form-group d-flex justify-content-between">
    <div class="form-check form-check-flat mt-0">
      <label class="form-check-label">
        <input type="checkbox" class="form-check-input" checked> Keep me signed in
      </label>
    </div>
    <a href="#" class="text-small forgot-password text-black">Forgot Password</a>
  </div>

  <div class="text-block text-center my-3">
<!--           <span class="text-small font-weight-semibold">Not a member ?</span>
-->          <a href="{{ route('indexpage')}}" class="text-black text-small">Back - Inventory System Home</a><br>
          <span class="text-small font-weight-semibold">©Copyright - Computer4schoolsGhana | 2018</span>
</div>
</form>

@endsection