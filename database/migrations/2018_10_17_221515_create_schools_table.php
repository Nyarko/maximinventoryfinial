<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schools', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('region_id')->unsigned()->nullable();
            $table->integer('district_id')->unsigned()->nullable();
            $table->integer('destination_id')->unsigned()->nullable();
            $table->string('school_name');
            $table->string('phone');
            $table->string('email')->unique();
            $table->string('address')->nullable();
            $table->string('town')->nullable();
            $table->string('map')->nullable();
            $table->integer('state')->default(0);
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('region_id')
                  ->references('id')
                  ->on('regions')
                  ->onDelete('set null')
                  ->onUpdate('cascade');

            $table->foreign('district_id')
                  ->references('id')
                  ->on('districts')
                  ->onDelete('set null')
                  ->onUpdate('cascade');

            $table->foreign('destination_id')
                  ->references('id')
                  ->on('destination_points')
                  ->onDelete('set null')
                  ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schools');
    }
}
