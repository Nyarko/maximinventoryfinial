<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDestinationPointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('destination_points', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('region_id')->unsigned()->nullable();
            $table->integer('district_id')->unsigned()->nullable();
            $table->integer('destination_status_id')->unsigned()->nullable();
            $table->string('destinationpoint_name');
            $table->string('town')->nullable();
            $table->string('description')->nullable();
            $table->timestamps();
            $table->softDeletes();
           
            $table->foreign('user_id')
                  ->references('id')
                  ->on('users')
                  ->onDelete('set null')
                  ->onUpdate('cascade');

            $table->foreign('region_id')
                  ->references('id')
                  ->on('regions')
                  ->onDelete('set null')
                  ->onUpdate('cascade');

            $table->foreign('district_id')
                  ->references('id')
                  ->on('districts')
                  ->onDelete('set null')
                  ->onUpdate('cascade');
          
            $table->foreign('destination_status_id')
                  ->references('id')
                  ->on('destination_point_statuses')
                  ->onDelete('set null')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('destination_points');
    }
}
