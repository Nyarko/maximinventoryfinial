<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeneralSerialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('general_serials', function (Blueprint $table) {
            $table->increments('id');
            $table->string('generalid')->unique();
            $table->integer('category_id')->unsigned()->nullable();
            $table->integer('collectionpoint_id')->unsigned()->nullable();
            $table->string('name')->nullable();
            $table->string('quantity')->nullable();
            $table->string('donorid');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('category_id')
                  ->references('id')
                  ->on('categories')
                  ->onDelete('set null')
                  ->onUpdate('cascade');

            $table->foreign('collectionpoint_id')
                  ->references('id')
                  ->on('collection_points')
                  ->onDelete('set null')
                  ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('general_serials');
    }
}
