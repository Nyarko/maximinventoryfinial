<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('general_id')->unsigned()->nullable();
            $table->integer('school_id')->unsigned()->nullable();
            $table->integer('item_status_id')->unsigned()->nullable();
            $table->string('description')->nullable()->nullable();
            $table->string('itemid')->unique();
            $table->integer('projectid')->default(0);
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('general_id')
                  ->references('id')
                  ->on('general_serials')
                  ->onDelete('set null')
                  ->onUpdate('cascade');

            $table->foreign('school_id')
                  ->references('id')
                  ->on('schools')
                  ->onDelete('set null')
                  ->onUpdate('cascade');

            $table->foreign('item_status_id')
                  ->references('id')
                  ->on('item_statuses')
                  ->onDelete('set null')
                  ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
