<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->enum('status', ['New', 'Ongoing', 'Completed']);
            $table->date('start');
            $table->date('end');
            $table->integer('destination_id')->unsigned()->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('destination_id')
                  ->references('id')
                  ->on('destination_points')
                  ->onDelete('set null')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
