<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCollectionPointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('collection_points', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('collection_status_id')->unsigned()->nullable();
            $table->integer('country_id')->unsigned()->nullable();
            $table->string('collectionid');
            $table->string('collectionpoint_name');
            $table->string('description')->nullable();
            $table->string('street_address')->nullable();
            $table->string('number_address')->nullable();
            $table->string('postal_address')->nullable();
            $table->string('image')->nullable();
            $table->text('map')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('user_id')
                  ->references('id')
                  ->on('users')
                  ->onDelete('set null')
                  ->onUpdate('cascade');

            $table->foreign('collection_status_id')
                  ->references('id')
                  ->on('collection_point_statuses')
                  ->onDelete('set null')
                  ->onUpdate('cascade');

            $table->foreign('country_id')
                  ->references('id')
                  ->on('countries')
                  ->onDelete('set null')
                  ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('collection_points');
    }
}
