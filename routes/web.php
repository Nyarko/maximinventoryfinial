<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Stephen's routes

//Returning the index page
Route::get('/', 'PagesController@indexPage')->name('indexpage');
Route::get('/track', 'PagesController@indexTrack')->name('trackpage');
Route::get('/collectionpoint/{id}/view', 'PagesController@viewCollectionPoint')->name('viewcollection');


//Login routes goes here
Route::get('/login', 'LoginController@loginUser')->name('login');
Route::post('/login', 'LoginController@postLoginUser')->name('post-login');
Route::get('/logout', 'LoginController@logOut')->name('logout');

Route::get('/admin/users', 'AdminController@allUsers')->name('alluser');
Route::get('/admin/user/add', 'AdminController@addUser')->name('addusers');
Route::post('/admin/user/add', 'AdminController@postAddUser')->name('post-add-user');
Route::get('/admin/user/{id}/edit', 'AdminController@EditUser')->name('edit-user');
Route::post('/admin/user/{id}/edit', 'AdminController@postEditUser')->name('post-edit-user');
Route::get('admin/user/{id}/delete', 'AdminController@deleteUser')->name('delete-user');


// ROUTES FOR ITEM CATEGORIES
Route::get('/admin/categories', 'AdminController@allCategories')->name('allCategories');
Route::get('/admin/category/create', 'AdminController@addCategory')->name('addCategory');
Route::post('/admin/varoius/create', 'AdminController@postAddCategory')->name('post-add-category');
Route::get('/admin/category/{id}/edit', 'AdminController@EditCategory')->name('edit-category');
Route::post('/admin/category/{id}/edit', 'AdminController@postEditCategory')->name('post-edit-category');
Route::get('admin/category/{id}/delete', 'AdminController@deleteCategory')->name('delete-category');



//ROUTES FOR DESTINATION POINT STATUS
Route::get('/admin/state', 'AdminController@allDestinationPointStatus')->name('allDestinationPointStatus');
Route::get('/admin/destinationpointstatus/add', 'AdminController@addDestinationPointStatus')->name('addDestinationpointStatus');
Route::post('/admin/destinationpointstatus/add', 'AdminController@PostDestinationPointStatus')->name('post-destinationpoint-status');
Route::get('/admin/destinationpointstatus/{id}/edit', 'AdminController@EditDestinationPointStatus')->name('edit-destinationpointstatus');
Route::post('/admin/destinationpointstatus/{id}/edit', 'AdminController@posEdittDestinationPointStatus')->name('post-edit-destinationpointstatus');
Route::get('/admin/destinationpointstatus/{id}/delete', 'AdminController@deleteDestinationPointStatus')->name('delete-destinationpointstatus');

//ROUTES FOR DESTINATION POINTS
Route::get('/admin/destinationpoint', 'AdminController@allDestinationPoint')->name('all-destination-points');
Route::get('/admin/destinationpoint/add', 'AdminController@addDestinationPoint')->name('addDestinationPoint');
Route::post('/admin/destinationpoint/add', 'AdminController@PostDestinationPoint')->name('post-destinationpoint');
Route::get('/admin/destinationpoint/{id}/edit', 'AdminController@EditDestinationPoint')->name('edit-destinationpoint');
Route::post('/admin/destinationpoint/{id}/edit', 'AdminController@PostEditDestinationPoint')->name('post-edit-destinationpoint');
Route::get('/admin/destinationpoint/{id}/view',  'AdminController@viewDestinationPoint')->name('view-destination-point');
Route::get('/admin/destinationpoint/{id}/delete', 'AdminController@deleteDestinationPoint')->name('delete-destinationpoint');


// ROUTES FOR ITEM STATUS CRUD
Route::get('/admin/statuses', 'AdminController@allItemsStatus')->name('allItemsStatus');
Route::get('/admin/item/status/add', 'AdminController@addItemsStatus')->name('addItemsStatus');
Route::post('/admin/add/item/status', 'AdminController@postadddItemStatus')->name('post-add-item-status');
Route::get('/admin/item/status/{id}/edit', 'AdminController@EditItemStatus')->name('edit-item-status');
Route::post('/admin/item/status/{id}/edit', 'AdminController@PostEditItemStatus')->name('post-edit-item-status');
Route::get('/admin/item/status/{id}/delete', 'AdminController@deleteItemStatus')->name('delete-item-status');


//ROUTES FOR PROJECT PLANNING
Route::get('/admin/cate/donated', 'AdminController@getAllCategoriesDonated')->name('all-categories-donated');
Route::get('/admin/view/item/{id}/category', 'AdminController@viewItemCategoryProject')->name('view-category-item-project');

Route::post('/admin/items/allocate/project', 'AdminController@allocateItemsToProject')->name('allocate-items-to-project');

Route::get('/admin/project', 'AdminController@projectIndex')->name('project-index');
Route::get('/admin/project/initiate', 'AdminController@addProject')->name('add-project-start');
Route::post('/admin/project/create', 'AdminController@postProject')->name('post-save-project');
Route::get('/admin/project/{id}/view', 'AdminController@viewProject')->name('view-project-plan');
Route::get('/admin/project/{id}/ongoing', 'AdminController@ongoingProject')->name('set-ongoing');
Route::get('/admin/project/{id}/completed', 'AdminController@completedProject')->name('set-completed');
Route::get('/admin/project/{id}/edit', 'AdminController@editProject')->name('edit-project');
Route::post('/admin/project/{project_id}/update', 'AdminController@updateProject')->name('update-project');

//ROUTES FOR ITEMS
Route::get('/admin/all/items', 'AdminController@allItem')->name('all-items');
Route::get('/admin/item/create', 'AdminController@createItem')->name('create-item');
Route::post('/admin/item/create', 'AdminController@postCreateItem')->name('post-create-item');
Route::get('/admin/item/{id}/view/items', 'AdminController@viewItems')->name('view-items');

Route::post('/admin/item/multiple/status/update', 'AdminController@multipleStatusUpdate')->name('item-multiple-status-update');

Route::post('/admin/item/status/{status_id}/update', 'AdminController@updateItemStatus')->name('item-status-update');
Route::get('/admin/item/{id}/project', 'AdminController@setProject')->name('add-project');
Route::get('/admin/item/{id}/reverted', 'AdminController@revertProject')->name('revert-project');
Route::get('/admin/item/{id}/tosend', 'AdminController@viewItemsToSend')->name('item-to-send');

Route::post('/admin/message/send', 'AdminController@send')->name('send');

Route::post('/admin/selected/add/project', 'AdminController@addSelectedItemProject')->name('all-add-to-project');


//Route::post('/admin/item/status/update', 'AdminController@updateItemStatus');










































//Isaac's routes


//***************************Admin pages ***************************8***
//Route for admin dashboard
Route::get('/admin/dashboard', 'AdminController@dashboard')->name('adminindex');


//Route fro regions
Route::get('/admin/regions', 'AdminController@allRegion')->name('allregion');
Route::get('/admin/regions/add', 'AdminController@addRegion')->name('addRegions');
Route::post('/admin/region/add', 'AdminController@postAddRegion')->name('postAddRegion');
Route::get('/admin/region/{id}/edit', 'AdminController@editRegion')->name('region-edit');
Route::post('/region/{region_id}/edit', 'AdminController@updateRegion')->name('update-region');
Route::get('/admin/region/delete/{id}', 'AdminController@deleteRegion')->name('deleteRegion');

//Route for District
Route::get('/admin/district', 'AdminController@allDistrict')->name('allDistrict');
Route::get('/admin/district/add', 'AdminController@addDistrict')->name('addDistrict');
Route::post('/admin/district/add', 'AdminController@postAddDistrict')->name('postAddDistrict');
Route::get('/admin/district/{id}/edit', 'AdminController@editDistrict')->name('district-edit');
Route::post('/admin/district/{district_id}/edit', 'AdminController@updateDistrict')->name('update-district');
Route::get('/admin/district/{id}/delete', 'AdminController@deleteDistrict')->name('deleteDistrict');



//Route for country
Route::get('/admin/country', 'AdminController@allCountry')->name('allCountry');
Route::get('admin/country/add', 'AdminController@addCountry')->name('addCountry');
Route::post('/admin/country/add', 'AdminController@postAddCountry')->name('postAddCountry');
Route::get('/admin/country/{id}/edit', 'AdminController@editCountry')->name('country-edit');
Route::post('/admin/country/{country_id}/edit', 'AdminController@updateCountry')->name('update-country');
Route::get('/admin/country/{id}/delete', 'AdminController@deleteCountry')->name('deleteCountry');




//Routes for collection point and collection point status
Route::get('/admin/collectionPoint', 'AdminController@allCollectionPoint')->name('allCollectionpoint');
Route::get('/admin/collectionPoint/add', 'AdminController@addCollectionPoint')->name('addCollectionPoint');
Route::post('/admin/collectionpoint/add', 'AdminController@postAddCollectionPoint')->name('add-collectionpoint');
Route::get('/admin/collectionpoint/{id}/view', 'AdminController@viewCollectionPoint')->name('view-collectionpoint');
Route::get('/admin/collectionpoint/{id}/active', 'AdminController@activeCollectionPoint')->name('active');
Route::get('/admin/collectionpoint/{id}/edit', 'AdminController@editCollectionPoint')->name('edit-collectionpoint');
Route::post('/admin/collectionpoint/{collection_id}/edit', 'AdminController@updateCollectionPoint')->name('update-collectionpoint');
Route::get('/admin/collectionpoint/{id}/delete', 'AdminController@deleteCollectionPoint')->name('inactive');


Route::get('/admin/collectionointstatus', 'AdminController@allCollectionPointStatus')->name('allCollectionPointStatus');
Route::get('/admin/collectionpoint/status/add', 'AdminController@addCollectionpointStatus')->name('addCollectionpointStatus');
Route::post('/admin/collectionpoint/status/add', 'AdminController@postAddCollectionPointStatus')->name('postCollectionPointStatus');
Route::get('/admin/collection/status/{id}/edit', 'AdminController@editCollectionStatus')->name('edit-collectionstatus');
Route::post('/admin/collection/status/{collection_id}/update', 'AdminController@updateCollectionStatus')->name('update-collectionStatus');
Route::get('/admin/collectionpoint/status/delete/{id}', 'AdminController@deleteCollectionPointStatus')->name('deleteCollectionPointStatus');


//Routes for donators
Route::get('/admin/alldonators', 'AdminController@allDonators')->name('alldonators');
Route::get('/admin/addDonator', 'AdminController@addDonators')->name('addDonator');
Route::post('/admin/donor/add', 'AdminController@postaddDonators')->name('post-donor');
Route::get('/admin/donor/{id}/edit', 'AdminController@editDonator')->name('edit-donor');
Route::post('/admin/donor/{donor_id}/update', 'AdminController@updateDonor')->name('update-donor');
Route::get('/admin/donor/{id}/delete', 'AdminController@deleteDonor')->name('delete-donor');
Route::get('/admin/donor/{id}/restore', 'AdminController@restoreDonors')->name('restore');

//Route for Schools
Route::get('/admin/allSchools', 'AdminController@allSchools')->name('allschools');
Route::get('/addSchool', 'AdminController@addSchool')->name('addschool');
Route::post('/admin/school/add', 'AdminController@postaddSchool')->name('post-school');
Route::get('/admin/Schools/{id}/view', 'AdminController@viewSchools')->name('view-school');
Route::get('/admin/school/{id}/inprogress', 'AdminController@setInprogress')->name('inprogress');
Route::get('/admin/school/{id}/completed', 'AdminController@setCompleted')->name('completed');
Route::get('/admin/school/{id}/delete', 'AdminController@deleteSchool')->name('delete-school');
Route::get('/admin/school/{id}/restore', 'AdminController@restoreSchool')->name('restoreschool');
Route::get('/admin/school/{id}/edit', 'AdminController@editSchool')->name('edit-school');
Route::post('/admin/school/{school_id}/update', 'AdminController@updateSchool')->name('update-student');

//******************** Admin Route Ends Here ***************************************************************************


//****************** MN Agent Route *************************************************************************************
Route::get('/mn_agent', 'MNAgentController@dashboard')->name('mn_agentindex');

//Schools routes
Route::get('/mn_agent/allSchools', 'MNAgentController@allSchools')->name('mn_allschools');
Route::get('/mn_agent/addSchool', 'MNAgentController@addSchool')->name('mn_addschool');

//Items route
Route::get('/mn_agent/allItems', 'MNAgentController@allItems')->name('mn_allItems');
Route::get('/mn_agent/addItems', 'MNAgentController@addItems')->name('mn_addItems');

//Donators route
Route::get('/mn_agent/allDonators', 'MNAgentController@allDonators')->name('mn_allDonators');
Route::get('/mn_agent/addDonator', 'MNAgentController@addDonator')->name('mn_addDonator');



//**************** MN Agent Route Ends *********************************************************************************


//*************** Collection Point Routes ******************************************************************************
//Dashboard
Route::get('/collectionpoint/index', 'CollectionPointController@dashboard')->name('collectionIndex');

//Items route
Route::get('/collectionpoint/allItems', 'CollectionPointController@allItems')->name('collection_allItems');
Route::get('/collectionpoint/addItems', 'CollectionPointController@addItems')->name('collection_addItems');

//Donators route
Route::get('/collectionpoint/allDonators', 'CollectionPointController@allDonors')->name('collection_allDonators');
Route::get('/collectionpoint/addDonator', 'CollectionPointController@addDonators')->name('collection_addDonator');


//************** Collection Point Routes Ends here *********************************************************************


//************** Destination Point Route *******************************************************************************
// Index
Route::get('destination/index', 'DestinationPointController@dashboard')->name('destination_index');

//School route
Route::get('/destination/allschools', 'DestinationPointController@allSchools')->name('destination_allSchools');
Route::get('/destination/school/add', 'DestinationPointController@addSchool')->name('destination_addSchool');

//Items route
Route::get('/destination/allItems', 'DestinationPointController@allItems')->name('destination_allItems');
Route::get('/destination/Item/add', 'DestinationPointController@addItems')->name('destination_addItems');

//************** End of destination point route ***********************************************************************
